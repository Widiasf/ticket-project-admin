var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

var Level = require('../components/Level')
var Petugas = require('../components/Petugas')
var Pemesanan = require('../components/Pemesanan')
var Penumpang = require('../components/Penumpang')
var TipeTransportasi = require('../components/TipeTransportasi')
var Transportasi = require('../components/Transportasi')
var Rute = require('../components/Rute')
var Pemberangkatan = require('../components/Pemberangkatan')
//LEVEL
router.post('/Level', Level.create);
router.get('/Level', Level.read);
router.get('/Level/:id', Level.readSingle);
router.put('/Level/:id', Level.update);
router.delete('/Level/:id',Level.delete);
//PETUGAS
router.post('/Petugas', Petugas.create);
router.get('/Petugas', Petugas.read);
router.get('/Petugas/:id', Petugas.readSingle);
router.put('/Petugas/:id', Petugas.update);
router.delete('/Petugas/:id',Petugas.delete);
router.post('/LoginPetugas', Petugas.loginPetugas);
//PENUMPANG
router.post('/Penumpang', Penumpang.create);
router.get('/Penumpang', Penumpang.read);
router.get('/Penumpang/:id', Penumpang.readSingle);
router.put('/Penumpang/:id', Penumpang.update);
router.delete('/Penumpang/:id',Penumpang.delete);
//TIPE TRANSPORTASI
router.post('/TipeTransportasi', TipeTransportasi.create);
router.get('/TipeTransportasi', TipeTransportasi.read);
router.get('/TipeTransportasi/:id', TipeTransportasi.readSingle);
router.put('/TipeTransportasi/:id', TipeTransportasi.update);
router.delete('/TipeTransportasi/:id',TipeTransportasi.delete);
//TRANSPORTASI
router.post('/Transportasi', Transportasi.create);
router.get('/Transportasi', Transportasi.read);
router.get('/Transportasi/:id', Transportasi.readSingle);
router.put('/Transportasi/:id', Transportasi.update);
router.delete('/Transportasi/:id',Transportasi.delete);
//RUTE
router.post('/Rute', Rute.create);
router.get('/Rute', Rute.read);
router.get('/Rute/:id', Rute.readSingle);
router.put('/Rute/:id', Rute.update);
router.delete('/Rute/:id',Rute.delete);
//PEMESANAN
router.post('/Pemesanan', Pemesanan.create);
router.get('/Pemesanan', Pemesanan.read);
router.get('/Pemesanan/:id', Pemesanan.readSingle);
router.put('/VerifikasiAdmin/:id', Pemesanan.verifikasiAdmin);
router.delete('/Pemesanan/:id',Pemesanan.delete);
//PEMBERANGKATAN
router.post('/Pemberangkatan', Pemberangkatan.create);
router.get('/Pemberangkatan', Pemberangkatan.read);
router.get('/Pemberangkatan/:id', Pemberangkatan.readSingle);
router.put('/Pemberangkatan/:id', Pemberangkatan.update);
router.delete('/Pemberangkatan/:id',Pemberangkatan.delete);
//ANDROID


//Read all transportation
router.get('/AllTransportasi', Transportasi.readAllTransportation);
//Read all pemesanan by user
router.get('/AllPemesanan/:id', Pemesanan.readAllPemesanan);
//update user sudah bayar
router.put('/Pemesanan/:id', Pemesanan.update);
//login penumpang
router.post('/LoginPenumpang', Penumpang.loginPenumpang);

var User = require('../components/User');
var UsersLevel = require('../components/UsersLevel');
var UsersProjectCategory = require('../components/UsersProjectCategory');
var UsersProjectFile= require('../components/UsersProjectFile');
var Project = require('../components/Project');
var UsersProject = require('../components/UsersProject');
var notification = require('../components/notification');
//USER 
router.post('/Users', User.create);
router.get('/Users', User.read);
router.get('/Users/:id', User.readSingle);
router.put('/Users/:id', User.update);
router.delete('/Users/:id',User.delete);
//USERS PROJECT CATEGORY 
router.post('/UsersProjectCategory', UsersProjectCategory.create);
router.get('/UsersProjectCategory', UsersProjectCategory.read);
router.get('/UsersProjectCategory/:id', UsersProjectCategory.readSingle);
router.put('/UsersProjectCategory/:id', UsersProjectCategory.update);
router.delete('/UsersProjectCategory/:id',UsersProjectCategory.delete);
//USERS LEVEL 
router.post('/UsersLevel', UsersLevel.create);
router.get('/UsersLevel', UsersLevel.read);
router.get('/UsersLevel/:id', UsersLevel.readSingle);
router.put('/UsersLevel/:id', UsersLevel.update);
router.delete('/UsersLevel/:id',UsersLevel.delete);
//PROJECT 
router.post('/Project', Project.create);
router.get('/Project/:id', Project.read);
router.get('/Project-detail/:id', Project.readSingle);
router.put('/Project/:id', Project.update);
router.delete('/Project/:id',Project.delete);
//USERS PROJECT FILE
router.post('/UsersProjectFile', UsersProjectFile.create);
router.get('/UsersProjectFile', UsersProjectFile.read);
router.get('/UsersProjectFile/:id', UsersProjectFile.readSingle);
router.put('/UsersProjectFile/:id', UsersProjectFile.update);
router.put('/RemoveUsersProjectFile/:id',UsersProjectFile.delete);
//USERS PROJECT 
router.post('/UsersProject', UsersProject.create);
router.get('/UsersProject/:id', UsersProject.read);
router.get('/UsersProjectRSingle/:id', UsersProject.readSingle);
router.put('/UsersProject/:id', UsersProject.update);
router.put('/RemoveUsersProject/:id',UsersProject.delete);
//LOGIN
router.post('/Login',User.login);
//READ MY JOB
router.get('/readMyJob/:id', UsersProject.readMyJob);
//READ FILE BY USER
router.get('/readFileByUser/:id',UsersProjectFile.readFileByUser);
//READ USER BY LEVEL
router.get('/readUserByLevel/:id',User.readUsersByLevel);
//COUNT PEKERJAAN
router.get('/countPekerjaan/:id',UsersProject.readCountPekerjaan);
//COUNT PEKERJAAN REVIEW
router.get('/readPekerjaanReview',UsersProject.readPekerjaanReview);
//PROJECT CHILD
router.post('/createProjectChild',Project.insertChild);
//NOTIFICATION
router.get('/readNotif/:id',notification.read);
router.put('/updateNotif/:id',notification.update);
//READ LIST KARYAWAN
router.get('/listKaryawan/:id', Project.readListKaryawan);
//READ LIST KARYAWAN
router.get('/readProjectKaryawan/:id', Project.readProjectKaryawan);
//FILE BY PROJECT
router.get('/ProjectFile/:id', UsersProjectFile.readFileByProject);
//LOG
router.get('/Log/:id', Project.readLog);
//READ PROJECT ADMIN
router.get('/ProjectAdmin', Project.readProjectAdmin);
module.exports = router;
