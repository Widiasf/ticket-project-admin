@extends('layout.app')

@section('content')
<div class="jumbotron bg-white">
    <div class="d-flex justify-content-between">
        <h1>Daftar Transportasi</h1>
        <div>
            <button class="btn btn-success" type="button" data-toggle="modal" data-target="#tambahTransportasi">
                <i class="icon-plus pr-1"></i>Tambah Transportasi</button>
        </div>
    </div>
    <table id="daftar_proyek" class="table table-responsive-sm">
        <thead>
            <tr>
                <th>Kode</th>
                <th>Nama Transportasi</th>
                <th>Jumlah Kursi</th>
                <th>Keterangan</th>
                <th>Tipe Transportasi</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            @if (!is_null($data))
            @php
            $x = 0
            @endphp
            @foreach ($data['transportasi'] as $key => $value)
            @php
            $x++
            @endphp
            <tr>
                <td>
                    {{$value->kode}}
                </td>
                <td>
                    {{$value->nama_transportasi}}
                </td>
                <td>
                    {{$value->jumlah_kursi}}
                </td>
                
                <td>
                    {{$value->keterangan}}
                </td>
                <td>
                    {{$value->nama_tipe}}
                </td>
                
                <td class="d-flex">
                    <button class="btn btn-info mr-1" type="button" data-toggle="modal" data-target="#ubahTransportasi{{$x}}">
                        <i class="icon-pencil pr-1"></i>Ubah
                    </button>
                    <form action="{{url('/transportasi', $value->id_transportasi)}}" method="POST">
                        @method('DELETE')
                        @csrf
                        <button class="btn btn-danger" type="submit">
                            <i class="icon-trash pr-1"></i>Hapus
                        </button>
                    </form>
                </td>
            </tr>
            <!-- Modal Ubah Tipe Pekerjaan-->
            <div class="modal fade" id="ubahTransportasi{{$x}}" tabindex="-1" role="dialog" aria-labelledby="ubahTransportasi{{$x}}"
                aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalCenterTitle">Ubah Transportasi</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <form action="{{url('/transportasi', $value->id_transportasi)}}" method="POST">
                            @method('PUT')
                            @csrf
                            <div class="modal-body">
                                <div class="form-group">
                                    <label for="valkode">Kode</label>
                                    <input class="form-control" id="valkode" name="valkode"
                                        type="text" placeholder="Kode" value="{{$value->kode}}">
                                </div>
                                <div class="form-group">
                                    <label for="valnamatransportasi">Nama Transportasi</label>
                                    <input class="form-control" id="valnamatransportasi" name="valnamatransportasi"
                                        type="text" placeholder="Nama Transportasi" value="{{$value->nama_transportasi}}">
                                </div>
                                <div class="form-group">
                                    <label for="valjumlahkursi">Jumlah Kursi</label>
                                    <input class="form-control" id="valjumlahkursi" name="valjumlahkursi"
                                        type="text" placeholder="Jumlah Kursi" value="{{$value->jumlah_kursi}}">
                                </div>
                                <div class="form-group">
                                    <label for="valketerangan">Keterangan</label>
                                    <input class="form-control" id="valketerangan" name="valketerangan"
                                        type="text" placeholder="Keterangan" value="{{$value->keterangan}}">
                                </div>
                                <div class="form-group">
                                <label for="validtipetransportasi">Tipe Transportasi</label>
                                <select name="validtipetransportasi" class="custom-select" required>
                                    <option selected disabled>Tipe Transportasi</option>
                                        @foreach ($data['tipe_transportasi'] as $key => $value)
                                        
                                    <option value="{{$value->id_tipe_transportasi}}">{{$value->nama_tipe}}</option>
                                       
                                        @endforeach
                                </select>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                <button type="submit" class="btn btn-success">Submit</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            @endforeach
            @endif
        </tbody>
        <tfoot>
            <td>Kode</td>
            <td>Nama Transportasi</td>
            <td>Jumlah Kursi</td>
            <td>Keterangan</td>
            <td>Tipe Transportasi</td>
            <td>Action</td>
        </tfoot>
    </table>
</div>
@endsection

@section('modal')
<!-- Modal Tambah Transportasi-->
<div class="modal fade" id="tambahTransportasi" tabindex="-1" role="dialog" aria-labelledby="tambahTransportasi"
    aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalCenterTitle">Tambah Transportasi</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="{{url('/transportasi')}}" method="post">
                @csrf
                <div class="modal-body">
                    <div class="form-group">
                        <label for="valkode">Kode</label>
                        <input class="form-control" id="valkode" name="valkode" type="text"
                            placeholder="Kode">
                    </div>
                    <div class="form-group">
                        <label for="valnamatransportasi">Nama Transportasi</label>
                        <input class="form-control" id="valnamatransportasi" name="valnamatransportasi" type="text"
                            placeholder="Nama Transportasi">
                    </div>
                    <div class="form-group">
                        <label for="valjumlahkursi">Jumlah Kursi</label>
                        <input class="form-control" id="valjumlahkursi" name="valjumlahkursi" type="text"
                            placeholder="Jumlah Kursi">
                    </div>
                    <div class="form-group">
                        <label for="valketerangan">Keterangan</label>
                        <input class="form-control" id="valketerangan" name="valketerangan" type="text"
                            placeholder="Keterangan">
                    </div>
                    <div class="form-group">
                        <label for="validtipetransportasi">Tipe Transportasi</label>
                        <select name="validtipetransportasi" class="custom-select" required>
                            <option selected disabled>Tipe Transportasi</option>
                                @foreach ($data['tipe_transportasi'] as $key => $value)
                            <option value="{{$value->id_tipe_transportasi}}">{{$value->nama_tipe}}</option>
                                @endforeach
                        </select>
                                </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-success">Submit</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
