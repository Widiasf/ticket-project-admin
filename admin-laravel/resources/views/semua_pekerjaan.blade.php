@extends('layout.app')

@section('content')
<div class="jumbotron bg-white">
    <h1>Pekerjaan Anda</h1>
    <table id="daftar_proyek" class="table table-responsive-sm">
        <thead>
            <tr>
                <th>No</th>
                <th>Status</th>
                <th>Pekerjaan</th>
                <th>Deskripsi</th>
                <th>Project</th>
                <th>Versi Project</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            @if (!empty($data['semua_pekerjaan']))
            @foreach ($data['semua_pekerjaan'] as $key => $value)
            <tr>
                <td>{{$key+1}}</td>
                <td>
                    @switch($data['semua_pekerjaan'][$key]->status)
                    @case('approved')
                    <span class="badge badge-success p-2">Approved</span>
                    @break
                    @case('revisi')
                    <span class="badge badge-warning p-2">Revisi</span>
                    @break
                    @case('completed')
                    <span class="badge badge-info p-2">Completed</span>
                    @break
                    @case('pengembangan')
                    <span class="badge badge-primary p-2">Pengembangan</span>
                    @break
                    @case('on progress')
                    <span class="badge badge-primary p-2">On Progress</span>
                    @break
                    @case('on analysis process')
                    <span class="badge badge-secondary p-2">On Analysis Process</span>
                    @break
                    @case('bertanya')
                    <span class="badge badge-secondary p-2">Bertanya</span>
                    @break
                    @case('pending')
                    <span class="badge badge-danger p-2">Pending</span>
                    @break
                    @endswitch
                </td>
                <td>{{$data['semua_pekerjaan'][$key]->{'nama pekerjaan'} }}</td>
                <td>{{$data['semua_pekerjaan'][$key]->description}}</td>
                <td>{{$data['semua_pekerjaan'][$key]->{'project name'} }}</td>
                <td>{{$data['semua_pekerjaan'][$key]->version}}</td>
                <td>
                    <form class="d-flex" action="{{url('/semua-pekerjaan', $data['semua_pekerjaan'][$key]->users_project_id)}}"
                        method="POST">
                        @method('PUT')
                        @csrf
                        <input type="hidden" name="project_id" value="{{$data['semua_pekerjaan'][$key]->project_id}}">
                        <input type="hidden" name="users_id" value="{{$data['semua_pekerjaan'][$key]->users_id}}">
                        <input type="hidden" name="name" value="{{$data['semua_pekerjaan'][$key]->{'nama pekerjaan'} }}">
                        <input type="hidden" name="version" value="{{$data['semua_pekerjaan'][$key]->version}}">
                        <input type="hidden" name="description" value="{{$data['semua_pekerjaan'][$key]->description}}">
                        <select name="status" class="custom-select custom-select-sm mr-1">
                            {{-- <option selected disabled>Status</option> --}}
                            <option value="revisi" {{$data['semua_pekerjaan'][$key]->status=='revisi'?'selected':''}}>Revisi</option>
                            <option value="completed"
                                {{$data['semua_pekerjaan'][$key]->status=='completed'?'selected':''}}>Completed</option>
                            <option value="pengembangan"
                                {{$data['semua_pekerjaan'][$key]->status=='pengembangan'?'selected':''}}>Pengembangan</option>
                            <option value="on progress"
                                {{$data['semua_pekerjaan'][$key]->status=='on progress'?'selected':''}}>On Progress</option>
                            <option value="on analysis process"
                                {{$data['semua_pekerjaan'][$key]->status=='on analysis process'?'selected':''}}>On
                                Analysis Process</option>
                            <option value="bertanya"
                                {{$data['semua_pekerjaan'][$key]->status=='bertanya'?'selected':''}}>Bertanya</option>
                            <option value="pending" {{$data['semua_pekerjaan'][$key]->status=='pending'?'selected':''}}>Pending</option>
                        </select>
                        <button type="submit" class="btn btn-success btn-sm">Ubah Status</button>
                    </form>
                </td>
            </tr>
            @endforeach
            @endif
        </tbody>
        <tfoot>
            <th>No</th>
            <td>Status</td>
            <td>Pekerjaan</td>
            <td>Deskripsi</td>
            <td>Project</td>
            <td>Versi Projek</td>
            <td>Action</td>
        </tfoot>
    </table>
</div>
@endsection
