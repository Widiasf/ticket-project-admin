@extends('layout.app')

@section('content')
<div class="jumbotron bg-white">
    <div class="d-flex justify-content-between">
        <h1>Daftar Rute</h1>
        <div>
            <button class="btn btn-success" type="button" data-toggle="modal" data-target="#tambahRute">
                <i class="icon-plus pr-1"></i>Tambah Rute</button>
        </div>
    </div>
    <table id="daftar_proyek" class="table table-responsive-sm">
        <thead>
            <tr>
                <th>Tujuan</th>
                <th>Rute Awal</th>
                <th>Rute Akhir</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            @if (!is_null($data))
            @php
            $x = 0
            @endphp
            @foreach ($data['rute'] as $key => $value)
            @php
            $x++
            @endphp
            <tr>
                <td>
                    {{$value->tujuan}}
                </td>
                <td>
                    {{$value->rute_awal}}
                </td>
                <td>
                    {{$value->rute_akhir}}
                </td>
                <td class="d-flex">
                    <button class="btn btn-info mr-1" type="button" data-toggle="modal" data-target="#ubahRute{{$x}}">
                        <i class="icon-pencil pr-1"></i>Ubah
                    </button>
                    <form action="{{url('/rute', $value->id_rute)}}" method="POST">
                        @method('DELETE')
                        @csrf
                        <button class="btn btn-danger" type="submit">
                            <i class="icon-trash pr-1"></i>Hapus
                        </button>
                    </form>
                </td>
            </tr>
            <!-- Modal Ubah Tipe Pekerjaan-->
            <div class="modal fade" id="ubahRute{{$x}}" tabindex="-1" role="dialog" aria-labelledby="ubahRute{{$x}}"
                aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalCenterTitle">Ubah Rute</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <form action="{{url('/rute', $value->id_rute)}}" method="POST">
                            @method('PUT')
                            @csrf
                            <div class="modal-body">
                                <div class="form-group">
                                    <label for="valtujuan">Tujuan</label>
                                    <input class="form-control" id="valtujuan" name="valtujuan"
                                        type="text" placeholder="Tujuan" value="{{$value->tujuan}}">
                                </div>
                                <div class="form-group">
                                    <label for="valruteawal">Rute Awal</label>
                                    <input class="form-control" id="valruteawal" name="valruteawal"
                                        type="text" placeholder="Rute Awal" value="{{$value->rute_awal}}">
                                </div>
                                <div class="form-group">
                                    <label for="valruteakhir">Rute Akhir</label>
                                    <input class="form-control" id="valruteakhir" name="valruteakhir"
                                        type="text" placeholder="Rute Akhir" value="{{$value->rute_akhir}}">
                                </div>
                                
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                <button type="submit" class="btn btn-success">Submit</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            @endforeach
            @endif
        </tbody>
        <tfoot>
            <td>Tujuan</td>
            <td>Rute Awal</td>
            <td>Rute Akhir</td>
            <td>Action</td>
        </tfoot>
    </table>
</div>
@endsection

@section('modal')
<!-- Modal Tambah Transportasi-->
<div class="modal fade" id="tambahRute" tabindex="-1" role="dialog" aria-labelledby="tambahRute"
    aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalCenterTitle">Tambah Rute</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="{{url('/rute')}}" method="post">
                @csrf
                <div class="modal-body">
                    <div class="form-group">
                        <label for="valtujuan">Tujuan</label>
                        <input class="form-control" id="valtujuan" name="valtujuan" type="text"
                            placeholder="Kode">
                    </div>
                    <div class="form-group">
                        <label for="valruteawal">Rute Awal</label>
                        <input class="form-control" id="valruteawal" name="valruteawal" type="text"
                            placeholder="Rute Awal">
                    </div>
                    <div class="form-group">
                        <label for="valruteakhir">Rute Akhir</label>
                        <input class="form-control" id="valruteakhir" name="valruteakhir" type="text"
                            placeholder="Rute Akhir">
                    </div>
                    
                    
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-success">Submit</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
