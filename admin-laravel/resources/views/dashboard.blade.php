@extends('layout.app')

@section('content')
<!-- /.Profil & Statistik-->
<div class="row">
    <div class="col-lg-12">
        <div class="card-deck pb-4">
            <div class="card">
                <div class="card-body d-flex">
                    <div class="row">
                        <div class="col-lg-12 d-flex">
                            <div class="row d-flex flex-fill align-items-center">
                                
                                <div class="col-6 col-lg-8 d-flex flex-column justify-content-center">
                                    <h5 id="dashboard-profile-nama">{{$data['user_info']->name}}</h5>
                                    <span id="dashboard-profile-jabatan">{{$data['user_info']->users_level_name}}</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-12 d-flex flex-column justify-content-around py-3">
                            <span class="d-flex align-items-center pb-1">
                                <i class="icon-location-pin dashboard-profile-icons pr-1"></i>
                                <span id="dashboard-profile-alamat">{{$data['user_info']->address}}</span>
                            </span>
                            <span class="d-flex align-items-center pb-1">
                                <i class="icon-envelope dashboard-profile-icons pr-1"></i>
                                <span id="dashboard-profile-email">{{$data['user_info']->email}}</span>
                            </span>
                            <span class="d-flex align-items-center">
                                <i class="icon-phone dashboard-profile-icons pr-1"></i>
                                <span id="dashboard-profile-nohp">{{$data['user_info']->no_telp}}</span>
                            </span>
                        </div>
                        <div class="col-lg-12 d-flex justify-content-end align-items-center">
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /.col-->
</div>

@endsection

@section('script')
<script>
    $(function () {
        statistik_pekerjaan();
    });

    function statistik_pekerjaan() {
        $.ajax({
            url: `http://localhost:2121/countPekerjaan/${$('#dashboard-statistik-karyawan').val()}`,
            method: "GET",
            success: function (result) {
                $('#dashboard-statistik-selesai').html(result.data.Selesai)
                $('#dashboard-statistik-dikerjakan').html(result.data.Dikerjakan)
                $('#dashboard-statistik-pending').html(result.data.Pending)
            },
            error: function (err) {
                console.log(err);
            }
        });
    }

    $('#dashboard-statistik-karyawan').change(function (e) {
        statistik_pekerjaan();
    });

</script>
@endsection
