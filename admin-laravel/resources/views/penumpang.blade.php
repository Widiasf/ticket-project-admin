@extends('layout.app')

@section('content')
<div class="jumbotron bg-white">
    <div class="d-flex justify-content-between">
        <h1>Daftar Penumpang</h1>
    </div>
    <table id="daftar_petugas" class="table table-responsive-sm">
        <thead>
            <tr>
                <th>Nama</th>
                <th>Username</th>
                <th>Alamat</th>
                <th>Tanggal Lahir</th>
                <th>Jenis Kelamin</th>
                <th>Telefone</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            @if (!is_null($data))
            @php
            $x = 0
            @endphp
            @foreach ($data['penumpang'] as $key => $value)
            @php
            $x++
            @endphp
            <tr>
                <td>
                    {{$value->nama_penumpang}}
                </td>
                <td>
                    {{$value->username}}
                </td>
                <td>
                    {{$value->alamat_penumpang}}
                </td>
                <td>
                    {{$value->tanggal_lahir}}
                </td>
                <td>
                    {{$value->jenis_kelamin}}
                </td>
                <td>
                    {{$value->telefone}}
                </td>
                <td class="d-flex">
                    
                    <form action="{{url('/penumpang', $value->id_penumpang)}}" method="POST">
                        @method('DELETE')
                        @csrf
                        <button class="btn btn-danger" type="submit">
                            <i class="icon-trash pr-1"></i>Hapus
                        </button>
                    </form>
                </td>
            </tr>
            @endforeach
            @endif
        </tbody>
        <tfoot>
        <td>Nama</td>
                <td>Username</td>
                <td>Alamat</td>
                <td>Tanggal Lahir</td>
                <td>Jenis Kelamin</td>
                <td>Telefone</td>
            <td>Action</td>
        </tfoot>
    </table>
</div>
@endsection
