<!DOCTYPE html>
<!--
* CoreUI - Free Bootstrap Admin Template
* @version v2.1.8
* @link https://coreui.io
* Copyright (c) 2018 creativeLabs Łukasz Holeczek
* Licensed under MIT (https://coreui.io/license)
-->

<html lang="en">

<head>
    <base href="./">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <meta name="description" content="Halaman admin untuk manajemen konten aplikasi android 'Laporin'">
    <meta name="author" content="PEMKOT CIMAHI">
    <meta name="keyword" content="pemkot, pemerintah, kota, cimahi, bandung, jawa, barat, layanan, masyarakat, laporin, keluhan, solusi, aplikasi, admin">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>TMS Dashboard | KAABA</title>
    <!-- Icons-->
    <link rel="stylesheet" href="{{url('css/coreui-icons.min.css')}}">
    <link rel="stylesheet" href="{{url('css/flag-icon.min.css')}}">
    <link rel="stylesheet" href="{{url('css/font-awesome.min.css')}}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/simple-line-icons/2.4.1/css/simple-line-icons.css">
    <!-- Main styles for this application-->
    <link rel="stylesheet" href="{{url('css/coreui.min.css')}}">
    <link rel="stylesheet" href="{{url('css/pace.min.css')}}">
    <link rel="stylesheet" href="{{url('css/datatables.min.css')}}">
    <link rel="stylesheet" href="{{url('css/custom-pace.css')}}">
    <link rel="stylesheet" href="{{url('css/style.css')}}">
    <link rel="stylesheet" href="{{url('css/datepicker.min.css')}}">
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet"
        integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <!-- Global site tag (gtag.js) - Google Analytics-->
    <script async="" src="https://www.googletagmanager.com/gtag/js?id=UA-118965717-3"></script>
    <script>
        window.dataLayer = window.dataLayer || [];

      function gtag() {
        dataLayer.push(arguments);
      }
      gtag('js', new Date());
      // Shared ID
      gtag('config', 'UA-118965717-3');
      // Bootstrap ID
      gtag('config', 'UA-118965717-5');
    </script>
</head>

<body class="app header-fixed sidebar-fixed sidebar-lg-show">
    <header class="app-header navbar">
        <button class="navbar-toggler sidebar-toggler d-lg-none mr-auto" type="button" data-toggle="sidebar-show">
            <span class="navbar-toggler-icon"></span>
        </button>
        <a class="navbar-brand" href="{{url('/')}}">
            <!-- <img class="navbar-brand-full" src="img/brand/logo.svg" width="89" height="25" alt="CoreUI Logo">
            <img class="navbar-brand-minimized" src="img/brand/sygnet.svg" width="30" height="30" alt="CoreUI Logo"> -->
            <img src="{{url('img/logo_kaaba.png')}}" class="img-fluid mr-2" width="30px" alt="logo_kaaba">
            <small class="navbar-brand-full">E-Ticket</small>
        </a>
        <button class="navbar-toggler sidebar-toggler d-md-down-none" type="button" data-toggle="sidebar-lg-show">
            <span class="navbar-toggler-icon"></span>
        </button>
        <ul class="nav navbar-nav ml-auto">
            <li class="nav-item dropdown">
                <a class="nav-link" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                    <div class="rounded-circle" style="width: 40px;height: 40px;background-position: center;background-image: url({{url(session('photo_profile'))}})"
                        alt="profile-pic">
                    </div>
                </a>
                <div class="dropdown-menu dropdown-menu-right">
                    <a class="dropdown-item" href="{{url('/edit-profile')}}">
                        <i class="fa fa-user"></i> Profile</a>
                    <form action="{{url('/logout')}}" method="post">
                        @csrf
                        <button class="dropdown-item btn" type="submit">
                            <i class="fa fa-lock"></i> Logout</button>
                    </form>
                </div>
            </li>
        </ul>
    </header>
    <div class="app-body">
        <div class="sidebar">
            <nav class="sidebar-nav">
                <ul class="nav">
                    <li class="nav-item">
                        <a class="nav-link py-4" href="{{url('/')}}">
                            <i class="nav-icon icon-drawer"></i> Profile
                        </a>
                    </li>
                    <li class="nav-item {{session('level')=='Karyawan' ? 'd-none':''}}">
                        <a class="nav-link py-4" href="{{url('/pemesanan')}}">
                            <i class="nav-icon icon-drawer"></i> Pemesanan</a>
                    </li>
                    <li class="nav-item {{session('level')=='Karyawan' ? 'd-none':''}}">
                        <a class="nav-link py-4" href="{{url('/pemberangkatan')}}">
                            <i class="nav-icon icon-drawer"></i> Pemberangkatan</a>
                    </li>
                    <li class="nav-item {{session('level')=='Karyawan' ? 'd-none':''}}">
                        <a class="nav-link py-4" href="{{url('/penumpang')}}">
                            <i class="nav-icon icon-drawer"></i> Penumpang</a>
                    </li>
                    <li class="nav-item {{session('level')=='Karyawan' ? 'd-none':''}}">
                        <a class="nav-link py-4" href="{{url('/petugas')}}">
                            <i class="nav-icon icon-drawer"></i> Petugas</a>
                    </li>
                    <li class="nav-item {{session('level')=='Karyawan' ? 'd-none':''}}">
                        <a class="nav-link py-4" href="{{url('/level')}}">
                            <i class="nav-icon icon-drawer"></i> Level Petugas</a>
                    </li>
                    <li class="nav-item {{session('level')=='Karyawan' ? 'd-none':''}}">
                        <a class="nav-link py-4" href="{{url('/transportasi')}}">
                            <i class="nav-icon icon-drawer"></i> Transportasi</a>
                    </li>
                    <li class="nav-item {{session('level')=='Karyawan' ? 'd-none':''}}">
                        <a class="nav-link py-4" href="{{url('/tipe-transportasi')}}">
                            <i class="nav-icon icon-drawer"></i> Tipe Transportasi</a>
                    </li>
                    <li class="nav-item {{session('level')=='Karyawan' ? 'd-none':''}}">
                        <a class="nav-link py-4" href="{{url('/rute')}}">
                            <i class="nav-icon icon-drawer"></i> Rute</a>
                    </li>
                </ul>
            </nav>
            <button class="sidebar-minimizer brand-minimizer" type="button"></button>
        </div>
        <main class="main pt-4">
            <div class="container-fluid">
                <div class="animated fadeIn">
                    @include('layout.flash-message')
                    @yield('content')
                </div>
            </div>
        </main>
    </div>
    <footer class="app-footer">
        <div>
            <a href="https://coreui.io">CoreUI</a>
            <span>&copy; 2018 creativeLabs.</span>
        </div>
        <div class="ml-auto">
            <span>Powered by</span>
            <a href="https://coreui.io">CoreUI</a>
        </div>
    </footer>

    @yield('modal')

    <!-- CoreUI and necessary plugins-->
    <script src="{{url('js/pace.min.js')}}"></script>
    <script src="{{url('js/perfect-scrollbar.min.js')}}"></script>

    <script src="{{url('js/jquery-3.3.1.min.js')}}"></script>
    <script src="{{url('js/popper.min.js')}}"></script>
    <script src="{{url('js/bootstrap.min.js')}}"></script>
    <script src="{{url('js/coreui.min.js')}}"></script>
    <script src="{{url('js/datepicker.min.js')}}"></script>

    <script src="{{url('js/datatables.min.js')}}"></script>
    <script>
        $(function () {
            $('#daftar_proyek').DataTable()

            $('.custom-file-input').change(function (e) {
                var files = $(this).prop("files")
                var names = $.map(files, function (val) {
                    return val.name;
                });

                $(this).next().html(names.join());
            });
        });

    </script>
    @yield('script')
    {{-- Notification --}}
    <script>
        $('#notif-button').click(function (e) {
            $('#notif-count').html('');
            $.ajax({
                url: `http://localhost:2121/updateNotif/{{session('user_id')}}`,
                method: "PUT",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data: {
                    "_method": 'PUT',
                    "_token": $('meta[name="csrf-token"]').attr('content')
                },
                success: function (result) {
                    console.log(result);
                },
                error: function (err) {
                    console.log(err);
                }
            });
        });

        function template_notif(name) {
            return `<a class="dropdown-item text-truncates" href="{{url('/semua-pekerjaan')}}">
                        <div class="d-flex flex-row align-items-center">
                            <div><i class="icon-note text-success"></i></div>
                            <div>${name} menambahkan <br> pekerjaan untuk anda</div>
                        </div>
                    </a>`
        }

        function load_unseen_notification() {
            $.ajax({
                url: `http://localhost:2121/readNotif/{{session('user_id')}}`,
                method: "GET",
                success: function (result) {
                    if (result.read_false > 0) {
                        $('#notif-count').show()
                        $('#notif-count').html(result.read_false);
                    } else {
                        $('#notif-count').hide()
                    }
                    var content = ''
                    result.data.forEach(element => {
                        content += template_notif(element.username)
                    });
                    $('#notif-fetch').html(content)
                    console.log(result);
                },
                error: function (err) {
                    console.log(err);
                }
            });
        }

        $(function () {
            load_unseen_notification()
        });
        setInterval(function () {
            load_unseen_notification();
        }, 2000);

    </script>
</body>

</html>
