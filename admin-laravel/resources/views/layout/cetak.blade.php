<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    {{--
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
        crossorigin="anonymous" media="all"> --}}

    <title>Cetak Report Pemesanan</title>
</head>

<body>
    <div class="card d-inline-flex">
        <div class="card-header text-center">
            {{-- <img class="mb-3" style="width: 50px" src="{{url('img/logo_kaaba.png')}}" alt=""> --}}
            <h2>Pemesanan {{$pemesanan->kode_pemesanan}}</h2>
        </div>
        <div class="card-body d-flex flex-column align-items-between">
            <div class="border-bottom pb-3">
                <div class="d-flex">
                    <div class="d-flex justify-content-between mr-2" style="width: 300px">
                        <span>Status</span>
                        <span>:</span>
                        @if ($pemesanan->status=='Valid')
                        <span class="badge badge-success p-2">Valid</span>
                        @else
                        <span class="badge badge-primary p-2">Progress</span>
                        @endif
                    </div>
                </div>
                <div class="d-flex">
                    <div class="d-flex justify-content-between mr-2" style="width: 300px">
                        <span>Kode Pemesanan</span>
                        <span>:</span>
                        <span>{{$pemesanan->kode_pemesanan}}</span>
                    </div>
                </div>
                <div class="d-flex">
                    <div class="d-flex justify-content-between mr-2" style="width: 300px">
                        <span>Tanggal Pemesanan</span>
                        <span>:</span>
                        <span>{{$pemesanan->tanggal_pemesanan}}</span>
                    </div>
                </div>
                <div class="d-flex">
                    <div class="d-flex justify-content-between mr-2" style="width: 300px">
                        <span>Tempat Pemesanan</span>
                        <span>:</span>
                        <span>{{$pemesanan->tempat_pemesanan}}</span>
                    </div>
                </div>
                <div class="d-flex">
                    <div class="d-flex justify-content-between mr-2" style="width: 300px">
                        <span>Kode Kursi</span>
                        <span>:</span>
                        <span>{{$pemesanan->kode_kursi}}</span>
                    </div>
                </div>
                <div class="d-flex">
                    <div class="d-flex justify-content-between mr-2" style="width: 300px">
                        <span>Id Pemberangkatan</span>
                        <span>:</span>
                        <span>{{$pemesanan->id_pemberangkatan}}</span>
                    </div>
                </div>
                <div class="d-flex">
                    <div class="d-flex justify-content-between mr-2" style="width: 300px">
                        <span>Nama Penumpang</span>
                        <span>:</span>
                        <span>{{$pemesanan->nama_penumpang}}</span>
                    </div>
                </div>
                <div class="d-flex">
                    <div class="d-flex justify-content-between mr-2" style="width: 300px">
                        <span>Tujuan</span>
                        <span>:</span>
                        <span>{{$pemesanan->tujuan}}</span>
                    </div>
                </div>
                
        </div>
        <div class="card-footer text-muted">
            &copy; E-TICKET 2019
        </div>
    </div>
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    {{-- <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
        crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
        crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
        crossorigin="anonymous"></script> --}}
</body>

</html>
