@extends('layout.app')

@section('content')
<div class="jumbotron bg-white">
    <div class="d-flex justify-content-between">
        <h1>Daftar Tipe Pekerjaan</h1>
        <div>
            <button class="btn btn-success" type="button" data-toggle="modal" data-target="#tambahTipePekerjaan">
                <i class="icon-plus pr-1"></i>Tambah Tipe Pekerjaan</button>
        </div>
    </div>
    <table id="daftar_proyek" class="table table-responsive-sm">
        <thead>
            <tr>
                <th>Nama Tipe Pekerjaan</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            @if (!is_null($data))
            @php
            $x = 0
            @endphp
            @foreach ($data['tipe_pekerjaan'] as $key => $value)
            @php
            $x++
            @endphp
            <tr>
                <td>
                    {{$value->name}}
                </td>
                <td class="d-flex">
                    <button class="btn btn-info mr-1" type="button" data-toggle="modal" data-target="#ubahTipePekerjaan{{$x}}">
                        <i class="icon-pencil pr-1"></i>Ubah
                    </button>
                    <form action="{{url('/tipe-pekerjaan', $value->category_id)}}" method="POST">
                        @method('DELETE')
                        @csrf
                        <button class="btn btn-danger" type="submit">
                            <i class="icon-trash pr-1"></i>Hapus
                        </button>
                    </form>
                </td>
            </tr>
            <!-- Modal Ubah Tipe Pekerjaan-->
            <div class="modal fade" id="ubahTipePekerjaan{{$x}}" tabindex="-1" role="dialog" aria-labelledby="ubahTipePekerjaan{{$x}}"
                aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalCenterTitle">Ubah Tipe Pekerjaan</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <form action="{{url('/tipe-pekerjaan', $value->category_id)}}" method="POST">
                            @method('PUT')
                            @csrf
                            <div class="modal-body">
                                <div class="form-group">
                                    <label for="nama_tipe_pekerjaan">Nama Tipe Pekerjaan</label>
                                    <input class="form-control" id="nama_tipe_pekerjaan" name="nama_tipe_pekerjaan"
                                        type="text" placeholder="Nama Tipe Pekerjaan" value="{{$value->name}}">
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                <button type="submit" class="btn btn-success">Submit</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            @endforeach
            @endif
        </tbody>
        <tfoot>
            <td>Nama Tipe Pekerjaan</td>
            <td>Action</td>
        </tfoot>
    </table>
</div>
@endsection

@section('modal')
<!-- Modal Tambah Tipe Pekerjaan-->
<div class="modal fade" id="tambahTipePekerjaan" tabindex="-1" role="dialog" aria-labelledby="tambahTipePekerjaan"
    aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalCenterTitle">Tambah Tipe Pekerjaan</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="{{url('/tipe-pekerjaan')}}" method="post">
                @csrf
                <div class="modal-body">
                    <div class="form-group">
                        <label for="nama_tipe_pekerjaan">Nama Tipe Pekerjaan</label>
                        <input class="form-control" id="nama_tipe_pekerjaan" name="nama_tipe_pekerjaan" type="text"
                            placeholder="Nama Tipe Pekerjaan">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-success">Submit</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
