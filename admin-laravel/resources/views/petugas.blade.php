@extends('layout.app')

@section('content')
<div class="jumbotron bg-white">
    <div class="d-flex justify-content-between">
        <h1>Daftar Petugas</h1>
        <div>
            <button class="btn btn-success" type="button" data-toggle="modal" data-target="#tambahPetugas">
                <i class="icon-plus pr-1"></i>Tambah Petugas</button>
        </div>
    </div>
    <table id="daftar_petugas" class="table table-responsive-sm">
        <thead>
            <tr>
                <th>Nama Petugas</th>
                <th>Username</th>
                <th>Level</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            @if (!is_null($data))
            @php
            $x = 0
            @endphp
            @foreach ($data['petugas'] as $key => $value)
            @php
            $x++
            @endphp
            <tr>
                <td>
                    {{$value->nama_petugas}}
                </td>
                <td>
                    {{$value->username}}
                </td>
                <td>
                    {{$value->nama_level}}
                </td>
                <td class="d-flex">
                    
                    <form action="{{url('/petugas', $value->id_petugas)}}" method="POST">
                        @method('DELETE')
                        @csrf
                        <button class="btn btn-danger" type="submit">
                            <i class="icon-trash pr-1"></i>Hapus
                        </button>
                    </form>
                </td>
            </tr>
            <!-- Modal Ubah Tipe Pekerjaan-->
            <div class="modal fade" id="ubahPetugas{{$x}}" tabindex="-1" role="dialog" aria-labelledby="ubahPetugas{{$x}}"
                aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalCenterTitle">Ubah Petugas</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <form action="{{url('/petugas', $value->id_petugas)}}" method="POST">
                            @method('PUT')
                            @csrf
                            <div class="modal-body">
                                <div class="form-group">
                                    <label for="nama_petugas">Nama Petugas</label>
                                    <input class="form-control" id="nama_petugas" name="nama_petugas"
                                        type="text" placeholder="Nama Petugas" value="{{$value->nama_petugas}}">
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                <button type="submit" class="btn btn-success">Submit</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            @endforeach
            @endif
        </tbody>
        <tfoot>
            <td>Nama Petugas</td>
            <td>Username</td>
            <td>Level</td>
            <td>Action</td>
        </tfoot>
    </table>
</div>
@endsection

@section('modal')
<!-- Modal Tambah Tipe Pekerjaan-->
<div class="modal fade" id="tambahPetugas" tabindex="-1" role="dialog" aria-labelledby="tambahPetugas"
    aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalCenterTitle">Tambah Petugas</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="{{url('/petugas')}}" method="post">
                @csrf
                <div class="modal-body">
                <div class="form-group">
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text">
                                <i class="fa fa-pencil-square"></i>
                            </span>
                        </div>
                        <input class="form-control" type="text" name="valnama_petugas"
                            placeholder="Nama" required>
                    </div>
                </div>
                <div class="form-group">
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text">
                                <i class="fa fa-pencil-square"></i>
                            </span>
                        </div>
                        <input class="form-control" type="text" name="valusername"
                            placeholder="Username" required>
                    </div>
                </div>
                <div class="form-group">
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text">
                                <i class="fa fa-pencil-square"></i>
                            </span>
                        </div>
                        <input class="form-control" type="password" name="valpassword"
                            placeholder="Password" required>
                    </div>
                </div>
                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text">
                                <i class="fa fa-user"></i>
                                </span>
                            </div>
                                <select name="id_level" class="custom-select" required>
                                    <option selected disabled>Pilih Level</option>
                                        @foreach ($data['level'] as $key => $value)
                                    <option value="{{$value->id_level}}">{{$value->nama_level}}</option>
                                        @endforeach
                                </select>
                             </div>
                         </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-success">Submit</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
