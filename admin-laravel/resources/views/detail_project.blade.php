@extends('layout.app')

@section('content')
<div class="row">
    <div class="col-sm-7">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header bg-primary text-center p-4">
                        <h2 class="m-0">{{$data['detail_project']->name}}</h2>
                    </div>
                    <div class="card-body d-flex flex-column">
                        <div class="d-flex">
                            <div class="d-flex flex-column pr-5">
                                <div class="d-flex pb-1 align-items-center">
                                    <div class="d-flex justify-content-between mr-2" style="width: 120px">
                                        <span>Status</span>
                                        <span>:</span>
                                    </div>
                                    @if ($data['detail_project']->status=='completed')
                                    <span class="badge badge-success p-2">Completed</span>
                                    @else
                                    <span class="badge badge-primary p-2">Progress</span>
                                    @endif
                                </div>
                                <div class="d-flex pb-1">
                                    <div class="d-flex justify-content-between mr-2" style="width: 120px">
                                        <span>Tanggal Mulai</span>
                                        <span>:</span>
                                    </div>
                                    <span>{{$data['detail_project']->tanggal_mulai}}</span>
                                </div>
                                <div class="d-flex pb-1">
                                    <div class="d-flex justify-content-between mr-2" style="width: 120px">
                                        <span>Tanggal Selesai</span>
                                        <span>:</span>
                                    </div>
                                    <span>{{$data['detail_project']->tanggal_berakhir}}</span>
                                </div>
                                <div class="d-flex pb-1">
                                    <div class="d-flex justify-content-between mr-2" style="width: 120px">
                                        <span>Dibuat pada</span>
                                        <span>:</span>
                                    </div>
                                    <span>{{$data['detail_project']->created_at}}</span>
                                </div>
                                <div class="d-flex pb-1">
                                    <div class="d-flex justify-content-between mr-2" style="width: 120px">
                                        <span>Terakhir diubah</span>
                                        <span>:</span>
                                    </div>
                                    <span>{{$data['detail_project']->updated_at}}</span>
                                </div>
                            </div>
                            <div class="d-flex flex-column">
                                <div class="d-flex pb-1">
                                    <div class="d-flex justify-content-between mr-2" style="width: 120px">
                                        <span>Versi</span>
                                        <span>:</span>
                                    </div>
                                    <span>{{$data['detail_project']->version}}</span>
                                </div>
                                <div class="d-flex pb-1 align-items-center">
                                    <div class="d-flex justify-content-between mr-2" style="width: 120px">
                                        <span>Cetak report</span>
                                        <span>:</span>
                                    </div>
                                    <div>
                                        <form action="{{url('/cetak')}}" method="post">
                                            @csrf
                                            <input type="hidden" name="id" value="{{$data['detail_project']->project_id}}">
                                            <button class="btn btn-default btn-sm" type="submit"><i class="fa fa-print"></i> Cetak</button>
                                        </form>
                                    </div>
                                </div>
                                <div class="d-flex flex-column justify-content-center">
                                    <span>Anggota Tim</span>
                                    <div class="d-flex justify-content-start pb-1">
                                        @if (!empty($data['karyawan']))
                                        @foreach ($data['karyawan'] as $key => $value)
                                        @if ($key<4) <img style="width:50px;height:50px"src="{{$value->photo_profile}}" alt="{{$value->username}}"
                                            data-toggle="tooltip" data-placement="top" title="{{$value->name}}" class="mr-1">
                                            @else
                                    </div>
                                    <div class="d-flex justify-content-between pb-1">
                                        @endif
                                        @endforeach
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="d-flex justify-content-end pt-5">
                            <button class="btn btn-primary mr-1" type="button" data-toggle="modal" data-target="#listPekerjaan">
                                <i class="icon-menu pr-1"></i>List Pekerjaan</button>
                            {{-- List Pekerjaan --}}
                            <div class="modal fade" id="listPekerjaan" tabindex="-1" role="dialog" aria-labelledby="listPekerjaan"
                                aria-hidden="true">
                                <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="listPekerjaanTitle">Daftar Pekerjaan Project :
                                                <b>{{$data['detail_project']->name}}</b></h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <table class="table table-responsive-sm">
                                                <thead>
                                                    <tr>
                                                        <th>No</th>
                                                        <th>Username</th>
                                                        <th>Tugas</th>
                                                        <th>Version</th>
                                                        <th>Description</th>
                                                        <th>Status</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach ($data['list_pekerjaan'] as $key => $value)
                                                    @if (!empty($value))
                                                    <tr>
                                                        <td>
                                                            {{$key+1}}
                                                        </td>
                                                        <td>
                                                            {{$value->username}}
                                                        </td>
                                                        <td>
                                                            {{$value->name}}
                                                        </td>
                                                        <td>
                                                            {{$value->version}}
                                                        </td>
                                                        <td>
                                                            {{$value->description}}
                                                        </td>
                                                        <td>
                                                            {{$value->status}}
                                                        </td>
                                                    </tr>
                                                    @endif
                                                    @endforeach
                                                </tbody>
                                                <tfoot>
                                                    <tr>
                                                        <th>No</th>
                                                        <th>Username</th>
                                                        <th>Tugas</th>
                                                        <th>Version</th>
                                                        <th>Description</th>
                                                        <th>Status</th>
                                                    </tr>
                                                </tfoot>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @if (session('level')!='Karyawan')
                            <button class="btn btn-success mr-1" type="button" data-toggle="modal" data-target="#tambahPekerjaan">
                                <i class="icon-plus pr-1"></i>Tambah Pekerjaan</button>
                            @endif
                            {{-- Tambah Pekerjaan --}}
                            <div class="modal fade" id="tambahPekerjaan" tabindex="-1" role="dialog" aria-labelledby="tambahPekerjaan"
                                aria-hidden="true">
                                <div class="modal-dialog modal-dialog-centered" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="tambahPekerjaanTitle">Tambah Pekerjaan
                                                {{$data['detail_project']->name}}</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <form action="{{url('/tambah-pekerjaan')}}" method="post">
                                            @csrf
                                            <input type="hidden" name="project_id" value="{{$data['detail_project']->project_id}}">
                                            <input type="hidden" name="version" value="{{$data['detail_project']->version}}">
                                            <input type="hidden" name="status" value="pending">
                                            <div class="modal-body">
                                                <div class="form-group">
                                                    <div class="input-group">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text">
                                                                <i class="fa fa-user"></i>
                                                            </span>
                                                        </div>
                                                        <select name="user" class="custom-select"
                                                            required>
                                                            <option selected disabled>Pilih User</option>
                                                            @foreach ($data['users'] as $key => $value)
                                                            @if ($value->users_id != session('user_id'))
                                                            <option value="{{$value->users_id}}">{{$value->username}}</option>
                                                            @endif
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="input-group">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text">
                                                                <i class="fa fa-pencil-square"></i>
                                                            </span>
                                                        </div>
                                                        <input class="form-control" type="text" name="nama_tugas"
                                                            placeholder="Nama Tugas" required>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="input-group">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text">
                                                                <i class="fa fa-list"></i>
                                                            </span>
                                                        </div>
                                                        <select name="tipe_pekerjaan" class="custom-select"
                                                            required>
                                                            <option selected disabled>Pilih Tipe Pekerjaan</option>
                                                            @foreach ($data['tipe_pekerjaan'] as $key => $value)
                                                            <option value="{{$value->category_id}}">{{$value->name}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                                <textarea class="form-control" name="deskripsi" rows="5" placeholder="Deskripsi Pekerjaan" required></textarea>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                <button type="submit" class="btn btn-success">Submit</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            @if (session('level')!='Supervisor' && session('level')!='Karyawan')
                            <button class="btn btn-warning mr-1" type="button" data-toggle="modal" data-target="#ubahProject">
                                <i class="icon-pencil pr-1"></i>Ubah Project</button>
                            @endif
                            {{-- Ubah Project --}}
                            <div class="modal fade" id="ubahProject" tabindex="-1" role="dialog" aria-labelledby="ubahProject"
                                aria-hidden="true">
                                <div class="modal-dialog modal-dialog-centered" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="ubahProjectTitle">Ubah Project <b>{{$data['detail_project']->name}}</b></h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <form action="{{url('/project', $data['detail_project']->project_id)}}" method="post">
                                            @method('PUT')
                                            @csrf
                                            <input type="hidden" name="status" value="{{$data['detail_project']->status}}">
                                            <div class="modal-body">
                                                <div class="form-group">
                                                    <div class="input-group">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text">
                                                                <i class="fa fa-pencil-square"></i>
                                                            </span>
                                                        </div>
                                                        <input class="form-control" type="text" name="nama_project"
                                                            placeholder="Nama Project" value="{{$data['detail_project']->name}}"
                                                            required>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="input-group">
                                                        <div class="input-group-append">
                                                            <span class="input-group-text">
                                                                <i class="fa fa-calendar" aria-hidden="true"></i>
                                                            </span>
                                                        </div>
                                                        <input name="tanggal_mulai" data-toggle="datepicker" type="text"
                                                            class="form-control docs-date" placeholder="Tanggal Mulai"
                                                            value="{{$data['detail_project']->start_date}}" required>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="input-group">
                                                        <div class="input-group-append">
                                                            <span class="input-group-text">
                                                                <i class="fa fa-calendar" aria-hidden="true"></i>
                                                            </span>
                                                        </div>
                                                        <input name="tanggal_berakhir" data-toggle="datepicker" type="text"
                                                            class="form-control docs-date" placeholder="Tanggal Berakhir"
                                                            value="{{$data['detail_project']->end_date}}" required>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="input-group">
                                                        <div class="input-group-append">
                                                            <span class="input-group-text">
                                                                <i class="fa fa-align-justify" aria-hidden="true"></i>
                                                            </span>
                                                        </div>
                                                        <textarea class="form-control" name="deskripsi" rows="3"
                                                            placeholder="Deskripsi Project" required>{{$data['detail_project']->description}}</textarea>
                                                    </div>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                    <button type="submit" class="btn btn-success">Submit</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12">
                <div class="card">
                    <div class="card-header d-flex justify-content-between align-items-center">
                        <div>
                            <i class="icon-docs pr-1"></i> File Project
                        </div>
                        <div>
                            <button class="btn btn-primary btn-sm" type="button" data-toggle="modal" data-target="#tambahFile">
                                <i class="icon-plus pr-1"></i>Tambah File</button>
                        </div>
                    </div>
                    <div class="card-body d-flex flex-column">
                        @if (empty($data['files']))
                        <span>Tidak ada file</span>
                        @else
                        @foreach ($data['files'] as $key => $value)
                            <div class="d-flex align-items-center pb-1">
                                <form action="{{url('/delete-file', $value->file_id)}}" method="post">
                                    @method('PUT')
                                    @csrf
                                    <div><button class="btn btn-danger btn-sm mr-3" type="submit"><i class="fa fa-trash fa-lg"></i></button></div>
                                </form>
                                <i class="icon-doc pr-3"></i>
                                <span>{{$value->username}} : </span>
                                <a href="{{url($value->file)}}" class="pl-2" download>{{$value->name}}</a>
                            </div>
                        @endforeach
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-sm-5 d-fslex">
        <div class="card">
            <div class="card-header">
                <i class="fa fa-history"></i> Activity Log
            </div>
            <div class="card-body">
                <!-- <div>
                        <img class="pr-1" src="https://via.placeholder.com/30" alt="">
                        <span>01/14/2019 14:20:25</span>
                        <br>
                        <b>Ne-Yo</b>
                        <span>menambah file</span>
                        <b>user-4.jpg</b>
                    </div> -->

                <table id="activity_log" class="table table-responsive-sm">
                    <thead>
                        <tr>
                            <th>Date</th>
                            <th>User</th>
                            <th>Activity</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if (!empty($data['log']))
                            @foreach ($data['log'] as $key => $value)
                                <tr>
                                    <td>
                                        <span>{{$value->created_at}}</span>
                                    </td>
                                    <td>
                                        <div class="rounded-circle pr-1" style="width: 40px;height: 40px;background-position: center;background-image: url({{url($value->photo_profile)}})" alt="{{$value->username}}">
                                        </div>
                                    </td>
                                    <td>
                                        <span>{{$value->username}}</span>
                                        <b>{{$value->type}}</b>
                                    </td>
                                </tr>
                            @endforeach
                        @endif
                    </tbody>
                    <tfoot>
                        <tr>
                            <td>Date</td>
                            <td>User</td>
                            <td>Activity</td>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection

@section('modal')
<!-- Modal Tambah File -->
<div class="modal fade" id="tambahFile" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
    aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalCenterTitle">Tambah File</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="{{url('/tambah-file')}}" method="post" enctype="multipart/form-data">
                @csrf
                <input type="hidden" name="project_id" value="{{$data['detail_project']->project_id}}">
                <div class="modal-body">
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="foto_profil_addon"><i class="icon-doc"></i></span>
                        </div>
                        <div class="custom-file">
                            <input type="file" class="custom-file-input" id="file_project" name="file_project"
                                aria-describedby="file_project_addon">
                            <label class="custom-file-label" for="file_project">File</label>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-success">Submit</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection

@section('script')
<script>
    $(function () {
        $('#activity_log').DataTable({
            scrollY: '50vh',
            scrollCollapse: true,
            paging: false
        })

        $('#activity_log_wrapper').children('div:first').children('div:first').remove()
    });

    var date = $('[data-toggle="datepicker"]').datepicker({
        format: 'yyyy-mm-dd'
    });

</script>
@endsection
