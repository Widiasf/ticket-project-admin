@extends('layout.app')

@section('content')
<div class="jumbotron bg-white">
    <div class="d-flex justify-content-between">
        <h1>Daftar Project</h1>
        <div>
            @if (session('level')!='Supervisor' && session('level')!='Karyawan')
            <button class="btn btn-success" type="button" data-toggle="modal" data-target="#tambahProject">
                <i class="icon-plus pr-1"></i>Tambah Project
            </button>
            @endif
        </div>
    </div>
    <div class="modal fade" id="tambahProject" tabindex="-1" role="dialog" aria-labelledby="tambahProject" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="tambahProjectTitle">Tambah Project</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="{{url('/project')}}" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="modal-body">
                        <div class="form-group">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">
                                        <i class="fa fa-pencil-square"></i>
                                    </span>
                                </div>
                                <input class="form-control" id="email" type="text" name="nama_project" placeholder="Nama Project"
                                    required>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group">
                                <div class="input-group-append">
                                    <span class="input-group-text">
                                        <i class="fa fa-calendar" aria-hidden="true"></i>
                                    </span>
                                </div>
                                <input name="tanggal_mulai" data-toggle="datepicker" type="text" class="form-control docs-date"
                                    placeholder="Tanggal Mulai" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group">
                                <div class="input-group-append">
                                    <span class="input-group-text">
                                        <i class="fa fa-calendar" aria-hidden="true"></i>
                                    </span>
                                </div>
                                <input name="tanggal_berakhir" data-toggle="datepicker" type="text" class="form-control docs-date"
                                    placeholder="Tanggal Berakhir" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">
                                        <i class="fa fa-level-up"></i>
                                    </span>
                                </div>
                                <select class="custom-select" name="supervisor">
                                    <option selected disabled>Pilih Supervisor</option>
                                    @foreach ($data['supervisor'] as $value)
                                    <option value="{{$value->users_id}}">{{$value->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group">
                                <div class="input-group-append">
                                    <span class="input-group-text">
                                        <i class="fa fa-align-justify" aria-hidden="true"></i>
                                    </span>
                                </div>
                                <textarea class="form-control" name="deskripsi" rows="3" placeholder="Deskripsi Project"
                                    required></textarea>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-success">Submit</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <table id="daftar_proyek" class="table table-responsive-sm">
        <thead>
            <tr>
                <th>No</th>
                <th>Nama Project</th>
                <th>Status Project</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            @if (!empty($data['project']))
            @foreach ($data['project'] as $key => $value)
            <tr>
                <td>{{$key+1}}</td>
                <td>
                    <a href="{{url('/project-detail', $value->project_id)}}">{{$value->name}}</a>
                    @if (session('level')=='Admin')
                        <div><small>Supervisor : {{$value->supervisor_name}}</small></div>
                    @endif
                    <div>
                        Dibuat pada <span>{{$value->dibuat_pada}} WIB</span>
                    </div>
                </td>
                <td>
                    @if ($value->persentase < 100) <span class="badge badge-warning p-2">{{round($value->persentase)}}%</span>
                        @else
                        <span class="badge badge-success p-2">{{round($value->persentase)}}%</span>
                        @endif
                </td>
                <td class="d-flex justify-content-around">
                    <form action="{{url('/project/complete', $value->project_id)}}" method="post" class="{{$value->persentase==100 && $value->status=='complete'?'d-none':''}}">
                        @method('PUT')
                        @csrf
                        <input type="hidden" name="name" value="{{$value->name}}">
                        <input type="hidden" name="description" value="{{$value->description}}">
                        <input type="hidden" name="start_date" value="{{$value->start_date}}">
                        <input type="hidden" name="end_date" value="{{$value->end_date}}">
                        <input type="hidden" name="status" value="complete">
                        <input type="hidden" name="file" value="{{$value->file}}">
                        <input type="hidden" name="users_id" value="{{$value->users_id}}">
                        <button class="btn btn-success" type="submit" {{$value->persentase < 100 ? 'disabled':''}}>
                            <i class="icon-check pr-1"></i>Selesai
                        </button>
                    </form>
                    <button class="btn btn-primary" type="button" data-toggle="modal" data-target="#projectProgress{{$key+1}}">
                        <i class="icon-control-play pr-1"></i>Progress
                    </button>
                    <form action="{{url('/project', $value->project_id)}}" method="post">
                        @method('DELETE')
                        @csrf
                        @if (session('level')!='Supervisor')
                        <button class="btn btn-danger" type="submit">
                            <i class="icon-trash pr-1"></i>Hapus
                        </button>
                        @endif
                    </form>
                    <!-- Modal Project Progress -->
                    <div class="modal fade" id="projectProgress{{$key+1}}" tabindex="-1" role="dialog" aria-labelledby="projectProgress{{$key+1}}"
                        aria-hidden="true">
                        <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="ubahProjectTitle">Daftar Pekerjaan Project : <b>{{$value->name}}</b></h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <table class="table table-responsive-sm table-striped">
                                        <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>Username</th>
                                                <th>Tugas</th>
                                                <th>Version</th>
                                                <th>Description</th>
                                                <th>Status</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($value->pekerjaan as $key => $value)
                                            <tr>
                                                <td>
                                                    {{$key+1}}
                                                </td>
                                                <td>
                                                    {{$value->username}}
                                                </td>
                                                <td>
                                                    {{$value->name}}
                                                </td>
                                                <td>
                                                    {{$value->version}}
                                                </td>
                                                <td>
                                                    {{$value->description}}
                                                </td>
                                                <td>
                                                    {{$value->status}}
                                                </td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <th>No</th>
                                                <th>Username</th>
                                                <th>Tugas</th>
                                                <th>Version</th>
                                                <th>Description</th>
                                                <th>Status</th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </td>
            </tr>
            @endforeach
            @endif
            {{-- <tr>
                <td>
                    <a href="{{url('/detail-project')}}">ImageWall - Design your panel</a>
                    <div>
                        Dibuat pada <span id="01_waktu">Senin, 2 Juli 2018 | 20.35 WIB</span>
                    </div>
                </td>
                <td>
                    <span class="badge badge-primary p-2">Progress</span>
                    <span class="badge badge-danger p-2">Terlambat 5 hari</span>
                </td>
                <td>
                    <button class="btn btn-success" type="button">
                        <i class="icon-check pr-1"></i>Selesai
                    </button>
                    <button class="btn btn-primary" type="button" disabled>
                        <i class="icon-control-play pr-1"></i>Progress
                    </button>
                    <button class="btn btn-info" type="button">
                        <i class="icon-pencil pr-1"></i>Ubah
                    </button>
                    <button class="btn btn-danger" type="button">
                        <i class="icon-trash pr-1"></i>Hapus
                    </button>
                </td>
            </tr>
            <tr>
                <td>
                    <a href="{{url('/detail-project')}}">Netra Eye Clinic Website</a>
                    <div>
                        Dibuat pada <span id="01_waktu">Jumat, 11 Desember 2018 | 02.15 WIB</span>
                    </div>
                </td>
                <td>
                    <span class="badge badge-success p-2">Selesai</span>
                </td>
                <td>
                    <button class="btn btn-success" type="button" disabled>
                        <i class="icon-check pr-1"></i>Selesai
                    </button>
                    <button class="btn btn-primary" type="button">
                        <i class="icon-control-play pr-1"></i>Progress
                    </button>
                    <button class="btn btn-info" type="button">
                        <i class="icon-pencil pr-1"></i>Ubah
                    </button>
                    <button class="btn btn-danger" type="button">
                        <i class="icon-trash pr-1"></i>Hapus
                    </button>
                </td>
            </tr>
            <tr>
                <td>
                    <a href="{{url('/detail-project')}}">D'sign panel </a>
                    <div>
                        Dibuat pada <span id="01_waktu">Senin, 2 Juli 2018 | 02.15 WIB</span>
                    </div>
                </td>
                <td>
                    <span class="badge badge-success p-2">Selesai</span>
                </td>
                <td>
                    <button class="btn btn-success" type="button" disabled>
                        <i class="icon-check pr-1"></i>Selesai
                    </button>
                    <button class="btn btn-primary" type="button">
                        <i class="icon-control-play pr-1"></i>Progress
                    </button>
                    <button class="btn btn-info" type="button" data-toggle="modal" data-target="#ubahProject">
                        <i class="icon-pencil pr-1"></i>Ubah
                    </button>
                    <button class="btn btn-danger" type="button">
                        <i class="icon-trash pr-1"></i>Hapus
                    </button>
                </td>
            </tr> --}}

        </tbody>
        <tfoot>
            <td>No</td>
            <td>Nama Project</td>
            <td>Status Project</td>
            <td>Action</td>
        </tfoot>
    </table>
</div>
@endsection

@section('script')
<script>
    var date = $('[data-toggle="datepicker"]').datepicker({
        format: 'yyyy-mm-dd'
    });

</script>
@endsection
