@extends('layout.app')

@section('content')
<div class="jumbotron bg-white">
    <div class="d-flex justify-content-between">
        <h1>Daftar Level</h1>
        <div>
            <button class="btn btn-success" type="button" data-toggle="modal" data-target="#tambahLevel">
                <i class="icon-plus pr-1"></i>Tambah Level</button>
        </div>
    </div>
    <table id="daftar_proyek" class="table table-responsive-sm">
        <thead>
            <tr>
                <th>Nama Level</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            @if (!is_null($data))
            @php
            $x = 0
            @endphp
            @foreach ($data['level'] as $key => $value)
            @php
            $x++
            @endphp
            <tr>
                <td>
                    {{$value->nama_level}}
                </td>
                <td class="d-flex">
                    <button class="btn btn-info mr-1" type="button" data-toggle="modal" data-target="#ubahLevel{{$x}}">
                        <i class="icon-pencil pr-1"></i>Ubah
                    </button>
                    <form action="{{url('/level', $value->id_level)}}" method="POST">
                        @method('DELETE')
                        @csrf
                        <button class="btn btn-danger" type="submit">
                            <i class="icon-trash pr-1"></i>Hapus
                        </button>
                    </form>
                </td>
            </tr>
            <!-- Modal Ubah Tipe Pekerjaan-->
            <div class="modal fade" id="ubahLevel{{$x}}" tabindex="-1" role="dialog" aria-labelledby="ubahLevel{{$x}}"
                aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalCenterTitle">Ubah Level</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <form action="{{url('/level', $value->id_level)}}" method="POST">
                            @method('PUT')
                            @csrf
                            <div class="modal-body">
                                <div class="form-group">
                                    <label for="valnamalevel">Nama Level</label>
                                    <input class="form-control" id="valnamalevel" name="valnamalevel"
                                        type="text" placeholder="Nama Level" value="{{$value->nama_level}}">
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                <button type="submit" class="btn btn-success">Submit</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            @endforeach
            @endif
        </tbody>
        <tfoot>
            <td>Nama Tipe Pekerjaan</td>
            <td>Action</td>
        </tfoot>
    </table>
</div>
@endsection

@section('modal')
<!-- Modal Tambah Tipe Pekerjaan-->
<div class="modal fade" id="tambahLevel" tabindex="-1" role="dialog" aria-labelledby="tambahLevel"
    aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalCenterTitle">Tambah Level</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="{{url('/level')}}" method="post">
                @csrf
                <div class="modal-body">
                    <div class="form-group">
                        <label for="valnamalevel">Nama Level</label>
                        <input class="form-control" id="valnamalevel" name="valnamalevel" type="text"
                            placeholder="Nama Level">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-success">Submit</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
