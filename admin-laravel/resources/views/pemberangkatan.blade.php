@extends('layout.app')

@section('content')
<div class="jumbotron bg-white">
    <div class="d-flex justify-content-between">
        <h1>Daftar Pemberangkatan</h1>
        <div>
            @if (session('level')!='Petugas')
            <button class="btn btn-success" type="button" data-toggle="modal" data-target="#tambahPemberangkatan">
                <i class="icon-plus pr-1"></i>Tambah Pemberangkatan
            </button>
            @endif
        </div>
    </div>
    <div class="modal fade" id="tambahPemberangkatan" tabindex="-1" role="dialog" aria-labelledby="tambahPemberangkatan" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="tambahPemberangkatanTitle">Tambah Pemberangkatan</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="{{url('/pemberangkatan')}}" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="modal-body">
                        <div class="form-group">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">
                                        <i class="fa fa-pencil-square"></i>
                                    </span>
                                </div>
                                <input class="form-control" id="email" type="text" name="valjamberangkat" placeholder="Jam Berangkat"
                                    required>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">
                                        <i class="fa fa-pencil-square"></i>
                                    </span>
                                </div>
                                <input class="form-control" id="email" type="text" name="valnamapemberangkatan" placeholder="Nama Pemberangkatan"
                                    required>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">
                                        <i class="fa fa-pencil-square"></i>
                                    </span>
                                </div>
                                <input class="form-control" id="email" type="text" name="vallamapemberangkatan" placeholder="Lama Perjalanan"
                                    required>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">
                                        <i class="fa fa-pencil-square"></i>
                                    </span>
                                </div>
                                <input class="form-control" id="email" type="text" name="valjamsampai" placeholder="Jam Sampai"
                                    required>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">
                                        <i class="fa fa-pencil-square"></i>
                                    </span>
                                </div>
                                <input class="form-control" id="email" type="text" name="valjamcheckin" placeholder="Jam Checkin"
                                    required>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">
                                        <i class="fa fa-pencil-square"></i>
                                    </span>
                                </div>
                                <input class="form-control" id="email" type="text" name="valharga" placeholder="Harga"
                                    required>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group">
                                <div class="input-group-append">
                                    <span class="input-group-text">
                                        <i class="fa fa-calendar" aria-hidden="true"></i>
                                    </span>
                                </div>
                                <input name="valtanggalberangkat" data-toggle="datepicker" type="text" class="form-control docs-date"
                                    placeholder="Tanggal Berangkat" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">
                                        <i class="fa fa-level-up"></i>
                                    </span>
                                </div>
                                <select class="custom-select" name="validtransportasi">
                                    <option selected disabled>Pilih Transportasi</option>
                                    @foreach ($data['transportasi'] as $value)
                                    <option value="{{$value->id_transportasi}}">{{$value->nama_transportasi}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">
                                        <i class="fa fa-level-up"></i>
                                    </span>
                                </div>
                                <select class="custom-select" name="validrute">
                                    <option selected disabled>Pilih Rute</option>
                                    @foreach ($data['rute'] as $value)
                                    <option value="{{$value->id_rute}}">{{$value->tujuan}},{{$value->rute_awal}}-{{$value->rute_akhir}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-success">Submit</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <table id="daftar_proyek" class="table table-responsive-sm">
        <thead>
            <tr>
                <th>No</th>
                <th>Nama Pemberangkatan</th>
                <th>Lama Pemberangkatan</th>
                <th>Harga</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            @if (!empty($data['pemberangkatan']))
            @foreach ($data['pemberangkatan'] as $key => $value)
            <tr>
                <td>{{$key+1}}</td>
                <td>
                <a href="{{url('/project-detail', $value->id_pemberangkatan)}}">{{$value->nama}}</a>
                        <div><small>Tanggal Berangkat : {{$value->tanggal_berangkat}}</small></div>
                
                    <div>
                        Jam Berangkat <span>{{$value->jam_berangkat}} WIB</span>
                    </div>
                </td>
                <td>{{$value->lama_pemberangkatan}}</td>
                <td>{{$value->harga}}</td>
                <td class="d-flex justify-content-around">
                    <button class="btn btn-primary" type="button" data-toggle="modal" data-target="#pemberangkatanDetail{{$key+1}}">
                        <i class="icon-control-play pr-1"></i>Detail
                    </button>
                    <form action="{{url('/pemberangkatan', $value->id_pemberangkatan)}}" method="post">
                        @method('DELETE')
                        @csrf
                        @if (session('level')!='Petugas')
                        <button class="btn btn-danger" type="submit">
                            <i class="icon-trash pr-1"></i>Hapus
                        </button>
                        @endif
                    </form>
                    <!-- Modal Detail Pemberangkatan -->
                    <div class="modal fade" id="pemberangkatanDetail{{$key+1}}" tabindex="-1" role="dialog" aria-labelledby="pemberangkatanDetail{{$key+1}}"
                        aria-hidden="true">
                        <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="ubahPemberangkatanTitle">Pemberangkatan : <b>{{$value->tanggal_berangkat}}</b></h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <table class="table table-responsive-sm table-striped">
                                        <thead>
                                            <tr>
                                              
                                                <th>Kode Transportasi</th>
                                                <th>Keterangan Tranportasi</th>
                                                <th>Version</th>
                                                <th>Description</th>
                                                <th>Status</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($data['pemberangkatan'] as $key => $value)
                                            <tr>
                                                <td>
                                                    {{$key+1}}
                                                </td>
                                                <td>
                                                    {{$value->tanggal_berangkat}}
                                                </td>
                                                <td>
                                                    {{$value->tanggal_berangkat}}
                                                </td>
                                                <td>
                                                    {{$value->tanggal_berangkat}}
                                                </td>
                                                <td>
                                                    {{$value->tanggal_berangkat}}
                                                </td>
                                                <td>
                                                    {{$value->tanggal_berangkat}}
                                                </td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <th>No</th>
                                                <th>Username</th>
                                                <th>Tugas</th>
                                                <th>Version</th>
                                                <th>Description</th>
                                                <th>Status</th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </td>
            </tr>
            @endforeach
            @endif
           

        </tbody>
        <tfoot>
            <td>No</td>
            <td>Nama Pemberangkatan</td>
            <td>Lama Pemberangkatan</td>
            <td>Harga</td>
            <td>Action</td>
        </tfoot>
    </table>
</div>
@endsection

@section('script')
<script>
    var date = $('[data-toggle="datepicker"]').datepicker({
        format: 'yyyy-mm-dd'
    });

</script>
@endsection
