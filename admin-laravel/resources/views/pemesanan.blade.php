@extends('layout.app')

@section('content')
<div class="jumbotron bg-white">
    <div class="d-flex justify-content-between">
        <h1>Daftar Pemesanan</h1>
        
    </div>
  
    <table id="daftar_pemesanan" class="table table-responsive-sm">
        <thead>
            <tr>
                <th>No</th>
                <th>Kode Pemesanan</th>
                <th>Nama Pemesan</th>
                <th>Kode Kursi</th>
                <th>Total Bayar</th>
                <th>Status</th>
                <th>Tujuan</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            @if (!empty($data['pemesanan']))
            @foreach ($data['pemesanan'] as $key => $value)
            <tr>
                <td>{{$key+1}}</td>
                <td>
                    {{$value->kode_pemesanan}}
                        <div><small>Tanggal Pemesanan : {{$value->tanggal_pemesanan}}</small></div>
                
                    <div>
                        Tempat Pemesanan <span>{{$value->tempat_pemesanan}}</span>
                    </div>
                </td>
                <td>{{$value->nama_penumpang}}</td>
                <td>{{$value->kode_kursi}}</td>
                <td>{{$value->total_bayar}}</td>
                <td>{{$value->status}}</td>
                <td>{{$value->tujuan}}</td>
                <td class="d-flex justify-content-around">
            <div>
                <form action="{{url('/cetak')}}" method="post">
                    @csrf
                    <input type="hidden" name="id" value="{{$value->id_pemesanan}}">
                    <button class="btn btn-default btn-sm" type="submit"><i class="fa fa-print"></i> Cetak</button>
                </form>
            </div>
                <form action="{{url('/pemesanan', $value->id_pemesanan)}}" method="post">
                        @method('PUT')
                        @csrf
                        <button class="btn btn-success" type="submit" {{$value->status=='Valid' ? 'disabled':''}}>
                            <i class="icon-check pr-1"></i>Valid
                        </button>
                    </form>
                    <form action="{{url('/pemesanan', $value->id_pemesanan)}}" method="post">
                        @method('DELETE')
                        @csrf
                        @if (session('level')!='Petugas')
                        <button class="btn btn-danger" type="submit">
                            <i class="icon-trash pr-1"></i>Hapus
                        </button>
                        @endif
                    </form>
                    
                </td>
            </tr>
            @endforeach
            @endif
           

        </tbody>
        <tfoot>
                <td>No</td>
                <td>Kode Pemesanan</td>
                <td>Nama Pemesan</td>
                <td>Kode Kursi</td>
                <td>Total Bayar</td>
                <td>Status</td>
                <td>Tujuan</td>
                <td>Action</td>
        </tfoot>
    </table>
</div>
@endsection

@section('script')
<script>
    var date = $('[data-toggle="datepicker"]').datepicker({
        format: 'yyyy-mm-dd'
    });

</script>
@endsection
