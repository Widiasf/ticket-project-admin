@extends('layout.app')

@section('content')
<div class="jumbotron bg-white pt-4">
    <h1>Edit Profile</h1>
    <form action="{{url('/edit-profile', session('user_id'))}}" method="post" enctype="multipart/form-data">
        @method('PUT')
        @csrf
        <div class="modal-body">
            <div class="form-group">
                <div class="input-group">
                    <div class="input-group-prepend">
                        <span class="input-group-text">
                            <i class="fa fa-asterisk"></i>
                        </span>
                    </div>
                    <input class="form-control" id="current_password" type="password" name="current_password"
                        placeholder="Current Password" autocomplete="current_password" required>
                </div>
            </div>
            <div class="form-group">
                <div class="input-group">
                    <div class="input-group-prepend">
                        <span class="input-group-text">
                            <i class="fa fa-asterisk"></i>
                        </span>
                    </div>
                    <input class="form-control" id="new_password" type="password" name="new_password" placeholder="New Password"
                        autocomplete="new_password" required>
                </div>
            </div>
            <div class="form-group">
                <div class="input-group">
                    <div class="input-group-prepend">
                        <span class="input-group-text">
                            <i class="fa fa-asterisk"></i>
                        </span>
                    </div>
                    <input class="form-control" id="retype_new_password" type="password" name="retype_new_password"
                        placeholder="Retype New Password" autocomplete="retype_new_password" required>
                </div>
            </div>
            <div class="form-group">
                <div class="input-group">
                    <div class="input-group-prepend">
                        <span class="input-group-text">
                            <i class="fa fa-envelope"></i>
                        </span>
                    </div>
                    <input class="form-control" id="email" type="email" name="email" placeholder="Email" autocomplete="email"
                        value="{{$data['user_info']['email']}}" required>
                </div>
            </div>
            <div class="form-group">
                <div class="input-group">
                    <div class="input-group-prepend">
                        <span class="input-group-text">
                            <i class="fa fa-user"></i>
                        </span>
                    </div>
                    <input class="form-control" id="nama" type="text" name="nama" placeholder="Nama" autocomplete="nama"
                        value="{{$data['user_info']['name']}}" required>
                </div>
            </div>
            <div class="form-group">
                <div class="input-group">
                    <div class="input-group-prepend">
                        <span class="input-group-text">
                            <i class="fa fa-user"></i>
                        </span>
                    </div>
                    <input class="form-control" id="username" type="text" name="username" placeholder="Username" autocomplete="username"
                    value="{{$data['user_info']['username']}}" required>
                </div>
            </div>
            <div class="form-group">
                <div class="input-group">
                    <div class="input-group-prepend">
                        <span class="input-group-text">
                            <i class="fa fa-phone"></i>
                        </span>
                    </div>
                    <input class="form-control" id="nomor_telepon" type="number" name="nomor_telepon" placeholder="Nomor Telepon"
                        autocomplete="nomor_telepon" value="{{$data['user_info']['no_telp']}}" required>
                </div>
            </div>
            <div class="input-group mb-3">
                <div class="input-group-prepend">
                    <span class="input-group-text" id="foto_profil_addon"><i class="icon-camera"></i></span>
                </div>
                <div class="custom-file">
                    <input type="file" class="custom-file-input" id="foto_profil" name="foto_profil" accept="image/*"
                        aria-describedby="foto_profil_addon">
                        {{-- explode filepath for display only --}}
                    @php
                        $filename = explode("/", $data['user_info']['photo_profile']);
                    @endphp
                    <label class="custom-file-label" for="foto_profil">{{!empty($filename[3]) ? $filename[3]:''}}</label>
                    <input type="hidden" name="foto_profil_filename" value="{{$data['user_info']['photo_profile']}}">
                </div>
            </div>
            <textarea class="form-control" id="alamat" name="alamat" rows="5" placeholder="Alamat" required>{{$data['user_info']['address']}}</textarea>
        </div>
        <div class="modal-footer">
            <button id="submit" type="submit" class="btn btn-success">Ubah</button>
        </div>
    </form>
</div>
@endsection

@section('script')
<script>
    function checkPasswordMatch() {
        var password = $("#new_password").val();
        var confirmPassword = $("#retype_new_password").val();

        if (password != confirmPassword)
            $("#submit").attr('disabled', true);
        else
            $("#submit").attr('disabled', false);
    }

    $(function () {
        $("#submit").attr('disabled', true);
        $("#new_password, #retype_new_password").keyup(checkPasswordMatch);
    });
</script>
@endsection
