@extends('layout.app')

@section('content')
<div class="row pb-3">
    <div class="col-12">
        <button class="btn btn-success float-right" type="button" data-toggle="modal" data-target="#tambahPengguna">
            <i class="icon-plus pr-1"></i>Tambah Pengguna</button>
    </div>
</div>
<div class="row">
    @foreach ($data['users'] as $value)
    @if ($value->users_id != session('user_id'))
    <div class="col-4 d-flex">
            <div class="card flex-fill">
                <div class="card-body">
                    <div class="d-flex">
                        <div style="width: 100px;height: 100px;background-position: center;background-image: url({{url($value->photo_profile)}})" alt="{{$value->name}}"
                            class="mr-2 rounded-circle"></div>
                        <div class="d-flex flex-column justify-content-center align-items-start flex-fill">
                            <h5>{{$value->name}}</h5>
                            {{$value->username}}
                        </div>
                        <div class="d-flex justify-content-end flex-fill">
                            <div>
                                <form action="{{url('/pengguna', $value->users_id)}}" method="post">
                                    @method('DELETE')
                                    @csrf
                                    <button type="submit" class="btn btn-danger py-0 px-1"><i class="icon-close"></i></button>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="d-flex flex-column justify-content-around py-3">
                        <span class="d-flex align-items-center pb-1">
                            <i class="icon-location-pin dashboard-profile-icons pr-1"></i>
                            <span id="dashboard-profile-alamat">
                                {{$value->address}}
                            </span>
                        </span>
                        <span class="d-flex align-items-center pb-1">
                            <i class="icon-envelope dashboard-profile-icons pr-1"></i>
                            <span id="dashboard-profile-email">{{$value->email}}</span>
                        </span>
                        <span class="d-flex align-items-center">
                            <i class="icon-phone dashboard-profile-icons pr-1"></i>
                            <span id="dashboard-profile-nohp">{{$value->no_telp}}</span>
                        </span>
                    </div>
                </div>
            </div>
        </div>
    @endif
    @endforeach
</div>
{{-- <div class="card-deck pb-4">
    <div class="card">
        <div class="card-body">
            <div class="d-flex">
                <img src="https://via.placeholder.com/100" alt="" class="pr-2">
                <div class="d-flex flex-column justify-content-center align-items-start flex-fill">
                    <h5>Steven Rogers</h5>
                    steve
                </div>
                <div class="d-flex justify-content-end flex-fill">
                    <div><button class="btn btn-danger py-0 px-1"><i class="icon-close"></i></button></div>
                </div>
            </div>
            <div class="d-flex flex-column justify-content-around py-3">
                <span class="d-flex align-items-center pb-1">
                    <i class="icon-location-pin dashboard-profile-icons pr-1"></i>
                    <span id="dashboard-profile-alamat">
                        Jln. Nanjung No. 80 RT 03/02 Kel. Utama Cimahi Selatan
                    </span>
                </span>
                <span class="d-flex align-items-center pb-1">
                    <i class="icon-envelope dashboard-profile-icons pr-1"></i>
                    <span id="dashboard-profile-email">captamerica@avengers.com</span>
                </span>
                <span class="d-flex align-items-center">
                    <i class="icon-phone dashboard-profile-icons pr-1"></i>
                    <span id="dashboard-profile-nohp">0854647555</span>
                </span>
            </div>
        </div>
    </div>
    <div class="card">
        <div class="card-body">
            <div class="d-flex">
                <img src="https://via.placeholder.com/100" alt="" class="pr-2">
                <div class="d-flex flex-column justify-content-center align-items-start flex-fill">
                    <h5>Steven Rogers</h5>
                    steve
                </div>
                <div class="d-flex justify-content-end flex-fill">
                    <div><button class="btn btn-danger py-0 px-1"><i class="icon-close"></i></button></div>
                </div>
            </div>
            <div class="d-flex flex-column justify-content-around py-3">
                <span class="d-flex align-items-center pb-1">
                    <i class="icon-location-pin dashboard-profile-icons pr-1"></i>
                    <span id="dashboard-profile-alamat">
                        Jln. Nanjung No. 80 RT 03/02 Kel. Utama Cimahi Selatan
                    </span>
                </span>
                <span class="d-flex align-items-center pb-1">
                    <i class="icon-envelope dashboard-profile-icons pr-1"></i>
                    <span id="dashboard-profile-email">captamerica@avengers.com</span>
                </span>
                <span class="d-flex align-items-center">
                    <i class="icon-phone dashboard-profile-icons pr-1"></i>
                    <span id="dashboard-profile-nohp">0854647555</span>
                </span>
            </div>
        </div>
    </div>
    <div class="card">
        <div class="card-body">
            <div class="d-flex">
                <img src="https://via.placeholder.com/100" alt="" class="pr-2">
                <div class="d-flex flex-column justify-content-center align-items-start flex-fill">
                    <h5>Steven Rogers</h5>
                    steve
                </div>
                <div class="d-flex justify-content-end flex-fill">
                    <div><button class="btn btn-danger py-0 px-1"><i class="icon-close"></i></button></div>
                </div>
            </div>
            <div class="d-flex flex-column justify-content-around py-3">
                <span class="d-flex align-items-center pb-1">
                    <i class="icon-location-pin dashboard-profile-icons pr-1"></i>
                    <span id="dashboard-profile-alamat">
                        Jln. Nanjung No. 80 RT 03/02 Kel. Utama Cimahi Selatan
                    </span>
                </span>
                <span class="d-flex align-items-center pb-1">
                    <i class="icon-envelope dashboard-profile-icons pr-1"></i>
                    <span id="dashboard-profile-email">captamerica@avengers.com</span>
                </span>
                <span class="d-flex align-items-center">
                    <i class="icon-phone dashboard-profile-icons pr-1"></i>
                    <span id="dashboard-profile-nohp">0854647555</span>
                </span>
            </div>
        </div>
    </div>
</div> --}}
@endsection

@section('modal')
<!-- Modal Tambah Pengguna -->
<div class="modal fade" id="tambahPengguna" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
    aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalCenterTitle">Tambah Pengguna</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="{{url('/pengguna')}}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="modal-body">
                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text">
                                    <i class="fa fa-user"></i>
                                </span>
                            </div>
                            <input class="form-control" id="username" type="text" name="username" placeholder="Username"
                                autocomplete="name">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text">
                                    <i class="fa fa-asterisk"></i>
                                </span>
                            </div>
                            <input class="form-control" id="password" type="password" name="password" placeholder="Password"
                                autocomplete="new-password">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text">
                                    <i class="fa fa-envelope"></i>
                                </span>
                            </div>
                            <input class="form-control" id="email" type="email" name="email" placeholder="Email"
                                autocomplete="email">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text">
                                    <i class="fa fa-user"></i>
                                </span>
                            </div>
                            <input class="form-control" id="nama" type="text" name="nama" placeholder="Nama"
                                autocomplete="nama">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text">
                                    <i class="fa fa-phone"></i>
                                </span>
                            </div>
                            <input class="form-control" id="nomor_telepon" type="number" name="nomor_telepon"
                                placeholder="Nomor Telepon" autocomplete="nomor_telepon">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text">
                                    <i class="fa fa-level-up"></i>
                                </span>
                            </div>
                            <select class="custom-select" id="user_level" name="user_level">
                                <option selected disabled>Pilih Level</option>
                                @foreach ($data['user_level'] as $value)
                                <option value="{{$value->users_level_id}}">{{$value->name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="foto_profil_addon"><i class="icon-camera"></i></span>
                        </div>
                        <div class="custom-file">
                            <input type="file" class="custom-file-input" id="foto_profil" name="foto_profil" accept="image/*"
                                aria-describedby="foto_profil_addon">
                            <label class="custom-file-label" for="foto_profil">Foto Profil</label>
                        </div>
                    </div>
                    <textarea class="form-control" id="alamat" name="alamat" rows="5" placeholder="Alamat"></textarea>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-success">Submit</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
