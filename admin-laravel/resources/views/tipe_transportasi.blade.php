@extends('layout.app')

@section('content')
<div class="jumbotron bg-white">
    <div class="d-flex justify-content-between">
        <h1>Daftar Tipe Transportasi</h1>
        <div>
            <button class="btn btn-success" type="button" data-toggle="modal" data-target="#tambahTipeTransportasi">
                <i class="icon-plus pr-1"></i>Tambah Tipe Transportasi</button>
        </div>
    </div>
    <table id="daftar_proyek" class="table table-responsive-sm">
        <thead>
            <tr>
                <th>Nama Tipe</th>
                <th>Keterangan</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            @if (!is_null($data))
            @php
            $x = 0
            @endphp
            @foreach ($data['tipe_transportasi'] as $key => $value)
            @php
            $x++
            @endphp
            <tr>
                <td>
                    {{$value->nama_tipe}}
                </td>
                <td>
                    {{$value->keterangan}}
                </td>
                
                
                <td class="d-flex">
                    <button class="btn btn-info mr-1" type="button" data-toggle="modal" data-target="#ubahTipeTransportasi{{$x}}">
                        <i class="icon-pencil pr-1"></i>Ubah
                    </button>
                    <form action="{{url('/tipe-transportasi', $value->id_tipe_transportasi)}}" method="POST">
                        @method('DELETE')
                        @csrf
                        <button class="btn btn-danger" type="submit">
                            <i class="icon-trash pr-1"></i>Hapus
                        </button>
                    </form>
                </td>
            </tr>
            <!-- Modal Ubah Tipe Pekerjaan-->
            <div class="modal fade" id="ubahTipeTransportasi{{$x}}" tabindex="-1" role="dialog" aria-labelledby="ubahTipeTransportasi{{$x}}"
                aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalCenterTitle">Ubah Tipe Transportasi</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <form action="{{url('/tipe-transportasi', $value->id_tipe_transportasi)}}" method="POST">
                            @method('PUT')
                            @csrf
                            <div class="modal-body">
                                <div class="form-group">
                                    <label for="valnamatipe">Nama Tipe</label>
                                    <input class="form-control" id="valnamatipe" name="valnamatipe"
                                        type="text" placeholder="Nama Tipe" value="{{$value->nama_tipe}}">
                                </div>
                                <div class="form-group">
                                    <label for="valketerangan">Keterangan</label>
                                    <input class="form-control" id="valjumlahkursi" name="valketerangan"
                                        type="text" placeholder="Keterangan" value="{{$value->keterangan}}">
                                </div>
                                
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                <button type="submit" class="btn btn-success">Submit</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            @endforeach
            @endif
        </tbody>
        <tfoot>
            <td>Nama Tipe</td>
            <td>Keterangan</td>
            <td>Action</td>
        </tfoot>
    </table>
</div>
@endsection

@section('modal')
<!-- Modal Tambah Transportasi-->
<div class="modal fade" id="tambahTipeTransportasi" tabindex="-1" role="dialog" aria-labelledby="tambahTipeTransportasi"
    aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalCenterTitle">Tambah Tipe Transportasi</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="{{url('/tipe-transportasi')}}" method="post">
                @csrf
                <div class="modal-body">
                    <div class="form-group">
                        <label for="valnamatipe">Nama Tipe</label>
                        <input class="form-control" id="valnamatipe" name="valnamatipe" type="text"
                            placeholder="Nama Tipe">
                    </div>
                    <div class="form-group">
                        <label for="valketerangan">Keterangan</label>
                        <input class="form-control" id="valketerangan" name="valketerangan" type="text"
                            placeholder="Keterangan">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-success">Submit</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
