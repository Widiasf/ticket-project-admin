<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/login', 'DataController@login');
Route::post('/login', 'DataController@login_verification');
Route::post('/logout', 'DataController@logout');

// Group Middleware CheckLogin
Route::group(['middleware' => ['web']], function () {
    // Dashboard
    Route::get('/', 'DataController@dashboard')->middleware('check.login');
    Route::put('/review/{id}', 'DataController@update_review')->middleware('check.login');
    
    // Halaman Project
    Route::get('/project/{id}', 'DataController@project')->middleware('check.login');
    Route::post('/project', 'DataController@create_project')->middleware('check.login');
    Route::put('/project/{id}', 'DataController@update_project')->middleware('check.login');
    Route::put('/project/complete/{id}', 'DataController@update_project_complete')->middleware('check.login');
    Route::delete('/project/{id}', 'DataController@delete_project')->middleware('check.login');

    // Project Detail
    Route::get('/project-detail/{id}', 'DataController@project_detail')->middleware('check.login');
    Route::post('/tambah-pekerjaan', 'DataController@tambah_pekerjaan')->middleware('check.login');
    Route::post('/tambah-file', 'DataController@tambah_file')->middleware('check.login');
    Route::put('/delete-file/{id}', 'DataController@delete_file')->middleware('check.login');

    // Profile
    Route::get('/edit-profile', 'DataController@edit_profile')->middleware('check.login');
    Route::put('/edit-profile/{id}', 'DataController@update_profile')->middleware('check.login');

    // Semua Pekerjaan
    Route::get('/semua-pekerjaan', 'DataController@semua_pekerjaan')->middleware('check.login');
    Route::put('/semua-pekerjaan/{id}', 'DataController@update_pekerjaan')->middleware('check.login');

    // Pengguna
    Route::get('/pengguna', 'DataController@pengguna')->middleware('check.login');
    Route::post('/pengguna', 'DataController@create_pengguna')->middleware('check.login');
    Route::delete('/pengguna/{id}', 'DataController@delete_pengguna')->middleware('check.login');
    Route::put('/pengguna/{id}', 'DataController@update_pengguna')->middleware('check.login');

    // Tipe Pekerjaan
    Route::get('/tipe-pekerjaan', 'DataController@tipe_pekerjaan')->middleware('check.login');
    Route::post('/tipe-pekerjaan', 'DataController@create_tipe_pekerjaan')->middleware('check.login');
    Route::delete('/tipe-pekerjaan/{id}', 'DataController@delete_tipe_pekerjaan')->middleware('check.login');
    Route::put('/tipe-pekerjaan/{id}', 'DataController@update_tipe_pekerjaan')->middleware('check.login');

    // Petugas
    Route::get('/petugas', 'DataController@petugas')->middleware('check.login');
    Route::post('/petugas', 'DataController@create_petugas')->middleware('check.login');
    Route::delete('/petugas/{id}', 'DataController@delete_petugas')->middleware('check.login');
    Route::put('/petugas/{id}', 'DataController@update_petugas')->middleware('check.login');

    // Level Petugas
    Route::get('/level', 'DataController@level_petugas')->middleware('check.login');
    Route::post('/level', 'DataController@create_level_petugas')->middleware('check.login');
    Route::delete('/level/{id}', 'DataController@delete_level_petugas')->middleware('check.login');
    Route::put('/level/{id}', 'DataController@update_level_petugas')->middleware('check.login');

    // Transportasi 
    Route::get('/transportasi', 'DataController@transportasi')->middleware('check.login');
    Route::post('/transportasi', 'DataController@create_transportasi')->middleware('check.login');
    Route::delete('/transportasi/{id}', 'DataController@delete_transportasi')->middleware('check.login');
    Route::put('/transportasi/{id}', 'DataController@update_transportasi')->middleware('check.login');

    //Tipe Transportasi 
    Route::get('/tipe-transportasi', 'DataController@tipe_transportasi')->middleware('check.login');
    Route::post('/tipe-transportasi', 'DataController@create_tipe_transportasi')->middleware('check.login');
    Route::delete('/tipe-transportasi/{id}', 'DataController@delete_tipe_transportasi')->middleware('check.login');
    Route::put('/tipe-transportasi/{id}', 'DataController@update_tipe_transportasi')->middleware('check.login');

    //Rute 
    Route::get('/rute', 'DataController@rute')->middleware('check.login');
    Route::post('/rute', 'DataController@create_rute')->middleware('check.login');
    Route::delete('/rute/{id}', 'DataController@delete_rute')->middleware('check.login');
    Route::put('/rute/{id}', 'DataController@update_rute')->middleware('check.login');

    //Penumpang 
    Route::get('/penumpang', 'DataController@penumpang')->middleware('check.login');
    Route::delete('/penumpang/{id}', 'DataController@delete_penumpang')->middleware('check.login');

    //Pemesanan 
    Route::get('/pemesanan', 'DataController@pemesanan')->middleware('check.login');
    Route::post('/pemesanan', 'DataController@create_pemesanan')->middleware('check.login');
    Route::delete('/pemesanan/{id}', 'DataController@delete_pemesanan')->middleware('check.login');
    Route::put('/pemesanan/{id}', 'DataController@update_pemesanan')->middleware('check.login');

    //Pemberangkatan 
    Route::get('/pemberangkatan', 'DataController@pemberangkatan')->middleware('check.login');
    Route::post('/pemberangkatan', 'DataController@create_pemberangkatan')->middleware('check.login');
    Route::delete('/pemberangkatan/{id}', 'DataController@delete_pemberangkatan')->middleware('check.login');
    Route::put('/pemberangkatan/{id}', 'DataController@update_pemberangkatan')->middleware('check.login');
    // Misc
    Route::get('/detail-project', 'DataController@detail_project')->middleware('check.login');
    Route::post('/cetak', 'DataController@cetak')->middleware('check.login');
});
