<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use PDF;

class DataController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    // ----------------------------------------------------------------

    // Login / Logout
    
    // Login
    public function login(){
        if(session()->has('authenticated'))
            return redirect('/');
        else
            return view('login');
    }
    //Login Tiket project
     // Login Verification
    //  public function login_verification(Request $request){
    //     $client = new \GuzzleHttp\Client();

    //     try {
    //         $response = $client->request('POST', 'http://localhost:2121/LoginPetugas', [
    //             'form_params' => [
    //                 'username' => $request->username,
    //                 'password' => $request->password
    //             ]
    //         ]);
    //     } catch (\GuzzleHttp\Exception\ConnectException $e) {
    //         // This is will catch all connection timeouts
    //         // Handle accordinly
    //     } catch (\GuzzleHttp\Exception\ClientException $e) {
    //         // This will catch all 400 level errors.
    //         // echo $e->getResponse()->getStatusCode();
    //         $x =  json_decode($e->getResponse()->getBody());
    //         // $response =  $x->success ? 'true':'false';
    //         if(!$x->success){
    //             return redirect('/login')->with('error', 'User not found!');
    //         }
    //     }
        
    //     $response = json_decode($response->getBody()->getContents());
    //     session(['authenticated' => true]);
    //     session(['id_petugas' => $response->data->id_petugas]);
    //     session(['username' => $response->data->username]);
    //     return redirect('/');
    // }

    
    // Login Verification
    public function login_verification(Request $request){
        $client = new \GuzzleHttp\Client();

        try {
            $response = $client->request('POST', 'http://localhost:2121/login', [
                'form_params' => [
                    'username' => $request->username,
                    'password' => $request->password
                ]
            ]);
        } catch (\GuzzleHttp\Exception\ConnectException $e) {
            // This is will catch all connection timeouts
            // Handle accordinly
        } catch (\GuzzleHttp\Exception\ClientException $e) {
            // This will catch all 400 level errors.
            // echo $e->getResponse()->getStatusCode();
            $x =  json_decode($e->getResponse()->getBody());
            // $response =  $x->success ? 'true':'false';
            if(!$x->success){
                return redirect('/login')->with('error', 'User not found!');
            }
        }
        
        $response = json_decode($response->getBody()->getContents());
        session(['authenticated' => true]);
        session(['user_id' => $response->data->users_id]);
        session(['level' => $response->data->name]);
        session(['username' => $response->data->username]);
        return redirect('/');
    }

    // Logout
    public function logout(){
        session()->flush();
        
        return redirect('/login');
    }

  // Dashboard
    public function dashboard(){
        $client = new \GuzzleHttp\Client();

        // User Info
        try {
            $request = $client->get('http://localhost:2121/Users/'.session('user_id'));
            $response = json_decode($request->getBody()->getContents());

            $data['user_info'] = $response->data;
        } catch (\GuzzleHttp\Exception\ClientException $e) {
            return $e->getResponse()->getBody();
        }
        
        // Jumlah Karyawan
        try {
            $request = $client->get('http://localhost:2121/readUserByLevel/4f445001-e1eb-4710-9d10-8385adab3770');
            $response = json_decode($request->getBody()->getContents());
            
            $data['jumlah_karyawan'] = $response->num_of_data;
        } catch (\GuzzleHttp\Exception\ClientException $e) {
            return $e->getResponse()->getBody();
        }
        
        // Projek Anda
        try {
            $request = $client->get('http://localhost:2121/UsersProject/'.session('user_id'));
            $response = json_decode($request->getBody()->getContents());
            
            $data['projek_anda'] = $response->num_of_data;
        } catch (\GuzzleHttp\Exception\ClientException $e) {
            return $e->getResponse()->getBody();
        }

        // Pekerjaan Anda
        try {
            $request = $client->get('http://localhost:2121/readMyJob/'.session('user_id'));
            $response = json_decode($request->getBody()->getContents());
            
            $data['pekerjaan_anda'] = sizeof($response->data);
        } catch (\GuzzleHttp\Exception\ClientException $e) {
            return $e->getResponse()->getBody();
        }

        // File
        try {
            $request = $client->get('http://localhost:2121/readFileByUser/'.session('user_id'));
            $response = json_decode($request->getBody()->getContents());
            
            $data['file'] = sizeof($response->data);
        } catch (\GuzzleHttp\Exception\ClientException $e) {
            return $e->getResponse()->getBody();
        }

        // Statistik Pengguna
        try {
            $client = new \GuzzleHttp\Client();
            
            $request = $client->get('http://localhost:2121/Users?sort_by=name&sort_order=desc&page_size=10&page_offset=0');
            $response = json_decode($request->getBody()->getContents());

            $data['users'] = $response->data;
        } catch (\GuzzleHttp\Exception\ClientException $e) {
            echo $e->getResponse()->getBody();
        }
        
        // Pekerjaan Butuh Review
        try {
            $client = new \GuzzleHttp\Client();
            
            $request = $client->get('http://localhost:2121/ReadPekerjaanReview');
            $response = json_decode($request->getBody()->getContents());

            $data['pekerjaan_review'] = $response->data;
        } catch (\GuzzleHttp\Exception\ClientException $e) {
            echo $e->getResponse()->getBody();
        }

        return view('dashboard')->with('data', $data);
    }

    // Ubah Review (PUT)
    public function update_review(Request $request, $id){
        try {
            $client = new \GuzzleHttp\Client();
            $response = $client->request('PUT', 'http://localhost:2121/UsersProject/'.$id, [
                'form_params' => [
                    'name' => $request->name,
                    "project_id" => $request->project_id,
                    "users_id" => $request->users_id,
                    "name" => $request->name,
                    "version" => $request->version,
                    "description" => $request->description,
                    "status" => $request->status,
                    "parent" => null
                ]
            ]);
        } catch (\GuzzleHttp\Exception\ClientException $e) {
            return $e->getResponse()->getBody();
        }

        return back();
    }
    
    // Semua Pekerjaan
    public function semua_pekerjaan(){
        $data = null;
        try {
            $client = new \GuzzleHttp\Client();
            
            $request = $client->get('http://localhost:2121/readMyJob/'.session('user_id'));
            $response = json_decode($request->getBody()->getContents());
            foreach ($response->data as $key => $value) {
                $data['semua_pekerjaan'][$key] = $value;
            }
        } catch (\GuzzleHttp\Exception\ClientException $e) {
            $data['semua_pekerjaan'] = null;
        }

        return view('semua_pekerjaan')->with('data', $data);
    }

    // Ubah Pengguna (PUT)
    public function update_pekerjaan(Request $request, $id){
        try {
            $client = new \GuzzleHttp\Client();
            $response = $client->request('PUT', 'http://localhost:2121/UsersProject/'.$id, [
                'form_params' => [
                    'name' => $request->name,
                    "project_id" => $request->project_id,
                    "users_id" => $request->users_id,
                    "name" => $request->name,
                    "version" => $request->version,
                    "description" => $request->description,
                    "status" => $request->status,
                    "parent" => null
                ]
            ]);
        } catch (\GuzzleHttp\Exception\ClientException $e) {
            return $e->getResponse()->getBody();
        }

        return back();
    }


    // Project
    public function project($id){
        $month = array(
            1 =>'Januari',
            2 =>'Februari',
            3 =>'Maret',
            4 =>'April',
            5 =>'Mei',
            6 =>'Juni',
            7 =>'Juli',
            8 =>'Agustus',
            9 =>'September',
            10 =>'Oktober',
            11 =>'November',
            12 =>'Desember'
        );

        $day = array(
            1 =>'Senin',
            2 =>'Selasa',
            3 =>'Rabu',
            4 =>'Kamis',
            5 =>'Jumat',
            6 =>'Sabtu',
            7 =>'Minggu'
        );
        
        $data = null;
        if(session('level')=='Admin'){
            try {
                $client = new \GuzzleHttp\Client();
                
                $request = $client->get('http://localhost:2121/projectAdmin');
                $response = json_decode($request->getBody()->getContents());

                $data['project'] = $response->data;
            } catch (\GuzzleHttp\Exception\ClientException $e) {
                echo $e->getResponse()->getBody();
            }
        }
        else if(session('level')=='Karyawan'){
            try {
                $client = new \GuzzleHttp\Client();
                
                $request = $client->get('http://localhost:2121/readProjectKaryawan/'.$id);
                $response = json_decode($request->getBody()->getContents());

                $data['project'] = $response->data;
            } catch (\GuzzleHttp\Exception\ClientException $e) {
                echo $e->getResponse()->getBody();
            }
        }
        else{
            try {
                $client = new \GuzzleHttp\Client();
                
                $request = $client->get('http://localhost:2121/Project/'.$id);
                $response = json_decode($request->getBody()->getContents());

                $data['project'] = $response->data;
            } catch (\GuzzleHttp\Exception\ClientException $e) {
                echo $e->getResponse()->getBody();
            }
        }
        foreach ($data['project'] as $key => $value) {
            $time = strtotime($value->created_at);
            $hari = $day[date('N',$time)];
            $tanggal = date('d',$time);
            $bulan = $month[(int)date('m',$time)];
            $tahun = date('Y',$time);
            $waktu = date('H:i',$time);
            
            $data['project'][$key]->dibuat_pada = $hari.', '.$tanggal.' '.$bulan.' '.$tahun;

            // Pekerjaan sesuai project
            try {
                $client = new \GuzzleHttp\Client();
                
                $request = $client->get('http://localhost:2121/UsersProject/'.$data['project'][$key]->project_id);
                $response = json_decode($request->getBody()->getContents());
                $data['project'][$key]->pekerjaan = $response->data;
            } catch (\GuzzleHttp\Exception\ClientException $e) {
                echo $e->getResponse()->getBody();
            }
        }

        // Daftar supervisor
        try {
            $client = new \GuzzleHttp\Client();
            
            $request = $client->get('http://localhost:2121/readUserByLevel/0d0414c4-5686-4034-bd83-7b9f664148d9');
            $response = json_decode($request->getBody()->getContents());

            $data['supervisor'] = $response->data;
        } catch (\GuzzleHttp\Exception\ClientException $e) {
            echo $e->getResponse()->getBody();
        }

        // Supervisor name
        foreach ($data['project'] as $key => $value) {
            try {
                $client = new \GuzzleHttp\Client();
                
                $request = $client->get('http://localhost:2121/Users/'.$value->users_id);
                $response = json_decode($request->getBody()->getContents());

                $data['project'][$key]->supervisor_name = $response->data->name;
            } catch (\GuzzleHttp\Exception\ClientException $e) {
                echo $e->getResponse()->getBody();
            }
        }
        // return $data;
        return view('project')->with('data', $data);
    }

    // Tambah Project (POST)
    public function create_project(Request $request){
        // $request->validate([
        //     'nama_project' => 'alpha_num'
        // ]);    
        
        try {
            // $uploadedFile = $request->file('file_project');
            // $filename = preg_replace('/\s+/', '_', $uploadedFile->getClientOriginalName());
            // $filepath = preg_replace('/\s+/', '_', '/project-data/'.$request->nama_project.'/file/');
            // $path = public_path($filepath);
            // $uploadedFile->move($path, $filename);
            // $result = $filepath.$filename;
            
            $client = new \GuzzleHttp\Client();
            $response = $client->request('POST', 'http://localhost:2121/Project', [
                'form_params' => [
                    "name" => $request->nama_project,
                    "description" => $request->deskripsi,
                    "start_date" => $request->tanggal_mulai,
                    "end_date" => $request->tanggal_berakhir,
                    "status" => 'on progress',
                    "file" => 'null',
                    "users_id" => $request->supervisor
                ]
            ]);
        } catch (\GuzzleHttp\Exception\ClientException $e) {
            return $e->getResponse()->getBody();
        }

        return back()->with('success', 'Project created');
    }

    // Tambah File (POST)
    public function tambah_file(Request $request){
        try {
            $uploadedFile = $request->file('file_project');
            $filename = preg_replace('/\s+/', '_', $uploadedFile->getClientOriginalName());
            $filepath = preg_replace('/\s+/', '_', '/project-data/'.$request->nama_project.'/file/');
            $path = public_path($filepath);
            $uploadedFile->move($path, $filename);
            $result = $filepath.$filename;
            
            $client = new \GuzzleHttp\Client();
            $response = $client->request('POST', 'http://localhost:2121/UsersProjectFile', [
                'form_params' => [
                    "project_id" => $request->project_id,
                    "file" => $result,
                    "name" => $filename,
                    "users_id" => session('user_id')
                ]
            ]);
        } catch (\GuzzleHttp\Exception\ClientException $e) {
            return $e->getResponse()->getBody();
        }

        return back()->with('success', 'File added successfully');
    }

    // Hapus File (PUT)
    public function delete_file($id){
        try {
            $client = new \GuzzleHttp\Client();
            $response = $client->request('PUT', 'http://localhost:2121/RemoveUsersProjectFile/'.$id);
        } catch (\GuzzleHttp\Exception\ClientException $e) {
            return $e->getResponse()->getBody();
        }

        return back()->with('success', 'File deleted successfully');
    }
    
    // Hapus Project (DELETE)
    public function delete_project($id){
        try {
            $client = new \GuzzleHttp\Client();
            $response = $client->request('DELETE', 'http://localhost:2121/Project/'.$id);
        } catch (\GuzzleHttp\Exception\ClientException $e) {
            return $e->getResponse()->getBody();
        }

        return back()->with('success', 'Project deleted');
    }

    // Ubah Project (PUT)
    public function update_project(Request $request, $id){
        // $request->validate([
        //     'nama_project' => 'alpha_num'
        // ]); 

        try {
            $client = new \GuzzleHttp\Client();
            $response = $client->request('PUT', 'http://localhost:2121/Project/'.$id, [
                'form_params' => [
                    "name" => $request->nama_project,
                    "description" => $request->deskripsi,
                    "start_date" => $request->tanggal_mulai,
                    "end_date" => $request->tanggal_berakhir,
                    "status" => $request->status,
                    "users_id" => session('user_id')
                ]
            ]);
        } catch (\GuzzleHttp\Exception\ClientException $e) {
            return $e->getResponse()->getBody();
        }

        return back()->with('success', 'Project update success!');
    }
    
    // Ubah Project Complete (PUT)
    public function update_project_complete(Request $request, $id){
        try {
            $client = new \GuzzleHttp\Client();
            $response = $client->request('PUT', 'http://localhost:2121/Project/'.$id, [
                'form_params' => [
                    "name" => $request->name,
                    "description" => $request->description,
                    "start_date" => $request->start_date,
                    "end_date" => $request->end_date,
                    "status" => $request->status,
                    "file" => $request->file,
                    "users_id" => $request->users_id
                ]
            ]);
        } catch (\GuzzleHttp\Exception\ClientException $e) {
            return $e->getResponse()->getBody();
        }

        return back();
    }

    
    // ======= EDIT PROFILE =======
    // Edit Profile (GET)
    public function edit_profile(){
        $client = new \GuzzleHttp\Client();
        $request = $client->get('http://localhost:2121/Users/'.session('user_id'));
        $response = json_decode($request->getBody()->getContents());

        foreach ($response->data as $key => $value) {
            $data['user_info'][$key] = $value;
        }
        session(['photo_profile' => $data['user_info']['photo_profile']]);
        // return $data;
        return view('edit_profile')->with('data', $data);
    }
    // Edit Profile (PUT)
    public function update_profile(Request $request, $id){
        $client = new \GuzzleHttp\Client();
        $current_password = null;

        try {
            $req = $client->get('http://localhost:2121/Users/'.session('user_id'));
            $response = json_decode($req->getBody()->getContents());
            $current_password = $response->data->password;
        } catch (\GuzzleHttp\Exception\ClientException $e) {
            echo $e->getResponse()->getBody();
        }

        if($request->current_password==$current_password){
            try {
                if ($request->hasFile('foto_profil')) {
                    $uploadedFile = $request->file('foto_profil');
                    $filename = preg_replace('/\s+/', '_', session('username').'.'.$uploadedFile->getClientOriginalExtension());
                    $filepath = preg_replace('/\s+/', '_', '/img/user/');
                    $path = public_path($filepath);
                    $uploadedFile->move($path, $filename);
                    $result = $filepath.$filename;
                }
                else{
                    $result = $request->foto_profil_filename;
                }
                
                $response = $client->request('PUT', 'http://localhost:2121/Users/'.session('user_id'), [
                    'form_params' => [
                        "username" => $request->username,
                        "name" => $request->nama,
                        "email" => $request->email,
                        "password" => $request->new_password,
                        "photo_profile" => $result,
                        "no_telp" => $request->nomor_telepon,
                        "address" => $request->alamat
                    ]
                ]);
            } catch (\GuzzleHttp\Exception\ClientException $e) {
                return $e->getResponse()->getBody();
            }
        }
        else{
            return back()->with('error', 'Password not match any data!');
        }
        
        return back()->with('success', 'Profile updated');
    }

    // ======= DETAIL PROJEK =======
    // Edit Profile (GET)
    public function project_detail($id){
        $data = null;
        try {
            $client = new \GuzzleHttp\Client();
            
            $request = $client->get('http://localhost:2121/Project-detail/'.$id);
            $response = json_decode($request->getBody()->getContents());

            $data['detail_project'] = $response->data;
        } catch (\GuzzleHttp\Exception\ClientException $e) {
            echo $e->getResponse()->getBody();
        }

        $month = array(
            1 =>'Januari',
            2 =>'Februari',
            3 =>'Maret',
            4 =>'April',
            5 =>'Mei',
            6 =>'Juni',
            7 =>'Juli',
            8 =>'Agustus',
            9 =>'September',
            10 =>'Oktober',
            11 =>'November',
            12 =>'Desember'
        );

        $day = array(
            1 =>'Senin',
            2 =>'Selasa',
            3 =>'Rabu',
            4 =>'Kamis',
            5 =>'Jumat',
            6 =>'Sabtu',
            7 =>'Minggu'
        );
        $time = strtotime($data['detail_project']->start_date);
        $tanggal = date('d',$time);
        $bulan = date('m',$time);
        $tahun = date('Y',$time);
        $data['detail_project']->start_date = "{$tahun}-{$bulan}-{$tanggal}";

        $time = strtotime($data['detail_project']->end_date);
        $tanggal = date('d',$time);
        $bulan = date('m',$time);
        $tahun = date('Y',$time);
        $data['detail_project']->end_date = "{$tahun}-{$bulan}-{$tanggal}";
        
        $time = strtotime($data['detail_project']->start_date);
        $tanggal = date('d',$time);
        $bulan = $month[(int)date('m',$time)];
        $tahun = date('Y',$time);
        $data['detail_project']->tanggal_mulai = "{$tanggal} {$bulan} {$tahun}";

        $time = strtotime($data['detail_project']->end_date);
        $tanggal = date('d',$time);
        $bulan = $month[(int)date('m',$time)];
        $tahun = date('Y',$time);
        $data['detail_project']->tanggal_berakhir = "{$tanggal} {$bulan} {$tahun}";

        $time = strtotime($data['detail_project']->created_at);
        $tanggal = date('d',$time);
        $bulan = $month[(int)date('m',$time)];
        $tahun = date('Y',$time);
        $data['detail_project']->created_at = "{$tanggal} {$bulan} {$tahun}";
        
        if($data['detail_project']->updated_at!=null){
            $time = strtotime($data['detail_project']->updated_at);
            $tanggal = date('d',$time);
            $bulan = $month[(int)date('m',$time)];
            $tahun = date('Y',$time);
            $data['detail_project']->updated_at = "{$tanggal} {$bulan} {$tahun}";
        }
        else{
            $data['detail_project']->updated_at = '-';
        }

        // Karyawan yang terlibat
        try {
            $client = new \GuzzleHttp\Client();
            
            $request = $client->get('http://localhost:2121/listKaryawan/'.$id);
            $response = json_decode($request->getBody()->getContents());

            foreach ($response->data as $key => $value) {
                $data['karyawan'][$key] = $value;
            }
        } catch (\GuzzleHttp\Exception\ClientException $e) {
            echo $e->getResponse()->getBody();
        }
        
        // Pekerjaan sesuai project
        try {
            $client = new \GuzzleHttp\Client();
            
            $request = $client->get('http://localhost:2121/UsersProject/'.$id);
            $response = json_decode($request->getBody()->getContents());
            $data['list_pekerjaan'] = $response->data;
        } catch (\GuzzleHttp\Exception\ClientException $e) {
            echo $e->getResponse()->getBody();
        }
        
        // User list
        try {
            $client = new \GuzzleHttp\Client();
            
            $request = $client->get('http://localhost:2121/Users?sort_by=name&sort_order=desc&page_size=10&page_offset=0');
            $response = json_decode($request->getBody()->getContents());

            $data['users'] = $response->data;
        } catch (\GuzzleHttp\Exception\ClientException $e) {
            echo $e->getResponse()->getBody();
        }
        
        // Tipe Pekerjaan
        try {
            $client = new \GuzzleHttp\Client();
            
            $request = $client->get('http://localhost:2121/UsersProjectCategory?sort_by=name&sort_order=asc&page_size=10&page_offset=0');
            $response = json_decode($request->getBody()->getContents());

            $data['tipe_pekerjaan'] = $response->data;
        } catch (\GuzzleHttp\Exception\ClientException $e) {
            echo $e->getResponse()->getBody();
        }

        // File
        try {
            $client = new \GuzzleHttp\Client();
            
            $request = $client->get('http://localhost:2121/ProjectFile/'.$id);
            $response = json_decode($request->getBody()->getContents());

            $data['files'] = $response->data;
        } catch (\GuzzleHttp\Exception\ClientException $e) {
            echo $e->getResponse()->getBody();
        }
        // Nama dari pemilik (yang mengupload) file
        foreach ($data['files'] as $key => $value) {
            try {
                $client = new \GuzzleHttp\Client();
                
                $request = $client->get('http://localhost:2121/Users/'.$value->users_id);
                $response = json_decode($request->getBody()->getContents());
    
                $value->username = $response->data->username;
            } catch (\GuzzleHttp\Exception\ClientException $e) {
                echo $e->getResponse()->getBody();
            }
        }

        // Log
        try {
            $client = new \GuzzleHttp\Client();
            
            $request = $client->get('http://localhost:2121/Log/'.$id);
            $response = json_decode($request->getBody()->getContents());

            $data['log'] = $response->data;
        } catch (\GuzzleHttp\Exception\ClientException $e) {
            echo $e->getResponse()->getBody();
        }
        
        // return $data;
        return view('detail_project')->with('data', $data);
    }  
    
    // Tambah Pekerjaan Project (POST)
    public function tambah_pekerjaan(Request $request){
        // $request->validate([
        //     'nama_tugas' => 'alpha_num'
        // ]); 
        
        try {
            $client = new \GuzzleHttp\Client();
            $response = $client->request('POST', 'http://localhost:2121/UsersProject', [
                'form_params' => [
                    "project_id" => $request->project_id,
                    "users_id" => $request->user,
                    "name" => $request->nama_tugas,
                    "version" => $request->version,
                    "description" => $request->deskripsi,
                    "status" => $request->status,
                    "parent" => '',
                    "users_id_pemberi" => session('user_id'),
                    "category_id" => $request->tipe_pekerjaan
                ]
            ]);
        } catch (\GuzzleHttp\Exception\ClientException $e) {
            return $e->getResponse()->getBody();
        }

        return back()->with('success', 'Pekerjaan added successful');
    }

    // Cetak Report Project (POST)
    public function cetak(Request $request){
        $id = $request->id;
        
        $data = null;
        try {
            $client = new \GuzzleHttp\Client();
            
            $request = $client->get('http://localhost:2121/Pemesanan/'.$id);
            $response = json_decode($request->getBody()->getContents());

            $data['pemesanan'] = $response->data;
        } catch (\GuzzleHttp\Exception\ClientException $e) {
            echo $e->getResponse()->getBody();
        }

        $month = array(
            1 =>'Januari',
            2 =>'Februari',
            3 =>'Maret',
            4 =>'April',
            5 =>'Mei',
            6 =>'Juni',
            7 =>'Juli',
            8 =>'Agustus',
            9 =>'September',
            10 =>'Oktober',
            11 =>'November',
            12 =>'Desember'
        );

        $day = array(
            1 =>'Senin',
            2 =>'Selasa',
            3 =>'Rabu',
            4 =>'Kamis',
            5 =>'Jumat',
            6 =>'Sabtu',
            7 =>'Minggu'
        );
        $time = strtotime($data['pemesanan']->tanggal_pemesanan);
        $tanggal = date('d',$time);
        $bulan = date('m',$time);
        $tahun = date('Y',$time);
        $data['pemesanan']->tanggal_pemesanan = "{$tahun}-{$bulan}-{$tanggal}";

        
        
        $time = strtotime($data['pemesanan']->tanggal_pemesanan);
        $tanggal = date('d',$time);
        $bulan = $month[(int)date('m',$time)];
        $tahun = date('Y',$time);
        $data['pemesanan']->tanggal_pemesanan = "{$tanggal} {$bulan} {$tahun}";
        
        $pdf = PDF::loadView('layout.cetak', $data)->setPaper(array(0, 0, 465, 710), 'portrait');
        return $pdf->stream();
        
        // return view('layout.cetak')->with('data', $data);
    }
    // ----------------------------------------------------------------


    // ======= PETUGAS =======
    // Petugas (GET)
    public function petugas(){
        if(session('level')=='Karyawan'){
            return redirect('/');
        }
        $data = null;
        try {
            $client = new \GuzzleHttp\Client();
            
            $request = $client->get('http://localhost:2121/Level');
            $response = json_decode($request->getBody()->getContents());

            $data['level'] = $response->data;
        } catch (\GuzzleHttp\Exception\ClientException $e) {
            echo $e->getResponse()->getBody();
        }
        try {
            $client = new \GuzzleHttp\Client();
            
            $request = $client->get('http://localhost:2121/Petugas');
            $response = json_decode($request->getBody()->getContents());

            foreach ($response->data as $key => $value) {
                $data['petugas'][$key] = $value;
            }
        } catch (\GuzzleHttp\Exception\ClientException $e) {
            echo $e->getResponse()->getBody();
        }
        
        // return $data;
        return view('petugas')->with('data', $data);
    }
    // Tambah Petugas (POST)
    public function create_petugas(Request $request){
        // $request->validate([
        //     'nama_tipe_pekerjaan' => 'alpha_num'
        // ]);    
        
        try {
            $client = new \GuzzleHttp\Client();
            $response = $client->request('POST', 'http://localhost:2121/Petugas', [
                'form_params' => [
                    'username' => $request->valusername,
                    'password' => $request->valpassword,
                    'nama_petugas' => $request->valnama_petugas,
                    'id_level' => $request->id_level
                ]
            ]);
        } catch (\GuzzleHttp\Exception\ClientException $e) {
            return $e->getResponse()->getBody();
        }

        return back();
    }
    // Hapus Petugas (DELETE)
    public function delete_petugas($id){
        try {
            $client = new \GuzzleHttp\Client();
            $response = $client->request('DELETE', 'http://localhost:2121/Petugas/'.$id);
        } catch (\GuzzleHttp\Exception\ClientException $e) {
            return $e->getResponse()->getBody();
        }

        return back();
    }
    // Ubah Petugas (PUT)
    public function update_petugas(Request $request, $id){
        // $request->validate([
        //     'nama_tipe_pekerjaan' => 'alpha_num'
        // ]); 
        
        try {
            $client = new \GuzzleHttp\Client();
            $response = $client->request('PUT', 'http://localhost:2121/Petugas/'.$id, [
                'form_params' => [
                    'username' => $request->username,
                    'password' => $request->password,
                    'nama_petugas' => $request->nama_petugas,
                    'id_level' => $request->id_level
                ]
            ]);
        } catch (\GuzzleHttp\Exception\ClientException $e) {
            return $e->getResponse()->getBody();
        }

        return back();
    }


    // ======= Level Petugas =======
    //Level Petugas (GET)
    public function level_petugas(){
        if(session('level')=='Petugas'){
            return redirect('/');
        }
        $data = null;
        try {
            $client = new \GuzzleHttp\Client();
            
            $request = $client->get('http://localhost:2121/Level');
            $response = json_decode($request->getBody()->getContents());

            $data['level'] = $response->data;
        } catch (\GuzzleHttp\Exception\ClientException $e) {
            echo $e->getResponse()->getBody();
        }
        
        // return $data;
        return view('level')->with('data', $data);
    }
    // Tambah Level Petugas (POST)
    public function create_level_petugas(Request $request){
         
        try {
            $client = new \GuzzleHttp\Client();
            $response = $client->request('POST', 'http://localhost:2121/Level', [
                'form_params' => [
                    'nama_level' => $request->valnamalevel
                ]
            ]);
        } catch (\GuzzleHttp\Exception\ClientException $e) {
            return $e->getResponse()->getBody();
        }

        return back();
    }
    // Hapus Level Petugas (DELETE)
    public function delete_level_petugas($id){
        try {
            $client = new \GuzzleHttp\Client();
            $response = $client->request('DELETE', 'http://localhost:2121/Level/'.$id);
        } catch (\GuzzleHttp\Exception\ClientException $e) {
            return $e->getResponse()->getBody();
        }

        return back();
    }
    // Ubah Level Petugas (PUT)
    public function update_level_petugas(Request $request, $id){
        // $request->validate([
        //     'nama_tipe_pekerjaan' => 'alpha_num'
        // ]); 
        
        try {
            $client = new \GuzzleHttp\Client();
            $response = $client->request('PUT', 'http://localhost:2121/Level/'.$id, [
                'form_params' => [
                    'nama_level' => $request->valnamalevel
                ]
            ]);
        } catch (\GuzzleHttp\Exception\ClientException $e) {
            return $e->getResponse()->getBody();
        }

        return back();
    }

    // ======= Transportasi =======
    //Transportasi (GET)
    public function transportasi(){
        
        $data = null;
        try {
            $client = new \GuzzleHttp\Client();
            
            $request = $client->get('http://localhost:2121/TipeTransportasi');
            $response = json_decode($request->getBody()->getContents());

            $data['tipe_transportasi'] = $response->data;
        } catch (\GuzzleHttp\Exception\ClientException $e) {
            echo $e->getResponse()->getBody();
        }
        try {
            $client = new \GuzzleHttp\Client();
            
            $request = $client->get('http://localhost:2121/Transportasi');
            $response = json_decode($request->getBody()->getContents());

            $data['transportasi'] = $response->data;
        } catch (\GuzzleHttp\Exception\ClientException $e) {
            echo $e->getResponse()->getBody();
        }
        
        // return $data;
        return view('transportasi')->with('data', $data);
    }
    // Tambah Transportasi (POST)
    public function create_transportasi(Request $request){
         
        try {
            $client = new \GuzzleHttp\Client();
            $response = $client->request('POST', 'http://localhost:2121/Transportasi', [
                'form_params' => [
                    'kode' => $request->valkode,
                    'jumlah_kursi' => $request->valjumlahkursi,
                    'keterangan' => $request->valketerangan,
                    'nama_transportasi' => $request->valnamatransportasi,
                    'id_tipe_transportasi' => $request->validtipetransportasi
                ]
            ]);
        } catch (\GuzzleHttp\Exception\ClientException $e) {
            return $e->getResponse()->getBody();
        }

        return back();
    }
    // Hapus Transportasi (DELETE)
    public function delete_transportasi($id){
        try {
            $client = new \GuzzleHttp\Client();
            $response = $client->request('DELETE', 'http://localhost:2121/Transportasi/'.$id);
        } catch (\GuzzleHttp\Exception\ClientException $e) {
            return $e->getResponse()->getBody();
        }

        return back();
    }
    // Ubah Transportasi (PUT)
    public function update_transportasi(Request $request, $id){
        try {
            $client = new \GuzzleHttp\Client();
            $response = $client->request('PUT', 'http://localhost:2121/Transportasi/'.$id, [
                'form_params' => [
                    'kode' => $request->valkode,
                    'jumlah_kursi' => $request->valjumlahkursi,
                    'keterangan' => $request->valketerangan,
                    'nama_transportasi' => $request->valnamatransportasi,
                    'id_tipe_transportasi' => $request->validtipetransportasi
                ]
            ]);
        } catch (\GuzzleHttp\Exception\ClientException $e) {
            return $e->getResponse()->getBody();
        }

        return back();
    }



    // ======= Tipe Transportasi =======
    //Tipe Transportasi (GET)
    public function tipe_transportasi(){
       
        $data = null;
        try {
            $client = new \GuzzleHttp\Client();
            
            $request = $client->get('http://localhost:2121/TipeTransportasi');
            $response = json_decode($request->getBody()->getContents());

            $data['tipe_transportasi'] = $response->data;
        } catch (\GuzzleHttp\Exception\ClientException $e) {
            echo $e->getResponse()->getBody();
        }
        
        // return $data;
        return view('tipe_transportasi')->with('data', $data);
    }
    // Tambah Tipe Transportasi (POST)
    public function create_tipe_transportasi(Request $request){
         
        try {
            $client = new \GuzzleHttp\Client();
            $response = $client->request('POST', 'http://localhost:2121/TipeTransportasi', [
                'form_params' => [
                    'nama_tipe' => $request->valnamatipe,
                    'keterangan' => $request->valketerangan
                ]
            ]);
        } catch (\GuzzleHttp\Exception\ClientException $e) {
            return $e->getResponse()->getBody();
        }

        return back();
    }
    // Hapus Transportasi (DELETE)
    public function delete_tipe_transportasi($id){
        try {
            $client = new \GuzzleHttp\Client();
            $response = $client->request('DELETE', 'http://localhost:2121/TipeTransportasi/'.$id);
        } catch (\GuzzleHttp\Exception\ClientException $e) {
            return $e->getResponse()->getBody();
        }

        return back();
    }
    // Ubah Transportasi (PUT)
    public function update_tipe_transportasi(Request $request, $id){
        try {
            $client = new \GuzzleHttp\Client();
            $response = $client->request('PUT', 'http://localhost:2121/TipeTransportasi/'.$id, [
                'form_params' => [
                    'nama_tipe' => $request->valnamatipe,
                    'keterangan' => $request->valketerangan
                ]
            ]);
        } catch (\GuzzleHttp\Exception\ClientException $e) {
            return $e->getResponse()->getBody();
        }

        return back();
    }


     // ======= Rute =======
    //Rute (GET)
    public function rute(){
        
        $data = null;
        try {
            $client = new \GuzzleHttp\Client();
            
            $request = $client->get('http://localhost:2121/Rute');
            $response = json_decode($request->getBody()->getContents());

            $data['rute'] = $response->data;
        } catch (\GuzzleHttp\Exception\ClientException $e) {
            echo $e->getResponse()->getBody();
        }
        try {
            $client = new \GuzzleHttp\Client();
            
            $request = $client->get('http://localhost:2121/Transportasi');
            $response = json_decode($request->getBody()->getContents());

            $data['transportasi'] = $response->data;
        } catch (\GuzzleHttp\Exception\ClientException $e) {
            echo $e->getResponse()->getBody();
        }
        
        // return $data;
        return view('rute')->with('data', $data);
    }
    // Tambah Tipe Transportasi (POST)
    public function create_rute(Request $request){
         
        try {
            $client = new \GuzzleHttp\Client();
            $response = $client->request('POST', 'http://localhost:2121/Rute', [
                'form_params' => [
                    'tujuan' => $request->valtujuan,
                    'rute_awal' => $request->valruteawal,
                    'rute_akhir' => $request->valruteakhir                                              ,
                ]
            ]);
        } catch (\GuzzleHttp\Exception\ClientException $e) {
            return $e->getResponse()->getBody();
        }

        return back();
    }
    // Hapus Transportasi (DELETE)
    public function delete_rute($id){
        try {
            $client = new \GuzzleHttp\Client();
            $response = $client->request('DELETE', 'http://localhost:2121/Rute/'.$id);
        } catch (\GuzzleHttp\Exception\ClientException $e) {
            return $e->getResponse()->getBody();
        }

        return back();
    }
    // Ubah Transportasi (PUT)
    public function update_rute(Request $request, $id){
        try {
            $client = new \GuzzleHttp\Client();
            $response = $client->request('PUT', 'http://localhost:2121/Rute/'.$id, [
                'form_params' => [
                    'tujuan' => $request->valtujuan,
                    'rute_awal' => $request->valruteawal,
                    'rute_akhir' => $request->valruteakhir
                ]
            ]);
        } catch (\GuzzleHttp\Exception\ClientException $e) {
            return $e->getResponse()->getBody();
        }

        return back();
    }

     // ======= Penumpang =======
    //Penumpang (GET)
    public function penumpang(){
       
        $data = null;
        try {
            $client = new \GuzzleHttp\Client();
            
            $request = $client->get('http://localhost:2121/Penumpang');
            $response = json_decode($request->getBody()->getContents());

            $data['penumpang'] = $response->data;
        } catch (\GuzzleHttp\Exception\ClientException $e) {
            echo $e->getResponse()->getBody();
        }
        
        // return $data;
        return view('penumpang')->with('data', $data);
    }
    // Hapus Penumpang (DELETE)
    public function delete_penumpang($id){
        try {
            $client = new \GuzzleHttp\Client();
            $response = $client->request('DELETE', 'http://localhost:2121/Penumpang/'.$id);
        } catch (\GuzzleHttp\Exception\ClientException $e) {
            return $e->getResponse()->getBody();
        }

        return back();
    }

      // ======= Pemesanan =======
    //Pemesanan (GET)
    public function pemesanan(){
       
        $data = null;
        try {
            $client = new \GuzzleHttp\Client();
            
            $request = $client->get('http://localhost:2121/Pemesanan');
            $response = json_decode($request->getBody()->getContents());

            $data['pemesanan'] = $response->data;
        } catch (\GuzzleHttp\Exception\ClientException $e) {
            echo $e->getResponse()->getBody();
        }
       
        
        // return $data;
        return view('pemesanan')->with('data', $data);
    }
    
    // Hapus Pemesanan (DELETE)
    public function delete_pemesanan($id){
        try {
            $client = new \GuzzleHttp\Client();
            $response = $client->request('DELETE', 'http://localhost:2121/Pemesanan/'.$id);
        } catch (\GuzzleHttp\Exception\ClientException $e) {
            return $e->getResponse()->getBody();
        }

        return back();
    }
    // Ubah Transportasi (PUT)
    public function update_pemesanan(Request $request, $id){
        try {
            $client = new \GuzzleHttp\Client();
            $response = $client->request('PUT', 'http://localhost:2121/VerifikasiAdmin/'.$id);
        } catch (\GuzzleHttp\Exception\ClientException $e) {
            return $e->getResponse()->getBody();
        }

        return back();
    }

     // ======= Pemberangkatan =======
    //Pemberangkatan (GET)
    public function pemberangkatan(){
        if(session('level')=='Karyawan'){
            return redirect('/');
        }
        $data = null;
        try {
            $client = new \GuzzleHttp\Client();
            
            $request = $client->get('http://localhost:2121/Rute');
            $response = json_decode($request->getBody()->getContents());

            $data['rute'] = $response->data;
        } catch (\GuzzleHttp\Exception\ClientException $e) {
            echo $e->getResponse()->getBody();
        }
        try {
            $client = new \GuzzleHttp\Client();
            
            $request = $client->get('http://localhost:2121/Transportasi');
            $response = json_decode($request->getBody()->getContents());

            $data['transportasi'] = $response->data;
        } catch (\GuzzleHttp\Exception\ClientException $e) {
            echo $e->getResponse()->getBody();
        }
        try {
            $client = new \GuzzleHttp\Client();
            
            $request = $client->get('http://localhost:2121/Pemberangkatan');
            $response = json_decode($request->getBody()->getContents());

            $data['pemberangkatan'] = $response->data;
        } catch (\GuzzleHttp\Exception\ClientException $e) {
            echo $e->getResponse()->getBody();
        }
        
        // return $data;
        return view('pemberangkatan')->with('data', $data);
    }
    // Tambah Pemberangkatan (POST)
    public function create_pemberangkatan(Request $request){
         
        try {
            $client = new \GuzzleHttp\Client();
            $response = $client->request('POST', 'http://localhost:2121/Pemberangkatan', [
                'form_params' => [
                    'id_transportasi' => $request->validtransportasi,
                    'tanggal_berangkat' => $request->valtanggalberangkat,
                    'id_rute' => $request->validrute,
                    'jam_berangkat' => $request->valjamberangkat,
                    'lama_pemberangkatan' => $request->vallamapemberangkatan,
                    'jam_sampai' => $request->valjamsampai,
                    'jam_checkin' => $request->valjamcheckin,
                    'harga' => $request->valharga,
                    'nama' => $request->valnamapemberangkatan
                ]
            ]);
        } catch (\GuzzleHttp\Exception\ClientException $e) {
            return $e->getResponse()->getBody();
        }

        return back();
    }
    // Hapus Pemberangkatan (DELETE)
    public function delete_pemberangkatan($id){
        try {
            $client = new \GuzzleHttp\Client();
            $response = $client->request('DELETE', 'http://localhost:2121/Pemberangkatan/'.$id);
        } catch (\GuzzleHttp\Exception\ClientException $e) {
            return $e->getResponse()->getBody();
        }

        return back();
    }
    // Ubah Pemberangkatan (PUT)
    public function update_pemberangkatan(Request $request, $id){
        try {
            $client = new \GuzzleHttp\Client();
            $response = $client->request('PUT', 'http://localhost:2121/Pemberangkatan/'.$id, [
                'form_params' => [
                    'id_transportasi' => $request->validtransportasi,
                    'tanggal_berangkat' => $request->valtanggalberangkat,
                    'id_rute' => $request->validrute,
                    'jam_berangkat' => $request->valjamberangkat,
                    'lama_pemberangkatan' => $request->vallamapemberangkatan,
                    'jam_sampai' => $request->valjamsampai,
                    'jam_checkin' => $request->valjamcheckin,
                    'harga' => $request->valharga,
                    'nama' => $request->valnamapemberangkatan
                ]
            ]);
        } catch (\GuzzleHttp\Exception\ClientException $e) {
            return $e->getResponse()->getBody();
        }

        return back();
    }

}


