var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');

var indexRouter = require('./routes/index');

var app = express();
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));


app.use(function(req, res, next) {
  res.setHeader("Access-Control-Allow-Methods", "POST, PUT, OPTIONS, DELETE, GET");
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  req.connection.setTimeout(30 * 1000);
  res.connection.setTimeout(30 * 1000);
  next();
});


app.use('/', indexRouter);


app.use(function(err, req, res, next) {
    res.status(err.status || 500)
    .json({
      status: 'error',
      message: err.message
    });
  });

module.exports = app;
