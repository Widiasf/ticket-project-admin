var conn = require('../lib/database');
var Axios = require('axios');
var Joi = require('joi');
db = conn.db;


// -------------------------------------------------------------------------------------------------------------------------------------------------
// --------------------------------------------------------CREATE----------------------------------------------------------------------------

async function insert(req, res) { 
  const result = req.body;
  params = req.body;

  if (result.error) return res.status(400).send(
    { success: false,
      message: result.error.details[0].message,
      data: null
    }); 

  status_postgre = await insertProject(params); //insert data user kedalam table user
  if (status_postgre.success == false ) {
    res.status(400).json(status_postgre);
  }
  project__id = status_postgre.data.project_id;
  status_postgre_log = await insertLog(params.users_id,project__id); //insert data user kedalam table user
  if (status_postgre_log.success == false ) {
    res.status(400).json(status_postgre_log);
  }
      res.status(200).json({ 
        success: true,
        message: "Data has been inserted into database",
        data: null
      }); 
}
async function insertProject(params) { //funsgi untuk insert data kedalam database postgre
  try {
    return {
      success: true,
      data: await  db.one('insert into project(name,start_date,end_date,status,file,description)'+
     'values(${name},${start_date},${end_date},${status},${file},${description}) RETURNING project_id',
     params)
    };
  } catch (error) {
    return {      
      success: false,
      message: 'Error in insert project into Database: '+ error.message + ' duplicate project name',
      data: null
    }
  }
}
async function insertChild(req, res) { 
  params = req.body;
  status_postgre = await insertChildProject(params); //insert data user kedalam table user
  if (status_postgre.success == false ) {
    res.status(400).json(status_postgre);
  }
  project__id = status_postgre.data.project_id;
  status_postgre_log = await insertLog(params.users_id,project__id); //insert data user kedalam table user
  if (status_postgre_log.success == false ) {
    res.status(400).json(status_postgre_log);
  }
      res.status(200).json({ 
        success: true,
        message: "Data has been inserted into database",
        data: null
      }); 
}
async function insertChildProject(params) { //funsgi untuk insert data kedalam database postgre
  let project_id = params.project_id;
  let name = params.name;
  let start_date = params.start_date;
  let end_date = params.end_date;
  let status = params.status;
  let file = params.file;
  let description = params.description;
  
  data_version = await db.result('select MAX(version) AS jumlah from project where project_id = $1 or parent=$1 and is_deleted=false',project_id);
  let version = Number(data_version.rows[0].jumlah)+1;
  try {
    return {
      success: true,
      data: await  db.one('insert into project(name,start_date,end_date,status,file,description,parent,version)'+
     'values($1,$2,$3,$4,$5,$6,$7,$8) RETURNING project_id',
     [name,start_date,end_date,status,file,description,project_id,version])
    };
  } catch (error) {
    return {      
      success: false,
      message: 'Error in insert project into Database: '+ error.message + ' duplicate project name',
      data: null
    }
  }
}


async function insertLog(users__id,project__id) { //funsgi untuk insert data kedalam database postgre
  let project_id = project__id;
  let users_id = users__id;
  let type = "Membuat project"
  try {
    return {
      success: true,
      data: await  db.one('insert into log(project_id,users_id,type)'+
     'values($1,$2,$3) RETURNING log_id',
     [project_id,users_id,type])
    };
  } catch (error) {
    return {      
      success: false,
      message: 'Error in insert project into Database: '+ error.detail,
      data: null
    }
  }
}


// -------------------------------------------------------------------------------------------------------------------------------------------------
// --------------------------------------------------------CREATE END----------------------------------------------------------------------------


// -------------------------------------------------------------------------------------------------------------------------------------------------
// --------------------------------------------------------READ----------------------------------------------------------------------------


function read(req, res, next) {
    let sort_order = (req.query.sort_order) ? req.query.sort_order : '';
    let sort_by = (req.query.sort_by) ? ' order by ' + req.query.sort_by : '';
    let page_offset = (req.query.page_offset) ? ' offset ' + req.query.page_offset : '';
    let page_size = (req.query.page_size) ? ' limit ' + parseInt(req.query.page_size) : '';
  
    query_select = ' select p.description,p.persentase,p.project_id,p.name,p.start_date,p.end_date,p.status,p.file,p.created_at, l.users_id from project p, log l where p.project_id=l.project_id and l.type like \'%$1#%\' and l.users_id=$2 and is_deleted=false' + sort_by + ' ' + sort_order + page_size + page_offset;
    query_all = 'select count(*) from project WHERE is_deleted=false';
    Promise.all([db.result(query_select,["Membuat project",req.params.id]), db.result(query_all)])
      .then(function (data) {
        res.status(200)
          .json({
            success: true,
            message: 'Retrieved project data',
            num_of_data: data[0].rowCount,
            data: data[0].rows,
            total_data : data[1].rows[0].count
            
          });
      })
      .catch(function (err) {
        res.status(400)
          .json({
            success: false,
            message: 'Error occured: '+err.message,
            data: null,
          });
      });
  }

  //red pekerjaan di project
  function readListKaryawan(req, res, next) {
    query_all = 'select distinct u.* from users u join users_project up on (up.users_id=u.users_id) join project p on (p.project_id=up.project_id) WHERE p.project_id=$1 and p.is_deleted=false ';
    Promise.all([db.result(query_all,req.params.id)])
      .then(function (data) {
        res.status(200)
          .json({
            success: true,
            message: 'Retrieved project data',
            data: data[0].rows,
            num_of_data: data[0].rowCount
          });
      })
      .catch(function (err) {
        res.status(400)
          .json({
            success: false,
            message: 'Error occured: '+err.message,
            data: null,
          });
      });
  }
function readSingle(req,res,next){ 
  db.one('select p.description,p.version,p.updated_at,p.parent,p.persentase,project_id,name,start_date,end_date,status,file,created_at from project p where project_id= $1 and is_deleted=false', req.params.id)
  .then(function(data) {
    res.status(200)
    .json({
      success: true,
      message: 'Retrieved project category data',
      data: data
      });
  })
  .catch(function (err) {
    res.status(400)
    .json({
      success: false,
      message: 'Error occured : ' + err.message,
      data: null
    });
  });
}

function readProjectKaryawan(req, res, next) {
  query_all = 'select p.* from users u join users_project up on (up.users_id=u.users_id) join project p on (p.project_id=up.project_id) WHERE up.users_id=$1 and p.is_deleted=false ';
  Promise.all([db.result(query_all,req.params.id)])
    .then(function (data) {
      res.status(200)
        .json({
          success: true,
          message: 'Retrieved project data',
          data: data[0].rows,
          num_of_data: data[0].rowCount
        });
    })
    .catch(function (err) {
      res.status(400)
        .json({
          success: false,
          message: 'Error occured: '+err.message,
          data: null,
        });
    });
}

  //red Log
  function readLog(req, res, next) {
    query_all = 'select u.photo_profile,l.log_id,l.users_id,u.username,l.created_at,l.type,l.target_object_id, l.target_users_id from log l inner join users u on (l.users_id=u.users_id) and l.project_id=$1'
    //inner join users_project up on (up.users_project_id=l.target_object_id) and l.project_id=$1 ';
    Promise.all([db.result(query_all,req.params.id)])
      .then(function (data) {
        res.status(200)
          .json({
            success: true,
            message: 'Retrieved project data',
            data: data[0].rows,
            num_of_data: data[0].rowCount
          });
      })
      .catch(function (err) {
        res.status(400)
          .json({
            success: false,
            message: 'Error occured: '+err.message,
            data: null,
          });
      });
  }

  function readProjectAdmin(req, res, next) {
    query_select = ' select p.description,p.persentase,p.project_id,p.name,p.start_date,p.end_date,p.status,p.file,p.created_at, l.users_id from project p, log l where p.project_id=l.project_id and l.type like \'%$1#%\'  and is_deleted=false';
    query_all = 'select count(*) from project WHERE is_deleted=false';
    Promise.all([db.result(query_select,["Membuat project"]), db.result(query_all)])
      .then(function (data) {
        res.status(200)
          .json({
            success: true,
            message: 'Retrieved project data',
            num_of_data: data[0].rowCount,
            data: data[0].rows,
            total_data : data[1].rows[0].count
            
          });
      })
      .catch(function (err) {
        res.status(400)
          .json({
            success: false,
            message: 'Error occured: '+err.message,
            data: null,
          });
      });
  }
  
// -------------------------------------------------------------------------------------------------------------------------------------------------
// --------------------------------------------------------READ END----------------------------------------------------------------------------  

// -------------------------------------------------------------------------------------------------------------------------------------------------
// --------------------------------------------------------UPDATE----------------------------------------------------------------------------

async function update(req, res) { //fungsi untuk merubah data user 
  params = req.body;
  project_id = req.params.id;
  status_postgre = await updateProject(params,project_id);  
  if (status_postgre.success == false ) {
    res.status(400).json(status_postgre);
    return
  }
  status_postgre_log = await updateLog(params.users_id,project_id); //insert data user kedalam table user
  if (status_postgre_log.success == false ) {
    res.status(400).json(status_postgre_log);
  }
      res.status(200).json({        
        success: 'true',
        message: "Data has been updated",
        data: null
      });
}

async function updateProject(params,project_id) { //fungsi untuk merubah data user 
   let name = params.name;
   let start_date = params.start_date;
   let end_date = params.end_date;
   let status = params.status;
   let description = params.description;
    try {
      return {
        success: true,
        data: await db.none(`update project set
        name   = '${name}',
        start_date   = '${start_date}',
        end_date   = '${end_date}',
        status   = '${status}',
        description = '${description}',
        updated_at   = CURRENT_TIMESTAMP
        where project_id = '${project_id}'`)
      };
    } catch (error) {
      return {        
        success: false,
        message: 'Error in update user : '+ error.message,
        data: null
      }
    }
}

async function updateLog(users__id,project__id) { //funsgi untuk insert data kedalam database postgre
  let project_id = project__id;
  let users_id = users__id;
  let type = "Mengubah project"
  try {
    return {
      success: true,
      data: await  db.one('insert into log(project_id,users_id,type)'+
     'values($1,$2,$3) RETURNING log_id',
     [project_id,users_id,type])
    };
  } catch (error) {
    return {      
      success: false,
      message: 'Error in insert project into Database: '+ error.detail,
      data: null
    }
  }
}
  
// -------------------------------------------------------------------------------------------------------------------------------------------------
// --------------------------------------------------------UPDATE END----------------------------------------------------------------------------  
// -------------------------------------------------------------------------------------------------------------------------------------------------
// --------------------------------------------------------DELETE----------------------------------------------------------------------------
async function remove(req, res) { //fungsi untuk merubah data user 
  let project_id = req.params.id;
  status_postgre = await removeProject(project_id);  
  if (status_postgre.success == false ) {
    res.status(400).json(status_postgre);
    return
  }
  status_postgre_remove_child = await removeChild(project_id);  
  if (status_postgre_remove_child.success == false ) {
    res.status(400).json(status_postgre_remove_child);
    return
  }
  let params=req.body;
  status_postgre_log = await removePekerjaan(project_id);  
  if (status_postgre.success == false ) {
    res.status(400).json(status_postgre);
    return
  }
      res.status(200).json({        
        success: 'true',
        message: "Data has been removed",
        data: null
      });
}

async function removeProject(project_id) { //fungsi untuk merubah data user 
    try {
      return {
        success: true,
        data: await db.result('Update project set is_deleted=true,deleted_at=CURRENT_TIMESTAMP WHERE project_id = $1', project_id)
      };
    } catch (error) {
      return {        
        success: false,
        message: 'Error in deleted users_project : '+ error.message,
        data: null
      }
    }
}
async function removeChild(project_id) { //fungsi untuk merubah data user 
  try {
    return {
      success: true,
      data: await db.result('Update project set is_deleted=true,deleted_at=CURRENT_TIMESTAMP WHERE parent = $1 or project_id=$1', project_id)
    };
  } catch (error) {
    return {        
      success: false,
      message: 'Error in deleted users_project : '+ error.message,
      data: null
    }
  }
}
async function removePekerjaan(project_id) { //fungsi untuk merubah data user 
  try {
    return {
      success: true,
      data: await db.result('Update users_project set is_deleted=true,deleted_at=CURRENT_TIMESTAMP WHERE project_id = $1', project_id)
    };
  } catch (error) {
    return {      
      success: false,
      message: 'Error in insert project into Database: '+ error.detail,
      data: null
    }
  }
}

// -------------------------------------------------------------------------------------------------------------------------------------------------
// --------------------------------------------------------DELETE END----------------------------------------------------------------------------  

function validate(body) {
  const schema = {
    name: Joi.string(),
    start_date: Joi.string(),
    end_date: Joi.string(),
    status: Joi.string(),
    file: Joi.string(),
    description: Joi.string(),
    users_id: Joi.string()
  };
  return Joi.validate(body, schema);
}


module.exports = {
  create : insert,
  read : read,
  readSingle : readSingle,
  update : update,
  delete : remove,
  insertChild : insertChild,
  readListKaryawan : readListKaryawan,
  readProjectKaryawan : readProjectKaryawan,
  readLog : readLog,
  readProjectAdmin : readProjectAdmin
};