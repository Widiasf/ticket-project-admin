var conn = require('../lib/database');
var Axios = require('axios');
var Joi = require('joi');
db = conn.db;


// -------------------------------------------------------------------------------------------------------------------------------------------------
// --------------------------------------------------------CREATE----------------------------------------------------------------------------

async function insert(req, res) { 
  const result = validate(req.body);
  params = req.body;

  if (result.error) return res.status(400).send(
    { success: false,
      message: result.error.details[0].message,
      data: null
    }); 

  status_postgre = await insertPenumpang(params); //insert data user kedalam table user
  if (status_postgre.success == false ) {
    res.status(400).json(status_postgre);
  }

      res.status(200).json({ 
        success: true,
        message: "Data has been inserted into database",
        data: null
      }); 
}
async function insertPenumpang(params) { //funsgi untuk insert data kedalam database postgre
  try {
    return {
      success: true,
      data: await  db.one('insert into penumpang(username,password,nama_penumpang,alamat_penumpang,tanggal_lahir,jenis_kelamin,telefone)'+
     'values(${username},${password},${nama_penumpang},${alamat_penumpang},${tanggal_lahir},${jenis_kelamin},${telefone}) RETURNING id_penumpang',
     params)
    };
  } catch (error) {
    return {      
      success: false,
      message: 'Error in insert penumpang into Database: '+ error.detail,
      data: null
    }
  }
}
// -------------------------------------------------------------------------------------------------------------------------------------------------
// --------------------------------------------------------CREATE END----------------------------------------------------------------------------


// -------------------------------------------------------------------------------------------------------------------------------------------------
// --------------------------------------------------------READ----------------------------------------------------------------------------

function read(req, res, next) {
    let sort_order = (req.query.sort_order) ? req.query.sort_order : '';
    let sort_by = (req.query.sort_by) ? ' order by ' + req.query.sort_by : '';
    let page_offset = (req.query.page_offset) ? ' offset ' + req.query.page_offset : '';
    let page_size = (req.query.page_size) ? ' limit ' + parseInt(req.query.page_size) : '';
    
    query_select = ' select * from penumpang ' + sort_by + ' ' + sort_order + page_size + page_offset;
    query_all = 'select count(*) from penumpang';
    Promise.all([db.result(query_select), db.result(query_all)])
      .then(function (data) {
        res.status(200)
          .json({
            success: true,
            message: 'Retrieved penumpang data',
            num_of_data: data[0].rowCount,
            data: data[0].rows,
            total_data : data[1].rows[0].count
            
          });
      })
      .catch(function (err) {
        res.status(400)
          .json({
            success: false,
            message: 'Error occured: '+err.message,
            data: null,
          });
      });
  }

function readSingle(req,res,next){ 
  db.one('select * from penumpang where id_penumpang=$1', req.params.id)
  .then(function(data) {
    res.status(200)
    .json({
      success: true,
      message: 'Retrieved penumpang data',
      data: data
      });
  })
  .catch(function (err) {
    res.status(400)
    .json({
      success: false,
      message: 'Error occured : ' + err.message,
      data: null
    });
  });
}

function loginPenumpang(req,res,next){ 
  db.one('select * from penumpang where username=$1 and password=$2', [req.body.username,req.body.password])
  .then(function(data) {
    res.status(200)
    .json({
      success: true,
      message: 'Retrieved penumpang data',
      data: data
      });
  })
  .catch(function (err) {
    res.status(400)
    .json({
      success: false,
      message: 'Error occured : ' + err.message,
      data: null
    });
  });
}
// -------------------------------------------------------------------------------------------------------------------------------------------------
// --------------------------------------------------------READ END----------------------------------------------------------------------------  

// -------------------------------------------------------------------------------------------------------------------------------------------------
// --------------------------------------------------------UPDATE----------------------------------------------------------------------------

async function update(req, res) { //fungsi untuk merubah data user 
  params = req.body;
  id_penumpang= req.params.id;
  status_postgre = await updatePenumpang(params,id_penumpang);  
  if (status_postgre.success == false ) {
    res.status(400).json(status_postgre);
    return
  }
      res.status(200).json({        
        success: 'true',
        message: "Data has been updated",
        data: null
      });
}

async function updatePenumpang(params,id_penumpang) { //fungsi untuk merubah data user 
   let username = params.username;
   let password = params.password;
   let nama_penumpang = params.nama_penumpang;
   let alamat_penumpang = params.alamat_penumpang;
   let tanggal_lahir = params.tanggal_lahir;
   let jenis_kelamin = params.jenis_kelamin;
   let telefone = params.nama_petugas;
    try {
      return {
        success: true,
        data: await db.none(`update penumpang set
        username   = '${username}',
        password = '${password}',
        nama_penumpang = '${nama_penumpang}',
        alamat_penumpang = '${alamat_penumpang}',
        tanggal_lahir = '${tanggal_lahir}',
        jenis_kelamin = '${jenis_kelamin}',
        telefone = '${telefone}'
        where id_penumpang = '${id_penumpang}'`)
      };
    } catch (error) {
      return {        
        success: false,
        message: 'Error in update penumpang : '+ error.message,
        data: null
      }
    }
}

  
// -------------------------------------------------------------------------------------------------------------------------------------------------
// --------------------------------------------------------UPDATE END----------------------------------------------------------------------------  
// -------------------------------------------------------------------------------------------------------------------------------------------------
// --------------------------------------------------------DELETE----------------------------------------------------------------------------


async function remove(req, res) { //fungsi untuk merubah data user 
  let id_penumpang = req.params.id;
  status_postgre = await removePenumpang(id_penumpang);  
  if (status_postgre.success == false ) {
    res.status(400).json(status_postgre);
    return
  }
      res.status(200).json({        
        success: 'true',
        message: "Data has been removed",
        data: null
      });
}

async function removePenumpang(id_penumpang) { //fungsi untuk merubah data user 
    try {
      return {
        success: true,
        data: await db.any('Delete from penumpang where id_penumpang = $1', id_penumpang)
      };
    } catch (error) {
      return {        
        success: false,
        message: 'Error in update penumpang : '+ error.message,
        data: null
      }
    }
}
  
// -------------------------------------------------------------------------------------------------------------------------------------------------
// --------------------------------------------------------DELETE END----------------------------------------------------------------------------  

function validate(body) {
  const schema = {
    username: Joi.string(),
    password: Joi.string(),
    nama_penumpang: Joi.string(),
    alamat_penumpang: Joi.string(),
    tanggal_lahir: Joi.string(),
    jenis_kelamin: Joi.string(),
    telefone: Joi.string()
  };
  return Joi.validate(body, schema);
}


module.exports = {
  create : insert,
  read : read,
  readSingle : readSingle,
  update : update,
  delete : remove,
  loginPenumpang : loginPenumpang
};