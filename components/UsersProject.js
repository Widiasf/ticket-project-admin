var conn = require('../lib/database');
var Axios = require('axios');
var Joi = require('joi');
db = conn.db;


// -------------------------------------------------------------------------------------------------------------------------------------------------
// --------------------------------------------------------CREATE----------------------------------------------------------------------------

async function insert(req, res) { 
 
  params = req.body;

  status_postgre = await insertUserProject(params); //insert data user kedalam table user
  if (status_postgre.success == false ) {
    res.status(400).json(status_postgre);
  }
  status_postgre_persen_project = await insertPersenProject(params.project_id); //insert data user kedalam table user
  if (status_postgre_persen_project.success == false ) {
    res.status(400).json(status_postgre_persen_project);
  }
  let users_project_id = status_postgre.data.users_project_id;
  status_postgre_rusercategory = await insertUserRProjCat(params.category_id,users_project_id); //insert data user kedalam table user
  if (status_postgre_rusercategory.success == false ) {
    res.status(400).json(status_postgre_rusercategory);
  }
  status_postgre_log = await insertLog(params); //insert data user kedalam table user
  if (status_postgre_log.success == false ) {
    res.status(400).json(status_postgre_log);
  }
  status_postgre_notif = await insertNotification(params); //insert data user kedalam table user
  if (status_postgre_notif.success == false ) {
    res.status(400).json(status_postgre_notif);
  }
      res.status(200).json({ 
        success: true,
        message: "Data has been inserted into database",
        data: status_postgre.data.users_project_id
      }); 
}

async function insertPersenProject(project_id){
  semua_pekerjaan = await db.result('select * from users_project where project_id = $1 and is_deleted=false',project_id);
  pekerjaan_completed = await db.result('select * from users_project where project_id = $1 and is_deleted=false and status LIKE \'%$2#%\' ',[project_id,"approved"]);
  data_semua_pekerjaan = semua_pekerjaan.rowCount;
  data_pekerjaan_completed = pekerjaan_completed.rowCount;
  let persen = await (100/Number(data_semua_pekerjaan))*Number(data_pekerjaan_completed);
  try {
    return {
      success: true,
      data: await db.none(`update project set
      persentase   = '${persen}'
      where project_id = '${project_id}'`)
    };
  } catch (error) {
    return {        
      success: false,
      message: 'Error in update persentase project : '+ error.message,
      data: null
    }
  }
  
  }
async function insertUserProject(params) { //funsgi untuk insert data kedalam database postgre
  try {
    return {
      success: true,
      data: await  db.one('insert into users_project(project_id,users_id,name,version,description,status,parent)'+
     'values(${project_id},${users_id},${name},${version},${description},${status},${parent}) RETURNING users_project_id',
     params)
    };
  } catch (error) {
    return {      
      success: false,
      message: 'Error in insert project into Database: '+ error.detail,
      data: null
    }
  }
}

async function insertUserRProjCat(category_id,users_project_id) { //funsgi untuk insert data kedalam database postgre
  try {
    return {
      success: true,
      data: await  db.any('insert into r_users_project_category(category_id,users_project_id)'+
     'values($1,$2)',
     [category_id,users_project_id])
    };
  } catch (error) {
    return {      
      success: false,
      message: 'Error in insert project into Database: '+ error.detail,
      data: null
    }
  }
}

async function insertLog(params) { //funsgi untuk insert data kedalam database postgre
  let project_id = params.project_id;
  let target_users_id = params.users_id;
  let users_id = params.users_id_pemberi;
  let type = "Menambah pekerjaan"
  try {
    return {
      success: true,
      data: await  db.one('insert into log(project_id,users_id,type,target_users_id)'+
     'values($1,$2,$3,$4) RETURNING log_id',
     [project_id,users_id,type,target_users_id])
    };
  } catch (error) {
    return {      
      success: false,
      message: 'Error in insert project into Database: '+ error.detail,
      data: null
    }
  }
}
async function insertNotification(params) { //funsgi untuk insert data kedalam database postgre
  let users_id = params.users_id;
  let users_id_pemberi = params.users_id_pemberi;
  try {
    return {
      success: true,
      data: await  db.one('insert into notification(users_id,users_id_pemberi)'+
     'values($1,$2) RETURNING notification_id',
     [users_id,users_id_pemberi])
    };
  } catch (error) {
    return {      
      success: false,
      message: 'Error in insert notification into Database: '+ error.detail,
      data: null
    }
  }
}

// -------------------------------------------------------------------------------------------------------------------------------------------------
// --------------------------------------------------------CREATE END----------------------------------------------------------------------------


// -------------------------------------------------------------------------------------------------------------------------------------------------
// --------------------------------------------------------READ----------------------------------------------------------------------------

function read(req, res, next) {
    let sort_order = (req.query.sort_order) ? req.query.sort_order : '';
    let sort_by = (req.query.sort_by) ? ' order by ' + req.query.sort_by : '';
    let page_offset = (req.query.page_offset) ? ' offset ' + req.query.page_offset : '';
    let page_size = (req.query.page_size) ? ' limit ' + parseInt(req.query.page_size) : '';
    let project_id = req.params.id;
    query_select = ' select up.users_id,up.users_project_id,u.username,up.name,up.version,up.description,up.status,up.parent from users_project up join users u on up.users_id=u.users_id where up.is_deleted=false and up.project_id=$1' + sort_by + ' ' + sort_order + page_size + page_offset;
    query_all = 'select count(*) from users_project WHERE is_deleted=false';
    Promise.all([db.result(query_select,project_id), db.result(query_all)])
      .then(function (data) {
        res.status(200)
          .json({
            success: true,
            message: 'Retrieved project data',
            num_of_data: data[0].rowCount,
            data: data[0].rows,
            total_data : data[1].rows[0].count
            
          });
      })
      .catch(function (err) {
        res.status(400)
          .json({
            success: false,
            message: 'Error occured: '+err.message,
            data: null,
          });
      });
  }

function readSingle(req,res,next){ 
  db.one('select up.users_project_id,u.username,up.name,up.version,up.description,up.status,up.parent from users_project up join users u on up.users_id=u.users_id where up.is_deleted=false and up.users_project_id=$1', req.params.id)
  .then(function(data) {
    res.status(200)
    .json({
      success: true,
      message: 'Retrieved project category data',
      data: data
      });
  })
  .catch(function (err) {
    res.status(400)
    .json({
      success: false,
      message: 'Error occured : ' + err.message,
      data: null
    });
  });
}

//read pekerjaan saya
function readMyJob(req,res,next){ 
  db.any('select up.project_id,up.users_id,up.users_project_id,u.username,up.name as "nama pekerjaan",up.version,up.description,up.status,up.parent, proj.name as "project name" from users_project up, users u, project proj where (up.users_id=u.users_id) and (proj.project_id=up.project_id) AND up.is_deleted=false and u.users_id=$1', req.params.id)
  .then(function(data) {
    res.status(200)
    .json({
      success: true,
      message: 'Retrieved project category data',
      data: data
      });
  })
  .catch(function (err) {
    res.status(400)
    .json({
      success: false,
      message: 'Error occured : ' + err.message,
      data: null
    });
  });
}

//read pekerjaan review
function readPekerjaanReview(req,res,next){ 
  db.any('select u.name,up.project_id,up.users_id,up.users_project_id,u.username,up.name as "nama pekerjaan",up.version,up.description,up.status,up.parent, proj.name as "project name" from users_project up, users u, project proj where (up.users_id=u.users_id) and (proj.project_id=up.project_id) AND up.is_deleted=false and up.status LIKE \'%$1#%\'', "completed")
  .then(function(data) {
    res.status(200)
    .json({
      success: true,
      message: 'Retrieved project category data',
      data: data
      });
  })
  .catch(function (err) {
    res.status(400)
    .json({
      success: false,
      message: 'Error occured : ' + err.message,
      data: null
    });
  });
}
function readCountPekerjaan(req,res,next){
  
  query_pekerjaan_selesai = 'select count (*) as "Selesai" from users_project up where up.is_deleted=false and up.status LIKE \'%$1#%\' AND up.users_id=$2';
  query_pekerjaan_dikerjakan= 'select count (*) from users_project up where up.is_deleted=false and (up.status LIKE \'%$1#%\' OR up.status LIKE \'%$2#%\' OR up.status LIKE \'%$3#%\') AND up.users_id=$4';
  query_pekerjaan_pending = 'select count (*)  from users_project up where up.is_deleted=false and (up.status LIKE \'%$1#%\' OR up.status LIKE \'%$2#%\' OR up.status LIKE \'%$3#%\' OR up.status LIKE \'%$4#%\')  AND up.users_id=$5';
  Promise.all([db.result(query_pekerjaan_selesai,["approved",req.params.id]), db.result(query_pekerjaan_dikerjakan,["pengembangan","on progress","completed",req.params.id]),db.result(query_pekerjaan_pending,["revisi","on analysis process","bertanya","pending",req.params.id])])
    .then(function (data) {
      let selesaiData = data[0].rows[0];
      let dikerjakanData = data[1].rows[0];
      let pendingData = data[2].rows[0];
      selesaiData.Dikerjakan = dikerjakanData.count;
      selesaiData.Pending = pendingData.count;
      
     
      res.status(200)
        .json({
          success: true,
          message: 'Retrieved pattern data',
          data : 
            selesaiData
        });
    })
   .catch(function (err) {
     res.status(400)
         .json({
           success: false,
           message: 'Error occured ' + err.message,
           data: null
         });
   });
}
// -------------------------------------------------------------------------------------------------------------------------------------------------
// --------------------------------------------------------READ END----------------------------------------------------------------------------  

// -------------------------------------------------------------------------------------------------------------------------------------------------
// --------------------------------------------------------UPDATE----------------------------------------------------------------------------

async function update(req, res) { //fungsi untuk merubah data user 
  params = req.body;
  users_project_id = req.params.id;
  status_postgre = await updateUserProject(params,users_project_id);  
  if (status_postgre.success == false ) {
    res.status(400).json(status_postgre);
    return
  }
  status_postgre_persen = await insertPersenProject(params.project_id);  
  if (status_postgre_persen.success == false ) {
    res.status(400).json(status_postgre_persen);
    return
  }
  users_project__id = req.params.id;
  status_postgre_log = await updateLog(params,users_project__id); //insert data user kedalam table user
  if (status_postgre_log.success == false ) {
    res.status(400).json(status_postgre_log);
  }
      res.status(200).json({        
        success: 'true',
        message: "Data has been updated",
        data: null
      });
}

async function updateUserProject(params,users_project_id) { //fungsi untuk merubah data user 
   let project_id = params.project_id;
   let users_id = params.users_id;
   let name = params.name;
   let version = params.version;
   let status = params.status;
   let description = params.description;
   let parent = params.parent;
    try {
      return {
        success: true,
        data: await db.none(`update users_project set
        name   = '${name}',
        project_id   = '${project_id}',
        users_id   = '${users_id}',
        version   = '${version}',
        status   = '${status}',
        description = '${description}',
        parent = '${parent}',
        updated_at   = CURRENT_TIMESTAMP
        where users_project_id = '${users_project_id}'`)
      };
    } catch (error) {
      return {        
        success: false,
        message: 'Error in update user : '+ error.message,
        data: null
      }
    }
}
async function insertPersenProject(project_id){
  semua_pekerjaan = await db.result('select * from users_project where project_id = $1 and is_deleted=false',project_id);
  pekerjaan_completed = await db.result('select * from users_project where project_id = $1 and is_deleted=false and status LIKE \'%$2#%\' ',[project_id,"approved"]);
  data_semua_pekerjaan = semua_pekerjaan.rowCount;
  data_pekerjaan_completed = pekerjaan_completed.rowCount;
  let persen = await (100/Number(data_semua_pekerjaan))*Number(data_pekerjaan_completed);
  try {
    return {
      success: true,
      data: await db.none(`update project set
      persentase   = '${persen}'
      where project_id = '${project_id}'`)
    };
  } catch (error) {
    return {        
      success: false,
      message: 'Error in update persentase project : '+ error.message,
      data: null
    }
  }
}
async function updateLog(params,users_project__id) { //funsgi untuk insert data kedalam database postgre
  let project_id = params.project_id;
  let target_users_id = params.users_id;
  let type = "Mengubah pekerjaan";
  let users_id = params.users_id_pengubah;
  let users_project_id = users_project__id;

  try {
    return {
      success: true,
      data: await  db.one('insert into log(project_id,users_id,type,target_object_id,target_users_id)'+
     'values($1,$2,$3,$4,$5) RETURNING log_id',
     [project_id,users_id,type,users_project_id,target_users_id])
    };
  } catch (error) {
    return {      
      success: false,
      message: 'Error in insert project into Database: '+ error.detail,
      data: null
    }
  }
}
  
// -------------------------------------------------------------------------------------------------------------------------------------------------
// --------------------------------------------------------UPDATE END----------------------------------------------------------------------------  
// -------------------------------------------------------------------------------------------------------------------------------------------------
// --------------------------------------------------------DELETE----------------------------------------------------------------------------
async function remove(req, res) { //fungsi untuk merubah data user 
  let users_project_id = req.params.id;
  status_postgre = await removeUsersProject(users_project_id);  
  if (status_postgre.success == false ) {
    res.status(400).json(status_postgre);
    return
  }
  let params=req.body;
  status_postgre_log = await removeLog(users_project_id,params);  
  if (status_postgre.success == false ) {
    res.status(400).json(status_postgre);
    return
  }
      res.status(200).json({        
        success: 'true',
        message: "Data has been removed",
        data: null
      });
}

async function removeUsersProject(users_project_id) { //fungsi untuk merubah data user 
    try {
      return {
        success: true,
        data: await db.result('Update users_project set is_deleted=true,deleted_at=CURRENT_TIMESTAMP WHERE users_project_id = $1', users_project_id)
      };
    } catch (error) {
      return {        
        success: false,
        message: 'Error in deleted users_project : '+ error.message,
        data: null
      }
    }
}
async function removeLog(users_project_id,params) { //fungsi untuk merubah data user 
  users_id = params.users_id_penghapus;
  target_users_id = params.users_id;
  project_id = params.project_id;
  type = "Menghapus pekerjaan";
  target_object_id = users_project_id;
  try {
    return {
      success: true,
      data: await  db.one('insert into log(project_id,users_id,type,target_object_id,target_users_id)'+
     'values($1,$2,$3,$4,$5) RETURNING log_id',
     [project_id,users_id,type,target_object_id,target_users_id])
    };
  } catch (error) {
    return {      
      success: false,
      message: 'Error in insert project into Database: '+ error.detail,
      data: null
    }
  }
}
  
// -------------------------------------------------------------------------------------------------------------------------------------------------
// --------------------------------------------------------DELETE END----------------------------------------------------------------------------  



module.exports = {
  create : insert,
  read : read,
  readSingle : readSingle,
  update : update,
  delete : remove,
  readMyJob : readMyJob,
  readCountPekerjaan : readCountPekerjaan,
  readPekerjaanReview : readPekerjaanReview
};