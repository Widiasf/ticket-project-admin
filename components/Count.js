var conn = require('../lib/database');
var Axios = require('axios');
var Joi = require('joi');
db = conn.db;

function readCountPekerjaan(req,res,next){
  
    query_pekerjaan_selesai = 'select count (*) as "Selesai" from users_project up where up.is_deleted=false and up.status LIKE \'%$1#%\' AND up.users_id=$2';
    query_pekerjaan_dikerjakan= 'select count (*) from users_project up where up.is_deleted=false and (up.status LIKE \'%$1#%\' OR up.status LIKE \'%$2#%\' OR up.status LIKE \'%$3#%\') AND up.users_id=$4';
    query_pekerjaan_pending = 'select count (*)  from users_project up where up.is_deleted=false and (up.status LIKE \'%$1#%\' OR up.status LIKE \'%$2#%\' OR up.status LIKE \'%$3#%\' OR up.status LIKE \'%$4#%\')  AND up.users_id=$5';
    Promise.all([db.result(query_pekerjaan_selesai,["approved",req.params.id]), db.result(query_pekerjaan_dikerjakan,["pengembangan","on progress","completed",req.params.id]),db.result(query_pekerjaan_pending,["revisi","on analysis process","bertanya","pending",req.params.id])])
      .then(function (data) {
        let selesaiData = data[0].rows[0];
        let dikerjakanData = data[1].rows[0];
        let pendingData = data[2].rows[0];
        selesaiData.Dikerjakan = dikerjakanData.count;
        selesaiData.Pending = pendingData.count;
        
       
        res.status(200)
          .json({
            success: true,
            message: 'Retrieved pattern data',
            data : 
              selesaiData
          });
      })
     .catch(function (err) {
       res.status(400)
           .json({
             success: false,
             message: 'Error occured ' + err.message,
             data: null
           });
     });
  }

  module.exports = {
    create : insert,
    read : read,
    readSingle : readSingle,
    update : update,
    delete : remove,
    readMyJob : readMyJob,
    readCountPekerjaan : readCountPekerjaan,
    readPekerjaanReview : readPekerjaanReview
  };