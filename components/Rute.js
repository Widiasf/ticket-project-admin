var conn = require('../lib/database');
var Axios = require('axios');
var Joi = require('joi');
db = conn.db;


// -------------------------------------------------------------------------------------------------------------------------------------------------
// --------------------------------------------------------CREATE----------------------------------------------------------------------------

async function insert(req, res) { 
  const result = validate(req.body);
  params = req.body;

  if (result.error) return res.status(400).send(
    { success: false,
      message: result.error.details[0].message,
      data: null
    }); 

  status_postgre = await insertRute(params); //insert data user kedalam table user
  if (status_postgre.success == false ) {
    res.status(400).json(status_postgre);
  }

      res.status(200).json({ 
        success: true,
        message: "Data has been inserted into database",
        data: null
      }); 
}
async function insertRute(params) { //funsgi untuk insert data kedalam database postgre
  try {
    return {
      success: true,
      data: await  db.one('insert into rute(tujuan,rute_awal,rute_akhir)'+
     'values(${tujuan},${rute_awal},${rute_akhir}) RETURNING id_rute',
     params)
    };
  } catch (error) {
    return {      
      success: false,
      message: 'Error in insert rute into Database: '+ error.detail,
      data: null
    }
  }
}
// -------------------------------------------------------------------------------------------------------------------------------------------------
// --------------------------------------------------------CREATE END----------------------------------------------------------------------------


// -------------------------------------------------------------------------------------------------------------------------------------------------
// --------------------------------------------------------READ----------------------------------------------------------------------------

function read(req, res, next) {
    let sort_order = (req.query.sort_order) ? req.query.sort_order : '';
    let sort_by = (req.query.sort_by) ? ' order by ' + req.query.sort_by : '';
    let page_offset = (req.query.page_offset) ? ' offset ' + req.query.page_offset : '';
    let page_size = (req.query.page_size) ? ' limit ' + parseInt(req.query.page_size) : '';
    
    query_select = ' select id_rute,tujuan,rute_awal,rute_akhir from rute' + sort_by + ' ' + sort_order + page_size + page_offset;
    query_all = 'select count(*) from rute';
    Promise.all([db.result(query_select), db.result(query_all)])
      .then(function (data) {
        res.status(200)
          .json({
            success: true,
            message: 'Retrieved rute data',
            num_of_data: data[0].rowCount,
            data: data[0].rows,
            total_data : data[1].rows[0].count
            
          });
      })
      .catch(function (err) {
        res.status(400)
          .json({
            success: false,
            message: 'Error occured: '+err.message,
            data: null,
          });
      });
  }

function readSingle(req,res,next){ 
  db.one('select id_rute,tujuan,rute_awal,rute_akhir from rute where id_rute=$1', req.params.id)
  .then(function(data) {
    res.status(200)
    .json({
      success: true,
      message: 'Retrieved rute data',
      data: data
      });
  })
  .catch(function (err) {
    res.status(400)
    .json({
      success: false,
      message: 'Error occured : ' + err.message,
      data: null
    });
  });
}
// -------------------------------------------------------------------------------------------------------------------------------------------------
// --------------------------------------------------------READ END----------------------------------------------------------------------------  

// -------------------------------------------------------------------------------------------------------------------------------------------------
// --------------------------------------------------------UPDATE----------------------------------------------------------------------------

async function update(req, res) { //fungsi untuk merubah data user 
  params = req.body;
  id_rute= req.params.id;
  status_postgre = await updateRute(params,id_rute);  
  if (status_postgre.success == false ) {
    res.status(400).json(status_postgre);
    return
  }
      res.status(200).json({        
        success: 'true',
        message: "Data has been updated",
        data: null
      });
}

async function updateRute(params,id_rute) { //fungsi untuk merubah data user 
   let tujuan = params.tujuan;
   let rute_awal = params.rute_awal;
   let rute_akhir = params.rute_akhir;
    try {
      return {
        success: true,
        data: await db.none(`update rute set
        tujuan   = '${tujuan}',
        rute_awal   = '${rute_awal}',
        rute_akhir   = '${rute_akhir}'
        where id_rute = '${id_rute}'`)
      };
    } catch (error) {
      return {        
        success: false,
        message: 'Error in update transportasi : '+ error.message,
        data: null
      }
    }
}

  
// -------------------------------------------------------------------------------------------------------------------------------------------------
// --------------------------------------------------------UPDATE END----------------------------------------------------------------------------  
// -------------------------------------------------------------------------------------------------------------------------------------------------
// --------------------------------------------------------DELETE----------------------------------------------------------------------------


async function remove(req, res) { //fungsi untuk merubah data user 
  let id_rute = req.params.id;
  status_postgre = await removeRute(id_rute);  
  if (status_postgre.success == false ) {
    res.status(400).json(status_postgre);
    return
  }
      res.status(200).json({        
        success: 'true',
        message: "Data has been removed",
        data: null
      });
}

async function removeRute(id_rute) { //fungsi untuk merubah data user 
    try {
      return {
        success: true,
        data: await db.any('Delete from rute where id_rute = $1', id_rute)
      };
    } catch (error) {
      return {        
        success: false,
        message: 'Error in update rute : '+ error.message,
        data: null
      }
    }
}
  
// -------------------------------------------------------------------------------------------------------------------------------------------------
// --------------------------------------------------------DELETE END----------------------------------------------------------------------------  

function validate(body) {
  const schema = {
    tujuan: Joi.string(),
    rute_awal: Joi.string(),
    rute_akhir: Joi.string()
  };
  return Joi.validate(body, schema);
}


module.exports = {
  create : insert,
  read : read,
  readSingle : readSingle,
  update : update,
  delete : remove
};