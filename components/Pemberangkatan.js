var conn = require('../lib/database');
var Axios = require('axios');
var Joi = require('joi');
db = conn.db;


// -------------------------------------------------------------------------------------------------------------------------------------------------
// --------------------------------------------------------CREATE----------------------------------------------------------------------------

async function insert(req, res) { 
  const result = validate(req.body);
  params = req.body;

  if (result.error) return res.status(400).send(
    { success: false,
      message: result.error.details[0].message,
      data: null
    }); 

  status_postgre = await insertPemberangkatan(params); //insert data user kedalam table user
  if (status_postgre.success == false ) {
    res.status(400).json(status_postgre);
  }

      res.status(200).json({ 
        success: true,
        message: "Data has been inserted into database",
        data: null
      }); 
}
async function insertPemberangkatan(params) { //funsgi untuk insert data kedalam database postgre
  try {
    return {
      success: true,
      data: await  db.one('insert into pemberangkatan(id_transportasi,tanggal_berangkat,id_rute,jam_berangkat,'+
      'lama_pemberangkatan,jam_sampai,jam_checkin,harga,nama)'+
     'values(${id_transportasi},${tanggal_berangkat},${id_rute},${jam_berangkat},${lama_pemberangkatan},${jam_sampai},'+
     '${jam_checkin},${harga},${nama}) RETURNING id_pemberangkatan',
     params)
    };
  } catch (error) {
    return {      
      success: false,
      message: 'Error in insert rute into Database: '+ error.detail,
      data: null
    }
  }
}
// -------------------------------------------------------------------------------------------------------------------------------------------------
// --------------------------------------------------------CREATE END----------------------------------------------------------------------------


// -------------------------------------------------------------------------------------------------------------------------------------------------
// --------------------------------------------------------READ----------------------------------------------------------------------------

function read(req, res, next) {
    let sort_order = (req.query.sort_order) ? req.query.sort_order : '';
    let sort_by = (req.query.sort_by) ? ' order by ' + req.query.sort_by : '';
    let page_offset = (req.query.page_offset) ? ' offset ' + req.query.page_offset : '';
    let page_size = (req.query.page_size) ? ' limit ' + parseInt(req.query.page_size) : '';
    
    query_select = ' select * from pemberangkatan' + sort_by + ' ' + sort_order + page_size + page_offset;
    query_all = 'select count(*) from pemberangkatan';
    Promise.all([db.result(query_select), db.result(query_all)])
      .then(function (data) {
        res.status(200)
          .json({
            success: true,
            message: 'Retrieved pemberangkatan data',
            num_of_data: data[0].rowCount,
            data: data[0].rows,
            total_data : data[1].rows[0].count
            
          });
      })
      .catch(function (err) {
        res.status(400)
          .json({
            success: false,
            message: 'Error occured: '+err.message,
            data: null,
          });
      });
  }

function readSingle(req,res,next){ 
  db.one('select * from pemberangkatan where id_pemberangkatan=$1', req.params.id)
  .then(function(data) {
    res.status(200)
    .json({
      success: true,
      message: 'Retrieved rute data',
      data: data
      });
  })
  .catch(function (err) {
    res.status(400)
    .json({
      success: false,
      message: 'Error occured : ' + err.message,
      data: null
    });
  });
}
// -------------------------------------------------------------------------------------------------------------------------------------------------
// --------------------------------------------------------READ END----------------------------------------------------------------------------  

// -------------------------------------------------------------------------------------------------------------------------------------------------
// --------------------------------------------------------UPDATE----------------------------------------------------------------------------

async function update(req, res) { //fungsi untuk merubah data user 
  params = req.body;
  id_pemberangkatan= req.params.id;
  status_postgre = await updatePemberangkatan(params,id_pemberangkatan);  
  if (status_postgre.success == false ) {
    res.status(400).json(status_postgre);
    return
  }
      res.status(200).json({        
        success: 'true',
        message: "Data has been updated",
        data: null
      });
}

async function updatePemberangkatan(params,id_pemberangkatan) { //fungsi untuk merubah data user 
   let id_transportasi = params.id_transportasi;
   let tanggal_berangkat = params.tanggal_berangkat;
   let id_rute = params.id_rute;
   let jam_berangkat = params.jam_berangkat;
   let lama_pemberangkatan = params.lama_pemberangkatan;
   let jam_sampai = params.jam_sampai;
   let jam_checkin = params.jam_checkin;
   let harga = params.harga;
   let nama = params.nama;
    try {
      return {
        success: true,
        data: await db.none(`update pemberangkatan set
        id_transportasi   = '${id_transportasi}',
        tanggal_berangkat   = '${tanggal_berangkat}',
        id_rute   = '${id_rute}',
        jam_berangkat   = '${jam_berangkat}',
        lama_pemberangkatan   = '${lama_pemberangkatan}',
        jam_sampai   = '${jam_sampai}',
        jam_checkin   = '${jam_checkin}',
        nama   = '${nama}',
        harga   = '${harga}'
        where id_pemberangkatan = '${id_pemberangkatan}'`)
      };
    } catch (error) {
      return {        
        success: false,
        message: 'Error in update pemberangkatan : '+ error.message,
        data: null
      }
    }
}

  
// -------------------------------------------------------------------------------------------------------------------------------------------------
// --------------------------------------------------------UPDATE END----------------------------------------------------------------------------  
// -------------------------------------------------------------------------------------------------------------------------------------------------
// --------------------------------------------------------DELETE----------------------------------------------------------------------------


async function remove(req, res) { //fungsi untuk merubah data user 
  let id_pemberangkatan = req.params.id;
  status_postgre = await removePemberangkatan(id_pemberangkatan);  
  if (status_postgre.success == false ) {
    res.status(400).json(status_postgre);
    return
  }
      res.status(200).json({        
        success: 'true',
        message: "Data has been removed",
        data: null
      });
}

async function removePemberangkatan(id_pemberangkatan) { //fungsi untuk merubah data user 
    try {
      return {
        success: true,
        data: await db.any('Delete from pemberangkatan where id_pemberangkatan = $1', id_pemberangkatan)
      };
    } catch (error) {
      return {        
        success: false,
        message: 'Error in update rute : '+ error.message,
        data: null
      }
    }
}
  
// -------------------------------------------------------------------------------------------------------------------------------------------------
// --------------------------------------------------------DELETE END----------------------------------------------------------------------------  

function validate(body) {
  const schema = {
    id_transportasi: Joi.string(),
    tanggal_berangkat: Joi.string(),
    id_rute: Joi.string(),
    jam_berangkat: Joi.string(),
    lama_pemberangkatan: Joi.string(),
    jam_sampai: Joi.string(),
    jam_checkin: Joi.string(),
    harga: Joi.number(),
    nama:Joi.string()
  };
  return Joi.validate(body, schema);
}


module.exports = {
  create : insert,
  read : read,
  readSingle : readSingle,
  update : update,
  delete : remove
};