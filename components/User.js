var conn = require('../lib/database');
var Axios = require('axios');
var Joi = require('joi');
db = conn.db;

// -------------------------------------------------------------------------------------------------------------------------------------------------
// --------------------------------------------------------LOGIN----------------------------------------------------------------------------
function login(req, res, next) {

  let username = (req.body.username);
  let password = (req.body.password);

    query_select = `select u.users_id,u.username,ul.name from users u, users_level ul where u.users_level_id=ul.users_level_id and username = '${username}' and password= '${password}'`;
  db.one(query_select)
    .then(function (data) {
      res.status(200)
        .json({
          success: true,
          message: 'User data exists',
          data: data,
        });
    })
    .catch(function (err) {
      res.status(400)
         .json({
           success: false,
           message: 'Login error ' + err.message,
           data: null
         });
    });
}

// -------------------------------------------------------------------------------------------------------------------------------------------------
// --------------------------------------------------------CREATE----------------------------------------------------------------------------

async function insert(req, res) { 
  const result = validate(req.body);
  params = req.body;

  if (result.error) return res.status(400).send(
    { success: false,
      message: result.error.details[0].message,
      data: null
    }); 

  status_postgre = await insertUser(params); //insert data user kedalam table user
  if (status_postgre.success == false ) {
    res.status(400).json(status_postgre);
  }
      res.status(200).json({ 
        success: true,
        message: "Data has been inserted into database",
        data: status_postgre.data.users_id
      }); 
}
async function insertUser(params) { //funsgi untuk insert data kedalam database postgre
  try {
    return {
      success: true,
      data: await  db.one('insert into users(users_level_id,username,name,email,password,photo_profile,no_telp,address)'+
     'values( ${users_level_id},${username},${name},${email},${password},${photo_profile},${no_telp},${address} ) RETURNING users_id',
     params)
    };
  } catch (error) {
    return {      
      success: false,
      message: 'Error in insert user into Database: '+ error.detail,
      data: null
    }
  }
}


// -------------------------------------------------------------------------------------------------------------------------------------------------
// --------------------------------------------------------CREATE END----------------------------------------------------------------------------


// -------------------------------------------------------------------------------------------------------------------------------------------------
// --------------------------------------------------------READ----------------------------------------------------------------------------

function read(req, res, next) {
  let sort_order = (req.query.sort_order) ? req.query.sort_order : '';
  let sort_by = (req.query.sort_by) ? ' order by ' + req.query.sort_by : '';
  let page_offset = (req.query.page_offset) ? ' offset ' + req.query.page_offset : '';
  let page_size = (req.query.page_size) ? ' limit ' + parseInt(req.query.page_size) : '';

  query_select = ' select usr.users_level_id,users_id,username,usr.name,email,password,photo_profile,no_telp,address,usrl.name as users_level_name from users usr inner join users_level usrl ON usr.users_level_id=usrl.users_level_id WHERE is_deleted=false' + sort_by + ' ' + sort_order + page_size + page_offset;

  query_all = 'select count(*) from users WHERE is_deleted=false';
  Promise.all([db.result(query_select), db.result(query_all)])
    .then(function (data) {
      res.status(200)
        .json({
          success: true,
          message: 'Retrieved users data',
          num_of_data: data[0].rowCount,
          data: data[0].rows,
          total_data : data[1].rows[0].count
          
        });
    })
    .catch(function (err) {
      res.status(400)
        .json({
          success: false,
          message: 'Error occured: '+err.message,
          data: null,
        });
    });
}
function readSingle(req,res,next){ 
  db.one('select usr.users_level_id,users_id,username,usr.name,email,password,photo_profile,no_telp,address,usrl.name as users_level_name from users usr inner join users_level usrl'+
  ' ON usr.users_level_id=usrl.users_level_id where users_id= $1 and is_deleted=false', req.params.id)
  .then(function(data) {
    res.status(200)
    .json({
      success: true,
      message: 'Retrieved user data',
      data: data
      });
  })
  .catch(function (err) {
    res.status(400)
    .json({
      success: false,
      message: 'Error occured : ' + err.message,
      data: null
    });
  });
}

//read users berdasarkan level
function readUsersByLevel(req,res,next){ 
  db.result('select u.name,u.username,u.users_id,u.email,u.password,u.photo_profile,u.no_telp,u.address,ul.name as "user level name" from users_level ul, users u where (u.users_level_id=ul.users_level_id) AND u.is_deleted=false and (ul.users_level_id=$1)', req.params.id)
  .then(function(data) {
    res.status(200)
    .json({
      success: true,
      message: 'Retrieved user data',
      data: data.rows,
      num_of_data: data.rowCount
      });
  })
  .catch(function (err) {
    res.status(400)
    .json({
      success: false,
      message: 'Error occured : ' + err.message,
      data: null
    });
  });
}
// -------------------------------------------------------------------------------------------------------------------------------------------------
// --------------------------------------------------------READ END----------------------------------------------------------------------------  

// -------------------------------------------------------------------------------------------------------------------------------------------------
// --------------------------------------------------------UPDATE----------------------------------------------------------------------------

async function update(req, res) { //fungsi untuk merubah data user 
  params = req.body;
  users_id = req.params.id;

  
  status_postgre = await updateUser(params,users_id);  
  if (status_postgre.success == false ) {
    res.status(400).json(status_postgre);
    return
  }
      res.status(200).json({        
        success: 'true',
        message: "Data has been updated",
        data: null
      });
}

async function updateUser(params,users_id) { //fungsi untuk merubah data user 
  
  let username = params.username;
  let name = params.name;
  let email = params.email;
  let password = params.password;
  let photo_profile = params.photo_profile;
  let no_telp = params.no_telp; 
  let address = params.address;
  try {
      return {
        success: true,
        data: await db.none(`update users set
        username   = '${username}',
        name   = '${name}',
        email   = '${email}',
        password  = '${password}',
        photo_profile   = '${photo_profile}',
        no_telp   = '${no_telp}',
        address = '${address}',
        updated_at = CURRENT_TIMESTAMP where users_id = '${users_id}'`)
      };
    } catch (error) {
      return {        
        success: false,
        message: 'Error in update user : '+ error.message,
        data: null
      }
    }
}

  
// -------------------------------------------------------------------------------------------------------------------------------------------------
// --------------------------------------------------------UPDATE END----------------------------------------------------------------------------  
// -------------------------------------------------------------------------------------------------------------------------------------------------
// --------------------------------------------------------DELETE----------------------------------------------------------------------------

async function remove(req, res) { //fungsi untuk merubah data 

  db.result('UPDATE users set is_deleted=true,deleted_at=CURRENT_TIMESTAMP WHERE users_id = $1', req.params.id)
  .then(function (data) {
    res.status(200)
      .json({
        success: true,
        message: 'delete ' + data.rowCount+' user data',
        data: null
        
      });
  })
  .catch(function (err) {
    res.status(400)
      .json({
        success: false,
        message: 'Error occured : ' + err.message,
        data: null
      });
  });
}
// -------------------------------------------------------------------------------------------------------------------------------------------------
// --------------------------------------------------------DELETE END----------------------------------------------------------------------------  

function validate(body) {
  const schema = {
    users_level_id: Joi.string(),
    username: Joi.string(),
    name: Joi.string(),
    email: Joi.string(),
    password: Joi.string(),
    no_telp: Joi.number(),
    photo_profile: Joi.string(),
    address: Joi.string()
  };
  return Joi.validate(body, schema);
}


module.exports = {
  create : insert,
  read : read,
  readSingle : readSingle,
  update : update,
  login : login,
  delete : remove,
  readUsersByLevel : readUsersByLevel

};