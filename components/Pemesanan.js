var conn = require('../lib/database');
var Axios = require('axios');
var Joi = require('joi');
var randomize = require('randomatic');
db = conn.db;


// -------------------------------------------------------------------------------------------------------------------------------------------------
// --------------------------------------------------------CREATE----------------------------------------------------------------------------

async function insert(req, res) { 
  const result = validate(req.body);
  params = req.body;

  if (result.error) return res.status(400).send(
    { success: false,
      message: result.error.details[0].message,
      data: null
    }); 

  status_postgre = await insertPemesanan(params); //insert data user kedalam table user
  if (status_postgre.success == false ) {
    res.status(400).json(status_postgre);
  }
  let id_pemesanan = status_postgre.data.id_pemesanan;
  status_postgre_update = await updateSisaKursi(id_pemesanan); //insert data user kedalam table user
  if (status_postgre_update.success == false ) {
    res.status(400).json(status_postgre_update);
  }

      res.status(200).json({ 
        success: true,
        message: "Data has been inserted into database",
        id_pemesanan: id_pemesanan
      }); 
}
async function insertPemesanan(params) { //funsgi untuk insert data kedalam database postgre
  
  let data = {
    status: "Pending",
    kode_pemesanan : "ORD-" + randomize('0', 12),
    tanggal_pemesanan : params.tanggal_pemesanan,
    tempat_pemesanan : params.tempat_pemesanan,
    id_penumpang : params.id_penumpang,
    kode_kursi : params.kode_kursi,
    total_bayar : params.total_bayar,
    id_petugas : "cdb6d4a1-e089-44c1-a723-1480f1510386",
    id_pemberangkatan : params.id_pemberangkatan
  }
  try {
    return {
      success: true,
      data: await  db.one('insert into pemesanan(kode_pemesanan,tanggal_pemesanan,tempat_pemesanan,id_penumpang,'+
      'kode_kursi,total_bayar,id_petugas,status,id_pemberangkatan)'+
     'values(${kode_pemesanan},${tanggal_pemesanan},${tempat_pemesanan},'+
     '${id_penumpang},${kode_kursi},'+
     '${total_bayar},${id_petugas},${status},${id_pemberangkatan}) RETURNING id_pemesanan',
     data)
    };
  } catch (error) {
    return {      
      success: false,
      message: 'Error in insert pemesanan into Database: '+ error.detail,
      data: null
    }
  }
}

async function updateSisaKursi(id_pemesanan) { //fungsi untuk merubah data user 

   try {
     return {
       success: true,
       data: await db.none(`update transportasi set jumlah_kursi = jumlah_kursi - 1 from pemesanan,pemberangkatan where pemesanan.id_pemberangkatan = pemberangkatan.id_pemberangkatan and pemberangkatan.id_transportasi = transportasi.id_transportasi and pemesanan.id_pemesanan = $1`,id_pemesanan)
     };
   } catch (error) {
     return {        
       success: false,
       message: 'Error in update level : '+ error.message,
       data: null
     }
   }
}
// -------------------------------------------------------------------------------------------------------------------------------------------------
// --------------------------------------------------------CREATE END----------------------------------------------------------------------------


// -------------------------------------------------------------------------------------------------------------------------------------------------
// --------------------------------------------------------READ----------------------------------------------------------------------------

function read(req, res, next) {
    query_select = 'select id_pemesanan,kode_pemesanan,tanggal_pemesanan,tempat_pemesanan,pn.id_penumpang,'+
    'kode_kursi,total_bayar,id_petugas,status,pb.id_pemberangkatan,pn.nama_penumpang,rt.tujuan '+
    'from pemesanan,penumpang pn,pemberangkatan pb,rute rt where pemesanan.id_penumpang=pn.id_penumpang and '+
    'pemesanan.id_pemberangkatan = pb.id_pemberangkatan and pb.id_rute = rt.id_rute';
    query_all = 'select count(*) from pemesanan';
    Promise.all([db.result(query_select), db.result(query_all)])
      .then(function (data) {
        res.status(200)
          .json({
            success: true,
            message: 'Retrieved level data',
            num_of_data: data[0].rowCount,
            data: data[0].rows,
            total_data : data[1].rows[0].count
            
          });
      })
      .catch(function (err) {
        res.status(400)
          .json({
            success: false,
            message: 'Error occured: '+err.message,
            data: null,
          });
      });
  }

function readSingle(req,res,next){ 
  db.one('select id_pemesanan,kode_pemesanan,tanggal_pemesanan,tempat_pemesanan,pn.id_penumpang,'+
  'kode_kursi,total_bayar,id_petugas,status,pb.id_pemberangkatan,pn.nama_penumpang,rt.tujuan '+
  'from pemesanan,penumpang pn,pemberangkatan pb,rute rt where pemesanan.id_penumpang=pn.id_penumpang and '+
  'pemesanan.id_pemberangkatan = pb.id_pemberangkatan and pb.id_rute = rt.id_rute and pemesanan.id_pemesanan=$1', req.params.id)
  .then(function(data) {
    res.status(200)
    .json({
      success: true,
      message: 'Retrieved level data',
      data: data
      });
  })
  .catch(function (err) {
    res.status(400)
    .json({
      success: false,
      message: 'Error occured : ' + err.message,
      data: null
    });
  });
}

function readAllPemesanan(req,res,next){ 
  db.any('select * from pemesanan where id_penumpang=$1', req.params.id)
  .then(function(data) {
    res.status(200)
    .json({
      success: true,
      message: 'Retrieved level data',
      data: data
      });
  })
  .catch(function (err) {
    res.status(400)
    .json({
      success: false,
      message: 'Error occured : ' + err.message,
      data: null
    });
  });
}
// -------------------------------------------------------------------------------------------------------------------------------------------------
// --------------------------------------------------------READ END----------------------------------------------------------------------------  

// -------------------------------------------------------------------------------------------------------------------------------------------------
// --------------------------------------------------------UPDATE----------------------------------------------------------------------------

async function update(req, res) { //fungsi untuk merubah data user 
  id_pemesanan = req.params.id;
  status_postgre = await updatePemesananSudahBayar(id_pemesanan);  
  if (status_postgre.success == false ) {
    res.status(400).json(status_postgre);
    return
  }
      res.status(200).json({        
        success: 'true',
        message: "Data has been updated",
        data: null
      });
}

async function updatePemesananSudahBayar(id_pemesanan) { //fungsi untuk merubah data user 
  let sudahbayar = "Sudah Bayar";
    try {
      return {
        success: true,
        data: await db.none(`update pemesanan set
        status   = '${sudahbayar}'
        where id_pemesanan = '${id_pemesanan}'`)
      };
    } catch (error) {
      return {        
        success: false,
        message: 'Error in update pemesanan sudah bayar : '+ error.message,
        data: null
      }
    }
}
async function verifikasiAdmin(req, res) { //fungsi untuk merubah data user 
  id_pemesanan = req.params.id;
  status_postgre = await updatePemesananVerifikasi(id_pemesanan);  
  if (status_postgre.success == false ) {
    res.status(400).json(status_postgre);
    return
  }
      res.status(200).json({        
        success: 'true',
        message: "Data has been updated",
        data: null
      });
}

async function updatePemesananVerifikasi(id_pemesanan) { //fungsi untuk merubah data user 
  let valid = "Valid";
    try {
      return {
        success: true,
        data: await db.none(`update pemesanan set
        status   = '${valid}'
        where id_pemesanan = '${id_pemesanan}'`)
      };
    } catch (error) {
      return {        
        success: false,
        message: 'Error in update pemesanan sudah bayar : '+ error.message,
        data: null
      }
    }
}

  
// -------------------------------------------------------------------------------------------------------------------------------------------------
// --------------------------------------------------------UPDATE END----------------------------------------------------------------------------  
// -------------------------------------------------------------------------------------------------------------------------------------------------
// --------------------------------------------------------DELETE----------------------------------------------------------------------------


async function remove(req, res) { //fungsi untuk merubah data user 
  let id_pemesanan = req.params.id;
  status_postgre = await removePemesanan(id_pemesanan);  
  if (status_postgre.success == false ) {
    res.status(400).json(status_postgre);
    return
  }
      res.status(200).json({        
        success: 'true',
        message: "Data has been removed",
        data: null
      });
}

async function removePemesanan(id_pemesanan) { //fungsi untuk merubah data user 
    try {
      return {
        success: true,
        data: await db.any('Delete from pemesanan where id_pemesanan = $1', id_pemesanan)
      };
    } catch (error) {
      return {        
        success: false,
        message: 'Error in delete pemesanan : '+ error.message,
        data: null
      }
    }
}
  
// -------------------------------------------------------------------------------------------------------------------------------------------------
// --------------------------------------------------------DELETE END----------------------------------------------------------------------------  

function validate(body) {
  const schema = {
    tanggal_pemesanan: Joi.string(),
    tempat_pemesanan: Joi.string(),
    id_penumpang: Joi.string(),
    kode_kursi: Joi.string(),
    id_pemberangkatan: Joi.string(),
    total_bayar: Joi.number()

  };
  return Joi.validate(body, schema);
}


module.exports = {
  create : insert,
  read : read,
  readSingle : readSingle,
  update : update,
  delete : remove,
  readAllPemesanan : readAllPemesanan,
  verifikasiAdmin :verifikasiAdmin
};