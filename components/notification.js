var conn = require('../lib/database');
var Axios = require('axios');
var Joi = require('joi');
db = conn.db;

// -------------------------------------------------------------------------------------------------------------------------------------------------
// --------------------------------------------------------READ----------------------------------------------------------------------------

function read(req, res, next) {
  query_select = ' select n.users_id,n.users_id_pemberi,n.is_read,n.notification_id,n.created_at,u.username from users u, notification n where u.users_id = n.users_id_pemberi and n.users_id=$1 order by created_at DESC';
  query_read_false = 'select * from notification where is_read=false and users_id=$1';
  Promise.all([db.result(query_select,req.params.id),db.result(query_read_false,req.params.id)])
    .then(function (data) {
      notif_data = data[0].rows;
      res.status(200)
        .json({
          success: true,
          message: 'Retrieved notif data',
          data: notif_data,
          num_of_data : data[0].rowCount,
          read_false : data[1].rowCount
        });
    })
    .catch(function (err) {
      res.status(400)
        .json({
          success: false,
          message: 'Error occured: '+err.message,
          data: null,
        });
    });
}



// -------------------------------------------------------------------------------------------------------------------------------------------------
// --------------------------------------------------------READ END----------------------------------------------------------------------------  

// -------------------------------------------------------------------------------------------------------------------------------------------------
// --------------------------------------------------------UPDATE----------------------------------------------------------------------------

async function update(req, res) { //fungsi untuk merubah data user 
 users_id = req.params.id;

  status_postgre = await updateNotif(users_id);  
  if (status_postgre.success == false ) {
    res.status(400).json(status_postgre);
    return
  }
      res.status(200).json({        
        success: 'true',
        message: "Data has been updated",
        data: null
      });
}

async function updateNotif(users_id) { //fungsi untuk merubah data user 
  let is_read = true;
  try {
      return {
        success: true,
        data: await db.none(`update notification set
        is_read   = '${is_read}'
        where users_id = '${users_id}' and is_read = false`)
      };
    } catch (error) {
      return {        
        success: false,
        message: 'Error in update notif : '+ error.message,
        data: null
      }
    }
}

  
// -------------------------------------------------------------------------------------------------------------------------------------------------
// --------------------------------------------------------UPDATE END----------------------------------------------------------------------------  


module.exports = {
 
  read : read,
  
  update : update,
  

};