var conn = require('../lib/database');
var Axios = require('axios');
var Joi = require('joi');
db = conn.db;


// -------------------------------------------------------------------------------------------------------------------------------------------------
// --------------------------------------------------------CREATE----------------------------------------------------------------------------

async function insert(req, res) { 
  const result = validate(req.body);
  params = req.body;

  if (result.error) return res.status(400).send(
    { success: false,
      message: result.error.details[0].message,
      data: null
    }); 

  status_postgre = await insertTransportasi(params); //insert data user kedalam table user
  if (status_postgre.success == false ) {
    res.status(400).json(status_postgre);
  }

      res.status(200).json({ 
        success: true,
        message: "Data has been inserted into database",
        data: null
      }); 
}
async function insertTransportasi(params) { //funsgi untuk insert data kedalam database postgre
  try {
    return {
      success: true,
      data: await  db.one('insert into transportasi(kode,jumlah_kursi,keterangan,id_tipe_transportasi,nama_transportasi)'+
     'values(${kode},${jumlah_kursi},${keterangan},${id_tipe_transportasi},${nama_transportasi}) RETURNING id_transportasi',
     params)
    };
  } catch (error) {
    return {      
      success: false,
      message: 'Error in insert transportasi into Database: '+ error.detail,
      data: null
    }
  }
}
// -------------------------------------------------------------------------------------------------------------------------------------------------
// --------------------------------------------------------CREATE END----------------------------------------------------------------------------


// -------------------------------------------------------------------------------------------------------------------------------------------------
// --------------------------------------------------------READ----------------------------------------------------------------------------

function read(req, res, next) {
    let sort_order = (req.query.sort_order) ? req.query.sort_order : '';
    let sort_by = (req.query.sort_by) ? ' order by ' + req.query.sort_by : '';
    let page_offset = (req.query.page_offset) ? ' offset ' + req.query.page_offset : '';
    let page_size = (req.query.page_size) ? ' limit ' + parseInt(req.query.page_size) : '';
    
    query_select = ' select id_transportasi,kode,jumlah_kursi,transportasi.keterangan,nama_tipe,nama_transportasi from transportasi inner join tipe_transportasi on transportasi.id_tipe_transportasi = tipe_transportasi.id_tipe_transportasi' + sort_by + ' ' + sort_order + page_size + page_offset;
    query_all = 'select count(*) from transportasi';
    Promise.all([db.result(query_select), db.result(query_all)])
      .then(function (data) {
        res.status(200)
          .json({
            success: true,
            message: 'Retrieved tipe transportasi data',
            num_of_data: data[0].rowCount,
            data: data[0].rows,
            total_data : data[1].rows[0].count
            
          });
      })
      .catch(function (err) {
        res.status(400)
          .json({
            success: false,
            message: 'Error occured: '+err.message,
            data: null,
          });
      });
  }

  function readAllTransportation(req, res, next) {
    
    query_select = ' select trs.id_transportasi,rute.tujuan,rute.rute_awal,rute.rute_akhir,pbr.harga,trs.kode,trs.jumlah_kursi,'+
    'trs.keterangan,tipe.nama_tipe,trs.nama_transportasi,pbr.tanggal_berangkat as tanggal_pemberangkatan, pbr.jam_berangkat,pbr.lama_pemberangkatan,pbr.jam_sampai,pbr.id_pemberangkatan,pbr.id_rute '+
    'from transportasi trs,rute rute,pemberangkatan pbr, tipe_transportasi tipe '+
    'where trs.id_transportasi=pbr.id_transportasi and trs.id_tipe_transportasi=tipe.id_tipe_transportasi and pbr.id_rute = rute.id_rute';
    query_all = 'select count(*) from transportasi';
    Promise.all([db.result(query_select), db.result(query_all)])
      .then(function (data) {
        res.status(200)
          .json({
            success: true,
            message: 'Retrieved tipe transportasi data',
            num_of_data: data[0].rowCount,
            data: data[0].rows,
            total_data : data[1].rows[0].count
            
          });
      })
      .catch(function (err) {
        res.status(400)
          .json({
            success: false,
            message: 'Error occured: '+err.message,
            data: null,
          });
      });
  }

function readSingle(req,res,next){ 
  db.one('select * from transportasi where id_transportasi=$1', req.params.id)
  .then(function(data) {
    res.status(200)
    .json({
      success: true,
      message: 'Retrieved transportasi data',
      data: data
      });
  })
  .catch(function (err) {
    res.status(400)
    .json({
      success: false,
      message: 'Error occured : ' + err.message,
      data: null
    });
  });
}
// -------------------------------------------------------------------------------------------------------------------------------------------------
// --------------------------------------------------------READ END----------------------------------------------------------------------------  

// -------------------------------------------------------------------------------------------------------------------------------------------------
// --------------------------------------------------------UPDATE----------------------------------------------------------------------------

async function update(req, res) { //fungsi untuk merubah data user 
  params = req.body;
  id_transportasi= req.params.id;
  status_postgre = await updateTipeTransportasi(params,id_transportasi);  
  if (status_postgre.success == false ) {
    res.status(400).json(status_postgre);
    return
  }
      res.status(200).json({        
        success: 'true',
        message: "Data has been updated",
        data: null
      });
}

async function updateTipeTransportasi(params,id_transportasi) { //fungsi untuk merubah data user 
   let kode = params.kode;
   let jumlah_kursi = params.jumlah_kursi;
   let id_tipe_transportasi = params.id_tipe_transportasi;
   let keterangan = params.keterangan;
   let nama_transportasi = params.nama_transportasi;
    try {
      return {
        success: true,
        data: await db.none(`update transportasi set
        kode   = '${kode}',
        jumlah_kursi   = '${jumlah_kursi}',
        id_tipe_transportasi   = '${id_tipe_transportasi}',
        nama_transportasi   = '${nama_transportasi}',
        keterangan = '${keterangan}'
        where id_transportasi = '${id_transportasi}'`)
      };
    } catch (error) {
      return {        
        success: false,
        message: 'Error in update transportasi : '+ error.message,
        data: null
      }
    }
}

  
// -------------------------------------------------------------------------------------------------------------------------------------------------
// --------------------------------------------------------UPDATE END----------------------------------------------------------------------------  
// -------------------------------------------------------------------------------------------------------------------------------------------------
// --------------------------------------------------------DELETE----------------------------------------------------------------------------


async function remove(req, res) { //fungsi untuk merubah data user 
  let id_transportasi = req.params.id;
  status_postgre = await removeTransportasi(id_transportasi);  
  if (status_postgre.success == false ) {
    res.status(400).json(status_postgre);
    return
  }
      res.status(200).json({        
        success: 'true',
        message: "Data has been removed",
        data: null
      });
}

async function removeTransportasi(id_transportasi) { //fungsi untuk merubah data user 
    try {
      return {
        success: true,
        data: await db.any('Delete from transportasi where id_transportasi = $1', id_transportasi)
      };
    } catch (error) {
      return {        
        success: false,
        message: 'Error in update tipe transportasi : '+ error.message,
        data: null
      }
    }
}
  
// -------------------------------------------------------------------------------------------------------------------------------------------------
// --------------------------------------------------------DELETE END----------------------------------------------------------------------------  

function validate(body) {
  const schema = {
    kode: Joi.string(),
    jumlah_kursi: Joi.string(),
    keterangan: Joi.string(),
    id_tipe_transportasi: Joi.string(),
    nama_transportasi : Joi.string()
  };
  return Joi.validate(body, schema);
}


module.exports = {
  create : insert,
  read : read,
  readSingle : readSingle,
  update : update,
  delete : remove,
  readAllTransportation : readAllTransportation
};