var conn = require('../lib/database');
var Axios = require('axios');
var Joi = require('joi');
db = conn.db;


// -------------------------------------------------------------------------------------------------------------------------------------------------
// --------------------------------------------------------CREATE----------------------------------------------------------------------------

async function insert(req, res) { 
  const result = validate(req.body);
  params = req.body;

  if (result.error) return res.status(400).send(
    { success: false,
      message: result.error.details[0].message,
      data: null
    }); 

  status_postgre = await insertPetugas(params); //insert data user kedalam table user
  if (status_postgre.success == false ) {
    res.status(400).json(status_postgre);
  }

      res.status(200).json({ 
        success: true,
        message: "Data has been inserted into database",
        data: null
      }); 
}
async function insertPetugas(params) { //funsgi untuk insert data kedalam database postgre
  try {
    return {
      success: true,
      data: await  db.one('insert into petugas(username,password,nama_petugas,id_level)'+
     'values(${username},${password},${nama_petugas},${id_level}) RETURNING id_petugas',
     params)
    };
  } catch (error) {
    return {      
      success: false,
      message: 'Error in insert petugas into Database: '+ error.detail,
      data: null
    }
  }
}
// -------------------------------------------------------------------------------------------------------------------------------------------------
// --------------------------------------------------------CREATE END----------------------------------------------------------------------------


// -------------------------------------------------------------------------------------------------------------------------------------------------
// --------------------------------------------------------READ----------------------------------------------------------------------------

function read(req, res, next) {
    let sort_order = (req.query.sort_order) ? req.query.sort_order : '';
    let sort_by = (req.query.sort_by) ? ' order by ' + req.query.sort_by : '';
    let page_offset = (req.query.page_offset) ? ' offset ' + req.query.page_offset : '';
    let page_size = (req.query.page_size) ? ' limit ' + parseInt(req.query.page_size) : '';
    
    query_select = ' select id_petugas, username, password, nama_petugas, petugas.id_level, nama_level from petugas inner join level on petugas.id_level=level.id_level ' + sort_by + ' ' + sort_order + page_size + page_offset;
    query_all = 'select count(*) from level';
    Promise.all([db.result(query_select), db.result(query_all)])
      .then(function (data) {
        res.status(200)
          .json({
            success: true,
            message: 'Retrieved petugas data',
            num_of_data: data[0].rowCount,
            data: data[0].rows,
            total_data : data[1].rows[0].count
            
          });
      })
      .catch(function (err) {
        res.status(400)
          .json({
            success: false,
            message: 'Error occured: '+err.message,
            data: null,
          });
      });
  }

function readSingle(req,res,next){ 
  db.one('select id_petugas, username, password, nama_petugas, petugas.id_level, nama_level from petugas inner join level on petugas.id_level=level.id_level where id_petugas=$1', req.params.id)
  .then(function(data) {
    res.status(200)
    .json({
      success: true,
      message: 'Retrieved petugas data',
      data: data
      });
  })
  .catch(function (err) {
    res.status(400)
    .json({
      success: false,
      message: 'Error occured : ' + err.message,
      data: null
    });
  });
}
function loginPetugas(req,res,next){ 
  db.one('select id_petugas,username, nama_petugas, password, nama_level from petugas,level where petugas.id_level=level.id_level and username=$1 and password=$2', [req.body.username,req.body.password])
  .then(function(data) {
    res.status(200)
    .json({
      success: true,
      message: 'Retrieved petugas data',
      data: data
      });
  })
  .catch(function (err) {
    res.status(400)
    .json({
      success: false,
      message: 'Error occured : ' + err.message,
      data: null
    });
  });
}
// -------------------------------------------------------------------------------------------------------------------------------------------------
// --------------------------------------------------------READ END----------------------------------------------------------------------------  

// -------------------------------------------------------------------------------------------------------------------------------------------------
// --------------------------------------------------------UPDATE----------------------------------------------------------------------------

async function update(req, res) { //fungsi untuk merubah data user 
  params = req.body;
  id_petugas = req.params.id;
  status_postgre = await updatePetugas(params,id_petugas);  
  if (status_postgre.success == false ) {
    res.status(400).json(status_postgre);
    return
  }
      res.status(200).json({        
        success: 'true',
        message: "Data has been updated",
        data: null
      });
}

async function updatePetugas(params,id_petugas) { //fungsi untuk merubah data user 
   let username = params.username;
   let password = params.password;
   let nama_petugas = params.nama_petugas;
   let id_level = params.id_level;
    try {
      return {
        success: true,
        data: await db.none(`update petugas set
        username   = '${username}',
        password = '${password}',
        nama_petugas = '${nama_petugas}',
        id_level = '${id_level}'
        where id_petugas = '${id_petugas}'`)
      };
    } catch (error) {
      return {        
        success: false,
        message: 'Error in update petugas : '+ error.message,
        data: null
      }
    }
}

  
// -------------------------------------------------------------------------------------------------------------------------------------------------
// --------------------------------------------------------UPDATE END----------------------------------------------------------------------------  
// -------------------------------------------------------------------------------------------------------------------------------------------------
// --------------------------------------------------------DELETE----------------------------------------------------------------------------


async function remove(req, res) { //fungsi untuk merubah data user 
  let id_petugas = req.params.id;
  status_postgre = await removePetugas(id_petugas);  
  if (status_postgre.success == false ) {
    res.status(400).json(status_postgre);
    return
  }
      res.status(200).json({        
        success: 'true',
        message: "Data has been removed",
        data: null
      });
}

async function removePetugas(id_petugas) { //fungsi untuk merubah data user 
    try {
      return {
        success: true,
        data: await db.any('Delete from petugas where id_petugas = $1', id_petugas)
      };
    } catch (error) {
      return {        
        success: false,
        message: 'Error in update user : '+ error.message,
        data: null
      }
    }
}
  
// -------------------------------------------------------------------------------------------------------------------------------------------------
// --------------------------------------------------------DELETE END----------------------------------------------------------------------------  

function validate(body) {
  const schema = {
    username: Joi.string(),
    password: Joi.string(),
    nama_petugas: Joi.string(),
    id_level: Joi.string(),
  };
  return Joi.validate(body, schema);
}


module.exports = {
  create : insert,
  read : read,
  readSingle : readSingle,
  update : update,
  delete : remove,
  loginPetugas : loginPetugas
};