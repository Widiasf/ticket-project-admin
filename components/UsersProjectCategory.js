var conn = require('../lib/database');
var Axios = require('axios');
var Joi = require('joi');
db = conn.db;


// -------------------------------------------------------------------------------------------------------------------------------------------------
// --------------------------------------------------------CREATE----------------------------------------------------------------------------

async function insert(req, res) { 
  const result = validate(req.body);
  params = req.body;

  if (result.error) return res.status(400).send(
    { success: false,
      message: result.error.details[0].message,
      data: null
    }); 

  status_postgre = await insertProjectCat(params.name); //insert data user kedalam table user
  if (status_postgre.success == false ) {
    res.status(400).json(status_postgre);
  }
      res.status(200).json({ 
        success: true,
        message: "Data has been inserted into database",
        data: null
      }); 
}
async function insertProjectCat(name) { //funsgi untuk insert data kedalam database postgre
  try {
    return {
      success: true,
      data: await  db.one('insert into users_project_category(name)'+
     'values($1) ON CONFLICT (name) DO NOTHING RETURNING category_id',
     [name])
    };
  } catch (error) {
    return {      
      success: false,
      message: 'Error in insert users project category into Database: '+ error.message+ ' duplicate name',
      data: null
    }
  }
}


// -------------------------------------------------------------------------------------------------------------------------------------------------
// --------------------------------------------------------CREATE END----------------------------------------------------------------------------


// -------------------------------------------------------------------------------------------------------------------------------------------------
// --------------------------------------------------------READ----------------------------------------------------------------------------

function read(req, res, next) {
    let sort_order = (req.query.sort_order) ? req.query.sort_order : '';
    let sort_by = (req.query.sort_by) ? ' order by ' + req.query.sort_by : '';
    let page_offset = (req.query.page_offset) ? ' offset ' + req.query.page_offset : '';
    let page_size = (req.query.page_size) ? ' limit ' + parseInt(req.query.page_size) : '';
  
    query_select = ' select category_id,name from users_project_category where is_deleted=false' + sort_by + ' ' + sort_order + page_size + page_offset;
    query_all = 'select count(*) from users_project_category WHERE is_deleted=false';
    Promise.all([db.result(query_select), db.result(query_all)])
      .then(function (data) {
        res.status(200)
          .json({
            success: true,
            message: 'Retrieved project category data',
            num_of_data: data[0].rowCount,
            data: data[0].rows,
            total_data : data[1].rows[0].count
            
          });
      })
      .catch(function (err) {
        res.status(400)
          .json({
            success: false,
            message: 'Error occured: '+err.message,
            data: null,
          });
      });
  }

function readSingle(req,res,next){ 
  db.one('select category_id,name from users_project_category where category_id= $1 and is_deleted=false', req.params.id)
  .then(function(data) {
    res.status(200)
    .json({
      success: true,
      message: 'Retrieved project category data',
      data: data
      });
  })
  .catch(function (err) {
    res.status(400)
    .json({
      success: false,
      message: 'Error occured : ' + err.message,
      data: null
    });
  });
}
// -------------------------------------------------------------------------------------------------------------------------------------------------
// --------------------------------------------------------READ END----------------------------------------------------------------------------  

// -------------------------------------------------------------------------------------------------------------------------------------------------
// --------------------------------------------------------UPDATE----------------------------------------------------------------------------

async function update(req, res) { //fungsi untuk merubah data user 
  params = req.body;
  category_id = req.params.id;

  
  status_postgre = await updateUserProject(params.name,category_id);  
  if (status_postgre.success == false ) {
    res.status(400).json(status_postgre);
    return
  }
      res.status(200).json({        
        success: 'true',
        message: "Data has been updated",
        data: null
      });
}

async function updateUserProject(name,category_id) { //fungsi untuk merubah data user 
    try {
      return {
        success: true,
        data: await db.none(`update users_project_category set
        name   = '${name}',
        updated_at=CURRENT_TIMESTAMP
        where category_id = '${category_id}'`)
      };
    } catch (error) {
      return {        
        success: false,
        message: 'Error in update user : '+ error.message,
        data: null
      }
    }
}

  
// -------------------------------------------------------------------------------------------------------------------------------------------------
// --------------------------------------------------------UPDATE END----------------------------------------------------------------------------  
// -------------------------------------------------------------------------------------------------------------------------------------------------
// --------------------------------------------------------DELETE----------------------------------------------------------------------------

async function remove(req, res) { //fungsi untuk merubah data 

  db.result('Update users_project_category set is_deleted=true,deleted_at=CURRENT_TIMESTAMP WHERE category_id = $1', req.params.id)
  .then(function (data) {
    res.status(200)
      .json({
        success: true,
        message: 'delete ' + data.rowCount+' user project category data',
        data: null
        
      });
  })
  .catch(function (err) {
    res.status(400)
      .json({
        success: false,
        message: 'Error occured : ' + err.message,
        data: null
      });
  });
}

  
// -------------------------------------------------------------------------------------------------------------------------------------------------
// --------------------------------------------------------DELETE END----------------------------------------------------------------------------  

function validate(body) {
  const schema = {
    name: Joi.string()
  };
  return Joi.validate(body, schema);
}


module.exports = {
  create : insert,
  read : read,
  readSingle : readSingle,
  update : update,
  delete : remove
};