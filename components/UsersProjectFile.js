var conn = require('../lib/database');
var Axios = require('axios');
var Joi = require('joi');
db = conn.db;


// -------------------------------------------------------------------------------------------------------------------------------------------------
// --------------------------------------------------------CREATE----------------------------------------------------------------------------

async function insert(req, res) { 
  const result = validate(req.body);
  params = req.body;

  if (result.error) return res.status(400).send(
    { success: false,
      message: result.error.details[0].message,
      data: null
    }); 

  status_postgre = await insertProjectFile(params); //insert data user kedalam table user
  if (status_postgre.success == false ) {
    res.status(400).json(status_postgre);
  }
  let file_id=status_postgre.data.file_id;
  status_postgre_relasi = await insertRelasi(file_id,params.project_id); //insert data user kedalam table user
  if (status_postgre_relasi.success == false ) {
    res.status(400).json(status_postgre_relasi);
  }
  status_postgre_log = await insertLog(params); //insert data user kedalam table user
  if (status_postgre_log.success == false ) {
    res.status(400).json(status_postgre_log);
  }
      res.status(200).json({ 
        success: true,
        message: "Data has been inserted into database",
        data: null
      }); 
}
async function insertProjectFile(params) { //funsgi untuk insert data kedalam database postgre
  try {
    return {
      success: true,
      data: await  db.one('insert into users_project_file(users_id,file,name)'+
     'values(${users_id},${file},${name}) RETURNING file_id',
     params)
    };
  } catch (error) {
    return {      
      success: false,
      message: 'Error in insert project into Database: '+ error.detail,
      data: null
    }
  }
}
async function insertRelasi(file_id,project_id) { //funsgi untuk insert data kedalam database postgre
  try {
    return {
      success: true,
      data: await  db.any('insert into r_file_users_project(project_id,file_id)'+
     'values($1,$2)',
     [project_id,file_id])
    };
  } catch (error) {
    return {      
      success: false,
      message: 'Error in insert project into Database: '+ error.detail,
      data: null
    }
  }
}
async function insertLog(params) { //funsgi untuk insert data kedalam database postgre
  let project_id = params.project_id;
  let users_id = params.users_id;
  let type = "Menambah file"
  try {
    return {
      success: true,
      data: await  db.one('insert into log(project_id,users_id,type)'+
     'values($1,$2,$3) RETURNING log_id',
     [project_id,users_id,type])
    };
  } catch (error) {
    return {      
      success: false,
      message: 'Error in insert project into Database: '+ error.detail,
      data: null
    }
  }
}


// -------------------------------------------------------------------------------------------------------------------------------------------------
// --------------------------------------------------------CREATE END----------------------------------------------------------------------------


// -------------------------------------------------------------------------------------------------------------------------------------------------
// --------------------------------------------------------READ----------------------------------------------------------------------------

function read(req, res, next) {
    let sort_order = (req.query.sort_order) ? req.query.sort_order : '';
    let sort_by = (req.query.sort_by) ? ' order by ' + req.query.sort_by : '';
    let page_offset = (req.query.page_offset) ? ' offset ' + req.query.page_offset : '';
    let page_size = (req.query.page_size) ? ' limit ' + parseInt(req.query.page_size) : '';
    let users_project_id = req.params.id;
    query_select = ' select file_id,users_project_file.name,file,username from users_project_file inner join users on users_project_file.users_id=users.users_id where users_project_file.is_deleted=false ' + sort_by + ' ' + sort_order + page_size + page_offset;
    query_all = 'select count(*) from users_project_file WHERE is_deleted=false';
    Promise.all([db.result(query_select,users_project_id), db.result(query_all)])
      .then(function (data) {
        res.status(200)
          .json({
            success: true,
            message: 'Retrieved project data',
            num_of_data: data[0].rowCount,
            data: data[0].rows,
            total_data : data[1].rows[0].count
            
          });
      })
      .catch(function (err) {
        res.status(400)
          .json({
            success: false,
            message: 'Error occured: '+err.message,
            data: null,
          });
      });
  }

function readSingle(req,res,next){ 
  db.one('select file_id,users_project_file.name,file,username from users_project_file inner join users on users_project_file.users_id=users.users_id where users_project_file.is_deleted=false and file_id=$1', req.params.id)
  .then(function(data) {
    res.status(200)
    .json({
      success: true,
      message: 'Retrieved project category data',
      data: data
      });
  })
  .catch(function (err) {
    res.status(400)
    .json({
      success: false,
      message: 'Error occured : ' + err.message,
      data: null
    });
  });
}

//read file by user_id
function readFileByUser(req,res,next){ 
  db.any('select upf.file_id,up.name as "nama pekerjaan",upf.name as "nama file",upf.file from users_project_file upf, users u, users_project up where (upf.users_id=u.users_id) and u.users_id=$1', req.params.id)
  .then(function(data) {
    res.status(200)
    .json({
      success: true,
      message: 'Retrieved project category data',
      data: data
      });
  })
  .catch(function (err) {
    res.status(400)
    .json({
      success: false,
      message: 'Error occured : ' + err.message,
      data: null
    });
  });
}

//read file by user_id
function readFileByProject(req,res,next){ 
  db.any('select upf.* from users_project_file upf, r_file_users_project rupf where (upf.file_id=rupf.file_id) AND upf.is_deleted=false and rupf.project_id=$1', req.params.id)
  .then(function(data) {
    res.status(200)
    .json({
      success: true,
      message: 'Retrieved file project data',
      data: data
      });
  })
  .catch(function (err) {
    res.status(400)
    .json({
      success: false,
      message: 'Error occured : ' + err.message,
      data: null
    });
  });
}
// -------------------------------------------------------------------------------------------------------------------------------------------------
// --------------------------------------------------------READ END----------------------------------------------------------------------------  

// -------------------------------------------------------------------------------------------------------------------------------------------------
// --------------------------------------------------------UPDATE----------------------------------------------------------------------------

async function update(req, res) { //fungsi untuk merubah data user 
  params = req.body;
  file_id = req.params.id;
  status_postgre = await updateProjectFile(params,file_id);  
  if (status_postgre.success == false ) {
    res.status(400).json(status_postgre);
    return
  }
      res.status(200).json({        
        success: 'true',
        message: "Data has been updated",
        data: null
      });
}

async function updateProjectFile(params,file_id) { //fungsi untuk merubah data user 
   let name = params.name;
   let users_project_id = params.users_project_id;
   let users_id = params.users_id;
   let file = params.file;
    try {
      return {
        success: true,
        data: await db.none(`update users_project_file set
        name   = '${name}',
        users_project_id   = '${users_project_id}',
        users_id   = '${users_id}',
        file   = '${file}',
        updated_at   = CURRENT_TIMESTAMP
        where file_id = '${file_id}'`)
      };
    } catch (error) {
      return {        
        success: false,
        message: 'Error in update user : '+ error.message,
        data: null
      }
    }
}

  
// -------------------------------------------------------------------------------------------------------------------------------------------------
// --------------------------------------------------------UPDATE END----------------------------------------------------------------------------  
// -------------------------------------------------------------------------------------------------------------------------------------------------
// --------------------------------------------------------DELETE----------------------------------------------------------------------------


async function remove(req, res) { //fungsi untuk merubah data user 
  let file_id = req.params.id;
  status_postgre = await removeFile(file_id);  
  if (status_postgre.success == false ) {
    res.status(400).json(status_postgre);
    return
  }
  let params=req.body;
  status_postgre_log = await removeLog(file_id,params);  
  if (status_postgre.success == false ) {
    res.status(400).json(status_postgre);
    return
  }
      res.status(200).json({        
        success: 'true',
        message: "Data has been removed",
        data: null
      });
}

async function removeFile(file_id) { //fungsi untuk merubah data user 
    try {
      return {
        success: true,
        data: await db.result('Update users_project_file set is_deleted=true,deleted_at=CURRENT_TIMESTAMP WHERE file_id = $1', file_id)
      };
    } catch (error) {
      return {        
        success: false,
        message: 'Error in update user : '+ error.message,
        data: null
      }
    }
}
async function removeLog(file_id,params) { //fungsi untuk merubah data user 
  users_id = params.users_id;
  project_id = params.project_id;
  type = "Menghapus file";
  target_object_id = file_id;
  try {
    return {
      success: true,
      data: await  db.one('insert into log(project_id,users_id,type,target_object_id)'+
     'values($1,$2,$3,$4) RETURNING log_id',
     [project_id,users_id,type,target_object_id])
    };
  } catch (error) {
    return {      
      success: false,
      message: 'Error in insert project into Database: '+ error.detail,
      data: null
    }
  }
}
  
// -------------------------------------------------------------------------------------------------------------------------------------------------
// --------------------------------------------------------DELETE END----------------------------------------------------------------------------  

function validate(body) {
  const schema = {
    name: Joi.string(),
    project_id: Joi.string(),
    file: Joi.string(),
    users_id: Joi.string()
  };
  return Joi.validate(body, schema);
}


module.exports = {
  create : insert,
  read : read,
  readSingle : readSingle,
  update : update,
  delete : remove,
  readFileByUser : readFileByUser,
  readFileByProject : readFileByProject
};