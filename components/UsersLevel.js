var conn = require('../lib/database');
var Axios = require('axios');
var Joi = require('joi');
db = conn.db;


// -------------------------------------------------------------------------------------------------------------------------------------------------
// --------------------------------------------------------CREATE----------------------------------------------------------------------------

async function insert(req, res) { 
  const result = validate(req.body);
  params = req.body;

  if (result.error) return res.status(400).send(
    { success: false,
      message: result.error.details[0].message,
      data: null
    }); 

  status_postgre = await insertUser(params.name); //insert data user kedalam table user
  if (status_postgre.success == false ) {
    res.status(400).json(status_postgre);
  }
      res.status(200).json({ 
        success: true,
        message: "Data has been inserted into database",
        data: null
      }); 
}
async function insertUser(name) { //funsgi untuk insert data kedalam database postgre
  try {
    return {
      success: true,
      data: await  db.one('insert into users_level(name)'+
     'values($1) ON CONFLICT (name) DO NOTHING RETURNING users_level_id',
     [name])
    };
  } catch (error) {
    return {      
      success: false,
      message: 'Error in insert user level into Database: '+ error.message + ' duplicate name',
      data: null
    }
  }
}


// -------------------------------------------------------------------------------------------------------------------------------------------------
// --------------------------------------------------------CREATE END----------------------------------------------------------------------------


// -------------------------------------------------------------------------------------------------------------------------------------------------
// --------------------------------------------------------READ----------------------------------------------------------------------------
function read(req,res,next){ 
  db.any('select * from users_level')
  .then(function (data) {
   res.status(200)
     .json({
       success: true,
       message: 'Retrieved user data',
       data: data
     });
   })
  .catch(function (err) {
   res.status(400)
      .json({
        success: false,
        message: 'Error occured : '+err.message,
        data: null
      });
   });
}

function readSingle(req,res,next){ 
  db.one('select * from users_level where users_level_id= $1', req.params.id)
  .then(function(data) {
    res.status(200)
    .json({
      success: true,
      message: 'Retrieved user data',
      data: data
      });
  })
  .catch(function (err) {
    res.status(400)
    .json({
      success: false,
      message: 'Error occured : ' + err.message,
      data: null
    });
  });
}
// -------------------------------------------------------------------------------------------------------------------------------------------------
// --------------------------------------------------------READ END----------------------------------------------------------------------------  

// -------------------------------------------------------------------------------------------------------------------------------------------------
// --------------------------------------------------------UPDATE----------------------------------------------------------------------------

async function update(req, res) { //fungsi untuk merubah data user 
  params = req.body;
  users_level_id = req.params.id;

  
  status_postgre = await updateUser(params.name,users_level_id);  
  if (status_postgre.success == false ) {
    res.status(400).json(status_postgre);
    return
  }
      res.status(200).json({        
        success: 'true',
        message: "Data has been updated",
        data: null
      });
}

async function updateUser(name,users_level_id) { //fungsi untuk merubah data user 
    try {
      return {
        success: true,
        data: await db.none(`update users_level set
        name   = '${name}'
        where users_level_id = '${users_level_id}'`)
      };
    } catch (error) {
      return {        
        success: false,
        message: 'Error in update user : '+ error.message,
        data: null
      }
    }
}

  
// -------------------------------------------------------------------------------------------------------------------------------------------------
// --------------------------------------------------------UPDATE END----------------------------------------------------------------------------  
// -------------------------------------------------------------------------------------------------------------------------------------------------
// --------------------------------------------------------DELETE----------------------------------------------------------------------------

async function remove(req, res) { //fungsi untuk merubah data 

  db.result('DELETE FROM users_level WHERE users_level_id = $1', req.params.id)
  .then(function (data) {
    res.status(200)
      .json({
        success: true,
        message: 'delete ' + data.rowCount+' user level data',
        data: null
        
      });
  })
  .catch(function (err) {
    res.status(400)
      .json({
        success: false,
        message: 'Error occured : ' + err.message,
        data: null
      });
  });
}

  
// -------------------------------------------------------------------------------------------------------------------------------------------------
// --------------------------------------------------------DELETE END----------------------------------------------------------------------------  

function validate(body) {
  const schema = {
    name: Joi.string()
  };
  return Joi.validate(body, schema);
}


module.exports = {
  create : insert,
  read : read,
  readSingle : readSingle,
  update : update,
  delete : remove
};