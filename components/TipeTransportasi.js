var conn = require('../lib/database');
var Axios = require('axios');
var Joi = require('joi');
db = conn.db;


// -------------------------------------------------------------------------------------------------------------------------------------------------
// --------------------------------------------------------CREATE----------------------------------------------------------------------------

async function insert(req, res) { 
  const result = validate(req.body);
  params = req.body;

  if (result.error) return res.status(400).send(
    { success: false,
      message: result.error.details[0].message,
      data: null
    }); 

  status_postgre = await insertTipeTransportasi(params); //insert data user kedalam table user
  if (status_postgre.success == false ) {
    res.status(400).json(status_postgre);
  }

      res.status(200).json({ 
        success: true,
        message: "Data has been inserted into database",
        data: null
      }); 
}
async function insertTipeTransportasi(params) { //funsgi untuk insert data kedalam database postgre
  try {
    return {
      success: true,
      data: await  db.one('insert into tipe_transportasi(nama_tipe,keterangan)'+
     'values(${nama_tipe},${keterangan}) RETURNING id_tipe_transportasi',
     params)
    };
  } catch (error) {
    return {      
      success: false,
      message: 'Error in insert tipe transportasi into Database: '+ error.detail,
      data: null
    }
  }
}
// -------------------------------------------------------------------------------------------------------------------------------------------------
// --------------------------------------------------------CREATE END----------------------------------------------------------------------------


// -------------------------------------------------------------------------------------------------------------------------------------------------
// --------------------------------------------------------READ----------------------------------------------------------------------------

function read(req, res, next) {
    let sort_order = (req.query.sort_order) ? req.query.sort_order : '';
    let sort_by = (req.query.sort_by) ? ' order by ' + req.query.sort_by : '';
    let page_offset = (req.query.page_offset) ? ' offset ' + req.query.page_offset : '';
    let page_size = (req.query.page_size) ? ' limit ' + parseInt(req.query.page_size) : '';
    
    query_select = ' select * from tipe_transportasi ' + sort_by + ' ' + sort_order + page_size + page_offset;
    query_all = 'select count(*) from tipe_transportasi';
    Promise.all([db.result(query_select), db.result(query_all)])
      .then(function (data) {
        res.status(200)
          .json({
            success: true,
            message: 'Retrieved tipe transportasi data',
            num_of_data: data[0].rowCount,
            data: data[0].rows,
            total_data : data[1].rows[0].count
            
          });
      })
      .catch(function (err) {
        res.status(400)
          .json({
            success: false,
            message: 'Error occured: '+err.message,
            data: null,
          });
      });
  }

function readSingle(req,res,next){ 
  db.one('select * from tipe_transportasi where id_tipe_transportasi=$1', req.params.id)
  .then(function(data) {
    res.status(200)
    .json({
      success: true,
      message: 'Retrieved tipe_transportasi data',
      data: data
      });
  })
  .catch(function (err) {
    res.status(400)
    .json({
      success: false,
      message: 'Error occured : ' + err.message,
      data: null
    });
  });
}
// -------------------------------------------------------------------------------------------------------------------------------------------------
// --------------------------------------------------------READ END----------------------------------------------------------------------------  

// -------------------------------------------------------------------------------------------------------------------------------------------------
// --------------------------------------------------------UPDATE----------------------------------------------------------------------------

async function update(req, res) { //fungsi untuk merubah data user 
  params = req.body;
  id_tipe_transportasi= req.params.id;
  status_postgre = await updateTipeTransportasi(params,id_tipe_transportasi);  
  if (status_postgre.success == false ) {
    res.status(400).json(status_postgre);
    return
  }
      res.status(200).json({        
        success: 'true',
        message: "Data has been updated",
        data: null
      });
}

async function updateTipeTransportasi(params,id_tipe_transportasi) { //fungsi untuk merubah data user 
   let nama_tipe = params.nama_tipe;
   let keterangan = params.keterangan;
    try {
      return {
        success: true,
        data: await db.none(`update tipe_transportasi set
        nama_tipe   = '${nama_tipe}',
        keterangan = '${keterangan}'
        where id_tipe_transportasi = '${id_tipe_transportasi}'`)
      };
    } catch (error) {
      return {        
        success: false,
        message: 'Error in update tipe transportasi : '+ error.message,
        data: null
      }
    }
}

  
// -------------------------------------------------------------------------------------------------------------------------------------------------
// --------------------------------------------------------UPDATE END----------------------------------------------------------------------------  
// -------------------------------------------------------------------------------------------------------------------------------------------------
// --------------------------------------------------------DELETE----------------------------------------------------------------------------


async function remove(req, res) { //fungsi untuk merubah data user 
  let id_tipe_transportasi = req.params.id;
  status_postgre = await removeTipeTransportasi(id_tipe_transportasi);  
  if (status_postgre.success == false ) {
    res.status(400).json(status_postgre);
    return
  }
      res.status(200).json({        
        success: 'true',
        message: "Data has been removed",
        data: null
      });
}

async function removeTipeTransportasi(id_tipe_transportasi) { //fungsi untuk merubah data user 
    try {
      return {
        success: true,
        data: await db.any('Delete from tipe_transportasi where id_tipe_transportasi = $1', id_tipe_transportasi)
      };
    } catch (error) {
      return {        
        success: false,
        message: 'Error in update tipe transportasi : '+ error.message,
        data: null
      }
    }
}
  
// -------------------------------------------------------------------------------------------------------------------------------------------------
// --------------------------------------------------------DELETE END----------------------------------------------------------------------------  

function validate(body) {
  const schema = {
    nama_tipe: Joi.string(),
    keterangan: Joi.string()
  };
  return Joi.validate(body, schema);
}


module.exports = {
  create : insert,
  read : read,
  readSingle : readSingle,
  update : update,
  delete : remove
};