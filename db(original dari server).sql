--
-- PostgreSQL database dump
--

-- Dumped from database version 9.5.12
-- Dumped by pg_dump version 9.5.12

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;

--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


--
-- Name: citext; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS citext WITH SCHEMA public;


--
-- Name: EXTENSION citext; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION citext IS 'data type for case-insensitive character strings';


--
-- Name: uuid-ossp; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS "uuid-ossp" WITH SCHEMA public;


--
-- Name: EXTENSION "uuid-ossp"; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION "uuid-ossp" IS 'generate universally unique identifiers (UUIDs)';


--
-- Name: email; Type: DOMAIN; Schema: public; Owner: postgres
--

CREATE DOMAIN public.email AS public.citext
	CONSTRAINT email_check CHECK ((VALUE OPERATOR(public.~) '^[a-zA-Z0-9.!#$%&''*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$'::public.citext));


ALTER DOMAIN public.email OWNER TO postgres;

--
-- Name: gender; Type: TYPE; Schema: public; Owner: postgres
--

CREATE TYPE public.gender AS ENUM (
    'male',
    'female',
    'other'
);


ALTER TYPE public.gender OWNER TO postgres;

--
-- Name: stock_transfer_operation; Type: TYPE; Schema: public; Owner: postgres
--

CREATE TYPE public.stock_transfer_operation AS ENUM (
    'IN',
    'OUT'
);


ALTER TYPE public.stock_transfer_operation OWNER TO postgres;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: agency; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.agency (
    name character varying NOT NULL,
    address character varying,
    city_name character varying,
    region_name character varying,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone,
    deleted_at timestamp without time zone,
    token_trx character varying,
    token_acc character varying,
    token_view character varying,
    username character varying NOT NULL,
    password_hash character varying NOT NULL,
    phone_number character varying,
    email public.email,
    last_login timestamp without time zone,
    is_active boolean DEFAULT true NOT NULL,
    is_deleted boolean DEFAULT false NOT NULL,
    is_banned boolean DEFAULT false NOT NULL,
    id uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    firebase_id character varying,
    address_coord_lat numeric,
    address_coord_long character varying,
    city_id character varying,
    region_id character varying
);


ALTER TABLE public.agency OWNER TO postgres;

--
-- Name: agency_dos; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.agency_dos (
    dos_id uuid NOT NULL,
    agency_id uuid NOT NULL,
    created_at timestamp without time zone DEFAULT now() NOT NULL,
    updated_at timestamp without time zone
);


ALTER TABLE public.agency_dos OWNER TO postgres;

--
-- Name: customer; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.customer (
    first_name character varying,
    created_at timestamp without time zone DEFAULT now() NOT NULL,
    updated_at timestamp without time zone,
    deleted_at timestamp without time zone,
    last_name character varying,
    gender public.gender,
    birthdate date,
    username character varying NOT NULL,
    password_hash character varying NOT NULL,
    address_coordinate point,
    city_id integer,
    city_name character varying,
    region_id integer,
    region_name character varying,
    address character varying,
    phone_number character varying,
    is_active boolean DEFAULT true NOT NULL,
    is_banned boolean DEFAULT false NOT NULL,
    token_trx character varying NOT NULL,
    token_acc character varying NOT NULL,
    token_view character varying NOT NULL,
    last_login timestamp without time zone,
    avatar_url character varying,
    email public.email NOT NULL,
    id uuid DEFAULT public.uuid_generate_v4() NOT NULL
);


ALTER TABLE public.customer OWNER TO postgres;

--
-- Name: customer_order; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.customer_order (
    order_number character varying,
    order_type character varying,
    pick_address character varying,
    pick_address_coordinate point,
    pick_city_id integer,
    pick_city_name character varying,
    pick_region_id integer,
    pick_region_name character varying,
    pick_date date,
    pick_time time without time zone,
    is_canceled boolean DEFAULT false NOT NULL,
    is_completed boolean DEFAULT false NOT NULL,
    is_in_progress boolean DEFAULT false NOT NULL,
    is_packing boolean DEFAULT false NOT NULL,
    is_paid boolean DEFAULT false NOT NULL,
    is_ready boolean DEFAULT false NOT NULL,
    is_shipping boolean DEFAULT false NOT NULL,
    canceled_date timestamp without time zone,
    completed_date timestamp without time zone,
    processed_date timestamp without time zone,
    packing_date timestamp without time zone,
    paid_date timestamp without time zone,
    ready_date timestamp without time zone,
    shipping_date timestamp without time zone,
    price_grandtotal numeric,
    created_at timestamp without time zone DEFAULT now() NOT NULL,
    updated_at timestamp without time zone,
    deleted_at timestamp without time zone,
    is_deleted boolean DEFAULT false NOT NULL,
    price_total numeric,
    price_shipping numeric,
    price_adm_fee numeric,
    payment_date timestamp without time zone,
    payment_type character varying,
    payment_vendor character varying,
    payment_status character varying,
    id uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    customer_id uuid NOT NULL,
    pick_dos_id uuid
);


ALTER TABLE public.customer_order OWNER TO postgres;

--
-- Name: customer_order_line; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.customer_order_line (
    product_name character varying,
    product_price numeric,
    product_price_discount numeric,
    product_qty numeric,
    product_price_subtotal numeric,
    created_at timestamp without time zone DEFAULT now() NOT NULL,
    updated_at timestamp without time zone,
    deleted_at timestamp without time zone,
    is_deleted boolean DEFAULT false NOT NULL,
    product_id uuid NOT NULL,
    customer_order_id uuid NOT NULL,
    id uuid NOT NULL,
    product_price_discount_percent double precision
);


ALTER TABLE public.customer_order_line OWNER TO postgres;

--
-- Name: dos; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.dos (
    name character varying NOT NULL,
    address character varying,
    city_name character varying,
    region_name character varying,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone,
    deleted_at timestamp without time zone,
    token_trx character varying,
    token_acc character varying,
    token_view character varying,
    username character varying NOT NULL,
    password_hash character varying NOT NULL,
    phone_number character varying,
    email public.email,
    last_login timestamp without time zone,
    avatar_url character varying,
    is_deleted boolean DEFAULT false NOT NULL,
    is_active boolean DEFAULT true NOT NULL,
    is_banned boolean DEFAULT false NOT NULL,
    id uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    city_id character varying,
    region_id character varying,
    address_coord_lat numeric,
    address_coord_long numeric,
    agency_id uuid,
    firebase_id character varying
);


ALTER TABLE public.dos OWNER TO postgres;

--
-- Name: inventory; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.inventory (
    note text,
    purchase_order_id character varying,
    qty numeric NOT NULL,
    is_stock_transfer boolean DEFAULT false NOT NULL,
    log_date timestamp without time zone,
    created_at timestamp without time zone DEFAULT now() NOT NULL,
    updated_at timestamp without time zone,
    deleted_at timestamp without time zone,
    is_deleted boolean DEFAULT false NOT NULL,
    operation public.stock_transfer_operation NOT NULL,
    id uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    warehouse_id uuid NOT NULL,
    product_id uuid NOT NULL,
    inventory_transfer_id uuid NOT NULL,
    customer_order_id uuid,
    operation_multiplier smallint NOT NULL
);


ALTER TABLE public.inventory OWNER TO postgres;

--
-- Name: inventory_transfer; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.inventory_transfer (
    note text,
    warehouse_from bigint,
    warehouse_to bigint,
    qty numeric,
    transfer_date timestamp without time zone,
    created_at timestamp without time zone DEFAULT now() NOT NULL,
    updated_at timestamp without time zone,
    deleted_at timestamp without time zone,
    is_deleted boolean DEFAULT false NOT NULL,
    id uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    product_id uuid NOT NULL
);


ALTER TABLE public.inventory_transfer OWNER TO postgres;

--
-- Name: product; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.product (
    name character varying NOT NULL,
    description text,
    created_at timestamp without time zone DEFAULT now() NOT NULL,
    updated_at timestamp without time zone,
    deleted_at timestamp without time zone,
    published_at timestamp without time zone,
    is_published boolean DEFAULT false NOT NULL,
    is_deleted boolean DEFAULT false NOT NULL,
    price_base numeric,
    price_discount numeric,
    price_discount_percent_value numeric,
    price_real_calc numeric,
    json_metadata json,
    tags json,
    is_discount boolean DEFAULT false NOT NULL,
    id uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    price_discount_percent numeric,
    category_id uuid,
    image_url character varying,
    city_id character varying,
    region_id character varying,
    city_name character varying,
    region_name character varying,
    firebase_id character varying,
    agency_id uuid
);


ALTER TABLE public.product OWNER TO postgres;

--
-- Name: product_category; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.product_category (
    id uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    name character varying NOT NULL,
    parent uuid,
    created_at timestamp without time zone DEFAULT now() NOT NULL,
    updated_at timestamp without time zone,
    deleted_at time with time zone,
    is_deleted boolean DEFAULT false NOT NULL
);


ALTER TABLE public.product_category OWNER TO postgres;

--
-- Name: product_image; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.product_image (
    id uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    product_id uuid NOT NULL,
    bucket_name character varying,
    sub_folder character varying,
    filename character varying,
    public_url character varying NOT NULL,
    created_at timestamp without time zone DEFAULT now() NOT NULL,
    updated_at timestamp without time zone,
    deleted_at timestamp without time zone,
    is_deleted boolean DEFAULT false NOT NULL,
    is_thumbnail boolean DEFAULT false,
    mimetype character varying
);


ALTER TABLE public.product_image OWNER TO postgres;

--
-- Name: promo; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.promo (
    images_url json,
    start_date timestamp without time zone,
    expire_date timestamp without time zone,
    title character varying NOT NULL,
    description text,
    is_published boolean,
    published_at timestamp without time zone,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    deleted_at timestamp without time zone,
    is_deleted boolean,
    city_id integer,
    city_name character varying,
    region_id integer,
    region_name character varying,
    product_id uuid,
    id uuid DEFAULT public.uuid_generate_v4() NOT NULL
);


ALTER TABLE public.promo OWNER TO postgres;

--
-- Name: warehouse; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.warehouse (
    name character varying NOT NULL,
    address text,
    address_coordinate point,
    city_id integer,
    city_name character varying,
    region_id integer,
    region_name character varying,
    created_at timestamp without time zone DEFAULT now(),
    updated_at timestamp without time zone,
    deleted_at timestamp without time zone,
    is_deleted boolean,
    agency_id uuid,
    id uuid DEFAULT public.uuid_generate_v4() NOT NULL
);


ALTER TABLE public.warehouse OWNER TO postgres;

--
-- Data for Name: agency; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.agency (name, address, city_name, region_name, created_at, updated_at, deleted_at, token_trx, token_acc, token_view, username, password_hash, phone_number, email, last_login, is_active, is_deleted, is_banned, id, firebase_id, address_coord_lat, address_coord_long, city_id, region_id) FROM stdin;
string	string	Bandung	Jawa Barat	2018-04-18 01:25:22	\N	\N	b31691a02eeb2764b6fe9eecfe4e3fd6	56b4b12a67b2558b823fbb5a5e56be30	bd57f9ff3f36c65d289c3da36b40cba0	string	$2b$12$rCN6vjZNZrn53QK4Tl8Oj.uH.uLCPJcs94TkVaB3WowFBn9k.NJ4C	122323	eko5@domain.com	\N	t	f	f	7ede9fc8-d0d0-4909-a390-e589ce2a3ac5	WhAVansZJmXKipMnX6ezrctQzG73	2.33	3.3	\N	\N
Agency Tes	string	Bandung	Jawa Barat	2018-04-18 01:28:07	2018-04-26 08:18:56	\N	cd7fa30b9915a52a62dd70f0f41c80e0	3b90bf2eba44a0f5154fe123182e15b9	fe277e4ec0c88f3a3c2c69c9f0567b4c	strings	$2b$12$ujlUjoB3h14zy55yr5mKFOC/QY32jUxBulKT2695huys0uefc0U8e	122323	eko6@domain.com	\N	t	f	f	22e8e60a-b6dc-49d6-aa50-e8a70fdd1391	MbZ9kvKUpvUnVAcrYpl6kL7vb4A2	2.33	3.3	36	23
\.


--
-- Data for Name: agency_dos; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.agency_dos (dos_id, agency_id, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: customer; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.customer (first_name, created_at, updated_at, deleted_at, last_name, gender, birthdate, username, password_hash, address_coordinate, city_id, city_name, region_id, region_name, address, phone_number, is_active, is_banned, token_trx, token_acc, token_view, last_login, avatar_url, email, id) FROM stdin;
Eko	2018-03-28 02:23:59	2018-03-28 22:49:09	\N	Nugraha	male	2018-01-28	Nugrahas	$2b$12$yt1BjKU/3mQScgypozcoQuJsNBLHg9.8Mitv5Nz4N7bweDEe57TvK	(22.3000000000000007,22.3999999999999986)	0	string	0	string	string	86612345	t	f	148dfe8b9228feb4a208b90f1652a4aa	3f0d9dca67454689cf8bfa1be029ffd1	e76ae14925ddfe72dbf391f36fe902d7	2018-03-28 22:47:43	string	string@sad.com	6754ad12-6319-42d5-bf54-5a0b2d30d4b4
Eko	2018-03-22 13:19:55	2018-04-05 21:11:41	\N	Nugraha	male	2018-01-28	stringss	$2b$12$R7LqcJi3i9cfie89CMyUaOhL9A0exutNa/TBcN3SYqf6hdC2QV8hC	(22.1999999999999993,33.2000000000000028)	0	string	0	string	string	86612345	t	f	5ebf34ac7e537ff1904bffe2694745e7	5fca4b81b507715b517c368b9514b950	1f52369165b4c20281829401cf64c72d	\N	string	string@email.coms	9edfb2b9-7af1-41b5-8bf1-f4a88e909b20
\.


--
-- Data for Name: customer_order; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.customer_order (order_number, order_type, pick_address, pick_address_coordinate, pick_city_id, pick_city_name, pick_region_id, pick_region_name, pick_date, pick_time, is_canceled, is_completed, is_in_progress, is_packing, is_paid, is_ready, is_shipping, canceled_date, completed_date, processed_date, packing_date, paid_date, ready_date, shipping_date, price_grandtotal, created_at, updated_at, deleted_at, is_deleted, price_total, price_shipping, price_adm_fee, payment_date, payment_type, payment_vendor, payment_status, id, customer_id, pick_dos_id) FROM stdin;
\.


--
-- Data for Name: customer_order_line; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.customer_order_line (product_name, product_price, product_price_discount, product_qty, product_price_subtotal, created_at, updated_at, deleted_at, is_deleted, product_id, customer_order_id, id, product_price_discount_percent) FROM stdin;
\.


--
-- Data for Name: dos; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.dos (name, address, city_name, region_name, created_at, updated_at, deleted_at, token_trx, token_acc, token_view, username, password_hash, phone_number, email, last_login, avatar_url, is_deleted, is_active, is_banned, id, city_id, region_id, address_coord_lat, address_coord_long, agency_id, firebase_id) FROM stdin;
DOS11	Sudirman	Jakarta Pusat	DKI Jakarta	2018-04-19 11:54:08	\N	\N	\N	\N	\N	DOS11_d5f90183	$2b$12$PLuD8nE/2uR.dnx5QEJVse2SzEznwMFC1g2tfAdTGYPz9Tsvt6Yy6	\N	dos11@gmail.com	\N	\N	f	t	f	f6412941-dae2-4ca0-8c58-581d76233588	22	11	22.33	112.22	7ede9fc8-d0d0-4909-a390-e589ce2a3ac5	-LARF9PiWhhQGfFah0wR
DOS12	Sudirman	Jakarta Pusat	DKI Jakarta	2018-04-19 11:55:57	\N	\N	\N	\N	\N	DOS12_5b0de5d9	$2b$12$EC6KDx8izPZ9s8A9as/pReTFmFxPrMPHudiY1Ob5x6z7PWDHrsAiW	\N	dos12@gmail.com	\N	\N	f	t	f	73c6f9e9-f7b8-4e60-a256-5f0009b6dce8	22	11	22.33	112.22	7ede9fc8-d0d0-4909-a390-e589ce2a3ac5	-LARF_ETzbHLio4VNd4Y
DOSQ	JL string	Bandung	Jawa Barat	2018-05-02 11:17:11	\N	\N	\N	\N	\N	DOSQ_KUHLNU	$2b$12$2TeMxK.zBCi0ryWOoGYZnums1Wk39aqWr5hdjls42T7neX1jGWRUS	\N	string@domain.com	\N	\N	f	t	f	d1070433-7452-4605-9cd5-0140d87be0f0	33	22	15.22	20.3	22e8e60a-b6dc-49d6-aa50-e8a70fdd1391	-LBVZV_cZH37pNTP63pq
\.


--
-- Data for Name: inventory; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.inventory (note, purchase_order_id, qty, is_stock_transfer, log_date, created_at, updated_at, deleted_at, is_deleted, operation, id, warehouse_id, product_id, inventory_transfer_id, customer_order_id, operation_multiplier) FROM stdin;
\.


--
-- Data for Name: inventory_transfer; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.inventory_transfer (note, warehouse_from, warehouse_to, qty, transfer_date, created_at, updated_at, deleted_at, is_deleted, id, product_id) FROM stdin;
\.


--
-- Data for Name: product; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.product (name, description, created_at, updated_at, deleted_at, published_at, is_published, is_deleted, price_base, price_discount, price_discount_percent_value, price_real_calc, json_metadata, tags, is_discount, id, price_discount_percent, category_id, image_url, city_id, region_id, city_name, region_name, firebase_id, agency_id) FROM stdin;
Product Upgrade	Test	2018-04-23 19:22:20	2018-05-03 06:40:07	\N	\N	f	f	22000	17600	4400	17600	\N	\N	t	ebd8283a-500a-4d25-89b5-7638cfbd5bff	20	\N	\N	33	22	Bandung	Jawa Barat	-LAmS4mFtojMsy4tHJRO	22e8e60a-b6dc-49d6-aa50-e8a70fdd1391
\.


--
-- Data for Name: product_category; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.product_category (id, name, parent, created_at, updated_at, deleted_at, is_deleted) FROM stdin;
bea7cbd6-c31a-44c5-b805-7156f9904217	Fashion	\N	2018-04-13 19:38:36	\N	\N	f
\.


--
-- Data for Name: product_image; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.product_image (id, product_id, bucket_name, sub_folder, filename, public_url, created_at, updated_at, deleted_at, is_deleted, is_thumbnail, mimetype) FROM stdin;
7e2ceecf-c799-4641-8f25-e1991381c831	338a787c-b72b-4566-9e7c-3d3b27130dc5	storages-bawaapa	ba-public/img-products/	product_338a787c-b72b-4566-9e7c-3d3b27130dc5_afbe65a4_symphony-green.png	https://storage.googleapis.com/storages-bawaapa/ba-public/img-products/product_338a787c-b72b-4566-9e7c-3d3b27130dc5_afbe65a4_symphony-green.png	2018-04-13 20:15:17	\N	\N	f	f	image/png
\.


--
-- Data for Name: promo; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.promo (images_url, start_date, expire_date, title, description, is_published, published_at, created_at, updated_at, deleted_at, is_deleted, city_id, city_name, region_id, region_name, product_id, id) FROM stdin;
\.


--
-- Data for Name: warehouse; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.warehouse (name, address, address_coordinate, city_id, city_name, region_id, region_name, created_at, updated_at, deleted_at, is_deleted, agency_id, id) FROM stdin;
\.


--
-- Name: agency_dos_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.agency_dos
    ADD CONSTRAINT agency_dos_pkey PRIMARY KEY (dos_id, agency_id);


--
-- Name: agency_email_uqkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.agency
    ADD CONSTRAINT agency_email_uqkey UNIQUE (email);


--
-- Name: agency_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.agency
    ADD CONSTRAINT agency_pkey PRIMARY KEY (id);


--
-- Name: agency_username_uqkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.agency
    ADD CONSTRAINT agency_username_uqkey UNIQUE (username);


--
-- Name: customer_order_line_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.customer_order_line
    ADD CONSTRAINT customer_order_line_pkey PRIMARY KEY (id);


--
-- Name: customer_order_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.customer_order
    ADD CONSTRAINT customer_order_pkey PRIMARY KEY (id);


--
-- Name: customer_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.customer
    ADD CONSTRAINT customer_pkey PRIMARY KEY (id);


--
-- Name: customer_username_uqkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.customer
    ADD CONSTRAINT customer_username_uqkey UNIQUE (username) DEFERRABLE;


--
-- Name: dos_email_uqkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.dos
    ADD CONSTRAINT dos_email_uqkey UNIQUE (email);


--
-- Name: dos_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.dos
    ADD CONSTRAINT dos_pkey PRIMARY KEY (id);


--
-- Name: dos_username_uqkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.dos
    ADD CONSTRAINT dos_username_uqkey UNIQUE (username);


--
-- Name: email_uqkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.customer
    ADD CONSTRAINT email_uqkey UNIQUE (email);


--
-- Name: inventory_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.inventory
    ADD CONSTRAINT inventory_pkey PRIMARY KEY (id);


--
-- Name: inventory_transfer_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.inventory_transfer
    ADD CONSTRAINT inventory_transfer_pkey PRIMARY KEY (id);


--
-- Name: product_category_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.product_category
    ADD CONSTRAINT product_category_pkey PRIMARY KEY (id);


--
-- Name: product_image_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.product_image
    ADD CONSTRAINT product_image_pkey PRIMARY KEY (id);


--
-- Name: product_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.product
    ADD CONSTRAINT product_pkey PRIMARY KEY (id);


--
-- Name: promo_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.promo
    ADD CONSTRAINT promo_pkey PRIMARY KEY (id);


--
-- Name: warehouse_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.warehouse
    ADD CONSTRAINT warehouse_pkey PRIMARY KEY (id);


--
-- Name: fki_customer_order_customer_id_fkey; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX fki_customer_order_customer_id_fkey ON public.customer_order USING btree (customer_id);


--
-- Name: fki_customer_order_line_customer_order_id_fkey; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX fki_customer_order_line_customer_order_id_fkey ON public.customer_order_line USING btree (customer_order_id);


--
-- Name: fki_customer_order_line_product_id_fkey; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX fki_customer_order_line_product_id_fkey ON public.customer_order_line USING btree (product_id);


--
-- Name: fki_customer_order_pick_dos_fkey; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX fki_customer_order_pick_dos_fkey ON public.customer_order USING btree (pick_dos_id);


--
-- Name: fki_dos_agency_id_fkey; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX fki_dos_agency_id_fkey ON public.dos USING btree (agency_id);


--
-- Name: fki_inventory_customer_order_id_fkey; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX fki_inventory_customer_order_id_fkey ON public.inventory USING btree (customer_order_id);


--
-- Name: fki_inventory_inventory_transfer_id_fkey; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX fki_inventory_inventory_transfer_id_fkey ON public.inventory USING btree (inventory_transfer_id);


--
-- Name: fki_inventory_product_id_fkey; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX fki_inventory_product_id_fkey ON public.inventory USING btree (product_id);


--
-- Name: fki_inventory_transfer_product_id_fkey; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX fki_inventory_transfer_product_id_fkey ON public.inventory_transfer USING btree (product_id);


--
-- Name: fki_inventory_warehouse_id_fkey; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX fki_inventory_warehouse_id_fkey ON public.inventory USING btree (warehouse_id);


--
-- Name: fki_product_category_id_fkey; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX fki_product_category_id_fkey ON public.product USING btree (category_id);


--
-- Name: fki_promo_product_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX fki_promo_product_id ON public.promo USING btree (product_id);


--
-- Name: fki_warehouse_dos_id_fkey; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX fki_warehouse_dos_id_fkey ON public.warehouse USING btree (agency_id);


--
-- Name: agency_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.agency_dos
    ADD CONSTRAINT agency_id_fkey FOREIGN KEY (agency_id) REFERENCES public.agency(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: customer_order_customer_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.customer_order
    ADD CONSTRAINT customer_order_customer_id_fkey FOREIGN KEY (customer_id) REFERENCES public.customer(id) ON UPDATE CASCADE;


--
-- Name: customer_order_line_customer_order_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.customer_order_line
    ADD CONSTRAINT customer_order_line_customer_order_id_fkey FOREIGN KEY (customer_order_id) REFERENCES public.customer_order(id) ON UPDATE CASCADE;


--
-- Name: customer_order_line_product_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.customer_order_line
    ADD CONSTRAINT customer_order_line_product_id_fkey FOREIGN KEY (product_id) REFERENCES public.product(id) ON UPDATE CASCADE;


--
-- Name: customer_order_pick_dos_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.customer_order
    ADD CONSTRAINT customer_order_pick_dos_fkey FOREIGN KEY (pick_dos_id) REFERENCES public.dos(id) ON UPDATE CASCADE ON DELETE SET NULL;


--
-- Name: dos_agency_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.dos
    ADD CONSTRAINT dos_agency_id_fkey FOREIGN KEY (agency_id) REFERENCES public.agency(id) ON UPDATE CASCADE ON DELETE SET NULL;


--
-- Name: dos_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.agency_dos
    ADD CONSTRAINT dos_id_fkey FOREIGN KEY (dos_id) REFERENCES public.dos(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: inventory_customer_order_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.inventory
    ADD CONSTRAINT inventory_customer_order_id_fkey FOREIGN KEY (customer_order_id) REFERENCES public.customer_order(id) ON UPDATE CASCADE ON DELETE SET NULL;


--
-- Name: inventory_inventory_transfer_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.inventory
    ADD CONSTRAINT inventory_inventory_transfer_id_fkey FOREIGN KEY (inventory_transfer_id) REFERENCES public.inventory_transfer(id) ON UPDATE CASCADE;


--
-- Name: inventory_product_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.inventory
    ADD CONSTRAINT inventory_product_id_fkey FOREIGN KEY (product_id) REFERENCES public.product(id) ON UPDATE CASCADE;


--
-- Name: inventory_transfer_product_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.inventory_transfer
    ADD CONSTRAINT inventory_transfer_product_id_fkey FOREIGN KEY (product_id) REFERENCES public.product(id) ON UPDATE CASCADE;


--
-- Name: inventory_warehouse_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.inventory
    ADD CONSTRAINT inventory_warehouse_id_fkey FOREIGN KEY (warehouse_id) REFERENCES public.warehouse(id) ON UPDATE CASCADE;


--
-- Name: product_category_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.product
    ADD CONSTRAINT product_category_id_fkey FOREIGN KEY (category_id) REFERENCES public.product_category(id) ON UPDATE CASCADE ON DELETE SET NULL;


--
-- Name: promo_product_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.promo
    ADD CONSTRAINT promo_product_id FOREIGN KEY (product_id) REFERENCES public.product(id) ON UPDATE CASCADE ON DELETE SET NULL;


--
-- Name: warehouse_agency_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.warehouse
    ADD CONSTRAINT warehouse_agency_id_fkey FOREIGN KEY (agency_id) REFERENCES public.dos(id) ON UPDATE CASCADE ON DELETE SET NULL;


--
-- Name: SCHEMA public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

